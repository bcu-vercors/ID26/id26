class ImmutableFunctions:
    def __setattr__ (self, name, value):
        if name in self.__class__.__dict__ and callable(self.__class__.__dict__[name]):
            raise AttributeError (f'cannot set attribute. {name} is a function.')
        super().__setattr__(name, value)


class TEST(ImmutableFunctions):

    def __init__(self):
        self._proper = 10
        self._factor = 10
        
    def function(self, value):
        return value

    @property
    def proper(self):
        return self._proper
    @proper.setter
    def proper(self, value):
        self._proper = value

    #other way for immutable, but is a setter only in fact - property to be fixed.
    @property
    def factor(self):
        return lambda x: self.__set_factor(x)
    def __set_factor(self, x):
        if x is not None:
            self._factor = x
        return self._factor


#
# the next one can be replaced by bliss.common.utils.PURPLE etc...
#
    
class bcolor:
    PURPLE = "\033[95m"
    CYAN = "\033[96m"
    DARKCYAN = "\033[36m"
    BLUE = "\033[94m"
    GREEN = "\033[92m"
    YELLOW = "\033[93m"
    RED = "\033[91m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"
    END = "\033[0m"
