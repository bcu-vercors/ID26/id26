import os
import fcntl
import resource

from bliss import current_session
from bliss.common.logtools import log_warning
from bliss.common.utils import PURPLE

def _get_open_fds():
    fds = []
    soft, hard = resource.getrlimit(resource.RLIMIT_NOFILE)
    for fd in range(0, soft):
        try:
            flags = fcntl.fcntl(fd, fcntl.F_GETFD)
        except IOError:
            continue
        fds.append(fd)
    return fds


def _get_file_names_from_file_number(fds):
    names = []
    for fd in fds:
        name = f'fd{fd} - '
        name += os.readlink('/proc/self/fd/%d' % fd)
        names.append(name)
    return names

#
#
#

def print_open_fds():
  print("\n".join( _get_file_names_from_file_number(_get_open_fds())))
  print(resource.getrlimit(resource.RLIMIT_NOFILE))

  
def log_no_fds():
    nofds = len(_get_file_names_from_file_number(_get_open_fds()))
    log_warning (current_session, f'{nofds} open files.')

def log_open_fds():
    ofds = _get_file_names_from_file_number(_get_open_fds())
    ofds_str  = "\n".join( ofds)
    log_warning (current_session, f'{len(ofds)} open files, max is  {resource.getrlimit(resource.RLIMIT_NOFILE)}:\n{ofds_str}')
   
def log_serial_raw_handler (ser_list):
    for ser in ser_list:
        rh=ser._raw_handler
        if rh:
            log_warning (ser, dict(zip(['fd','rp','wp'],[rh.fd,rh._rpipe,rh._wpipe])))
        else:
            log_warning (ser, 'no handler initialised yet')

#
#
#

def trap_OSError(*ser_list):
    def inner(func):
        def wrapper(*a, **b):
            try:
                print(PURPLE('OSError tracking active'))
                log_no_fds()
                log_serial_raw_handler (ser_list)
                return func(*a, **b)
            except OSError as oserr:
                log_warning(current_session,"OPEN_FDS OSError" , oserr)
                log_serial_raw_handler (ser_list)
                log_open_fds()
        return wrapper
    return inner

#
# TEST
#

#==================
# chasing OSError 24 @trap_OSError(*ser_list) from fscan
# from id26.helpers.open_fds import trap_OSError
#ser_list = [
#    FSCAN.mono.controller.sock,
#    FSCAN.opiom._boards['opiom_cc1']._cnx,
#    ]
#try:
#    ser_list.append(setup_globals.qgchi2.controller.moco._cnx)
#except Exception:
#    pass
# @trap_OSError(*ser_list)
#def fscan(

@trap_OSError
def test2():
    ffl = []
    for ii in range(1200):
        ffl.append(open(f"/tmp/fff{ii}", mode="a+"))
   
    
def test1():  
   
    ffl = []

    try:
        for ii in range(1200):
            ffl.append(open(f"/tmp/fff{ii}", mode="a+"))
    except OSError as oserr:
        print("oula lalla" , oserr)
        print("\n".join( _get_file_names_from_file_number(_get_open_fds())))

        
        
