import functools
import traceback

from bliss.common.logtools import log_warning, log_debug
from bliss.common.utils import PURPLE

def tb(f=None, limit=100):
    stack = traceback.format_stack(f=f, limit=limit)
    if len(stack) >= 3:
        caller = stack[-3]
        caller = caller.rstrip()
#        return stack
        return caller

    
def log_trace (func):
    #print(PURPLE(f'traceback wrapper active on : {func}'))
    @functools.wraps(func)
    def wrapper (self, *args, **kwargs):
        log_debug(self, f'{func} called from: {tb()}')
        return func(self, *args, **kwargs)
    return wrapper

#
# test
#

class Test:

    @log_trace
    def state (self,msg='Hello'):
        print (msg)

        
