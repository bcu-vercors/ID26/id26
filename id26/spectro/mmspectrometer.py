# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import math
import numpy
from bliss.physics.units import ur
from bliss.physics.diffraction import CrystalPlane
from bliss.config import static
from bliss.controllers.motor import CalcController
from bliss.common.axis import Axis
from bliss.config.settings import HashObjSetting
from bliss.scanning.scan_meta import get_user_scan_meta 

from id26.spectro.spectrometer import SpectrometerBase, SpectroAnalyser, SpectroDetector

np = numpy

def _xes_eh2(radius, theta, a_y_off, a_z_set, det_deviation_x, alpha):

        def _make_angles(vector1,vector2):
            Unit_vector1 = vector1/np.linalg.norm(vector1)
            Unit_vector2 = vector2/np.linalg.norm(vector2)
            dot_product = np.dot(Unit_vector1,Unit_vector2)
            angle = np.arccos(dot_product)
            return angle
        
        np.set_printoptions(precision = 3, suppress = 'True')

        theta=np.deg2rad(theta)   #convert angles in radians
        alpha=np.deg2rad(alpha)

        #____caculate positions in Rowland frame assuming that detector and sample are placed along vertical line_____
        #axvert=radius*np.square(np.sin(theta)) # distance center line sample-detector and symmetric crystal Rowland position
        azvert=radius*np.sin(theta)*np.cos(theta) 
        dzvert=2*azvert # distance sample - detector

        Avec = [radius*np.sin(theta+alpha)*np.sin(theta-alpha), 0 , radius*np.sin(theta+alpha)*np.cos(theta-alpha)]    
        Dvec = [0, 0, dzvert]
        #Rvec = [axvert, 0, azvert] # this is needed to define the position of the Rowland circle

        rot_alpha = np.array([[np.cos(-alpha), 0, np.sin(-alpha)],[0, 1, 0], [-np.sin(-alpha), 0, np.cos(-alpha)]])
        CvecNorm = rot_alpha.dot([-1,0,0]); # norm on crystal plane
        AvecNorm = rot_alpha.dot(CvecNorm); # norm on crystal optical surface 
        #RvecNorm = [-1,0,0]
    
        y_angle=np.arcsin(a_y_off/Avec[0]) # phi rotation in vertical position
        rot_z=np.array([[np.cos(y_angle), -np.sin(y_angle), 0], [np.sin(y_angle), np.cos(y_angle), 0], [0, 0, 1]])
        Avec=rot_z.dot(Avec)
        Dvec=rot_z.dot(Dvec)
        #Rvec=rot_z.dot(Rvec)
        CvecNorm=rot_z.dot(CvecNorm)
        AvecNorm=rot_z.dot(AvecNorm)
        #RvecNorm=rot_z.dot(RvecNorm)

        # rotate Rowland frame such that az becomes a_z_set defined as
        # position of center of analyzer crystal
        if a_z_set != -1: # choose -1 if you do not want to rotate around y axis. This is only for testing and not an option in the EH2 spectrometer
            theta_z_off = np.arcsin(a_z_set/radius*np.sin(theta+alpha)) 
            z_angle = np.pi/2-theta+alpha-theta_z_off # rotation to have crystal at zoffvec(1). Positive alpha gives positive azoff
            rot_y = np.array([[np.cos(z_angle), 0, np.sin(z_angle)],[0, 1,0],[-np.sin(z_angle), 0, np.cos(z_angle)]])
            Avec = rot_y.dot(Avec)
            Dvec = rot_y.dot(Dvec)
            #Rvec = rot_y.dot(Rvec)
            CvecNorm = rot_y.dot(CvecNorm)
            AvecNorm = rot_y.dot(AvecNorm)
            #RvecNorm = rot_y.dot(RvecNorm)
    
        DvecNorm = np.subtract(Avec,Dvec)
        DvecNorm = DvecNorm/np.linalg.norm(DvecNorm)

        theta_ana = _make_angles(AvecNorm,[0,0,1])
        chi_ana = _make_angles([AvecNorm[0], AvecNorm[1], 0],[-1,0,0]) * np.sign(AvecNorm[1]) # angle in the xy plane
        dth = _make_angles(DvecNorm,[-1,0,0])
    
        # apply deviation of detector position. Definition in agreement with ID26 ray tracing code
        #print('det_deviation_x',det_deviation_x)
        det_deviation = det_deviation_x/np.cos(np.pi-dth)
        Dvec=Dvec+DvecNorm*det_deviation

        #return Avec, Dvec, Rvec, theta_ana, chi_ana, dth
        return Avec, Dvec, theta_ana, chi_ana, dth

class MMSpectroAnalyser(SpectroAnalyser):
    
    def __init__(self, *args, **kwargs):
        
        SpectroAnalyser.__init__(self, *args, **kwargs)
        
        bragg = self.config.get("bragg_master", None)
        if bragg is not None:
                self.bragg = bragg.position
        
        self.ref_params = HashObjSetting(f"MMSpectroAnalyser:{self.name}")

        if self.ref_params.get('radius') is None:
                self.ref_params['radius'] = self.config.get('radius', float, 1000)

        if self.ref_params.get('alpha') is None:
                self.ref_params['alpha'] = self.config.get('alpha', float, 0)

        if self.ref_params.get('a_z_set') is None:
                self.ref_params['a_z_set'] = self.config.get('a_z_set', float, 0)

        self._init_meta_data_publishing()

    def _init_meta_data_publishing(self):
        """this is about metadata publishing to the h5 file"""
        if not self.name:
            user_warning(
                "to publish metadata the spectrometer analyser needs a name in config"
            )
            return
        scan_meta_obj = get_user_scan_meta()
        scan_meta_obj.instrument.set(self, lambda _: {self.name: self.metadata()})

    def metadata(self):
        """ 
        this is about metadata publishing to the h5 file AND ICAT
        #self.reals[0].controller.ref_params['radius']
        #self.ref_params
        """
        
        meta_dict = dict()

        for key in self.ref_params.keys():
                meta_dict.update(
                        {
                                key: self.ref_params[key],
                        }
            )

        meta_dict["@NX_class"] = "NXspectro"

        return meta_dict
                
        
    def __info__(self):
            _infostr = f"{self.name}: radius {self.ref_params['radius']} y: {self.xpos} "
            if self.ref_params['alpha'] != 0:
                    _infostr = f"{_infostr} alpha: {self.ref_params['alpha']} "
            if self.ref_params['a_z_set'] != 0:
                    _infostr = f"{_infostr} a_z_set: {self.ref_params['a_z_set']} "
            return _infostr
     
    def calc_to_real(self, positions_dict):

        real_dict = dict()
        theta = positions_dict["bragg"] #mmanth = xes_th
        self.bragg = theta
        
        radius = self.ref_params['radius']
        alpha = self.ref_params['alpha']
        a_z_set = self.ref_params['a_z_set']
        
        if not numpy.isnan(theta):
            Vecs = _xes_eh2 (radius, theta, self.xpos, a_z_set, 0, alpha)
            real_dict["y"] = Vecs[0][0]
            real_dict["z"] = Vecs[0][2]
            real_dict["chi"] = math.degrees(Vecs[3])
            real_dict["th"] = math.degrees(Vecs[2])
        else:
            for axis in self.reals:
                real_dict[self._axis_tag(axis)] = axis.position
            
        return real_dict

    def calc_from_real(self, positions_dict):
        return {"bragg" : self.bragg}

    
class MMSpectroDetector(SpectroDetector):
    
    def __init__(self, *args, **kwargs):
        SpectroDetector.__init__(self, *args, **kwargs)
        
        self.dlong = self.config.get("dlong", None)
        self.azlong = self.config.get("azlong", None)

        self.dlong_angle  = self.config.get("dlong_angle", float, 68)
        self.dlong_travel = self.config.get("dlong_travel", float, 1200)
        self.dlong_base_x = self.config.get("dlong_base_x", float, -21.51)
        self.dlong_base_z = self.config.get("dlong_base_z", float, 160.25)
        
        self.ref_params = HashObjSetting(f"MMSpectroDetector:{self.name}")

       
        if self.ref_params.get('dx_offset') is None:
                print("Warning: dx_offset is None, updating with default value of 150 (mm)")
                self.ref_params['dx_offset'] = 150

        if self.ref_params.get('dz_offset') is None:
                print("Warning: dz_offset is None, updating with default value of 150 (mm)")
                self.ref_params['dz_offset'] = 150
        
        if self.ref_params.get('det_deviation_x') is None:
                print("Warning: det_deviation_x is None, updating with zero.")
                self.ref_params['det_deviation_x'] = 0  

        if self.ref_params.get('dz_correction') is None:
                self.ref_params['dz_correction'] = 0  

        if self.ref_params.get('radius') is None:
                self.ref_params['radius'] = self.config.get('radius', float)
           
        if self.ref_params.get('alpha') is None:
                self.ref_params['alpha'] = self.config.get('alpha', float, 0)

        if self.ref_params.get('a_z_set') is None:
                self.ref_params['a_z_set'] = self.config.get('a_z_set', float, 0)
                
        self._init_meta_data_publishing()

    def _init_meta_data_publishing(self):
        """this is about metadata publishing to the h5 file"""
        if not self.name:
            user_warning(
                "to publish metadata the spectrometer detector controller needs a name in config"
            )
            return
        scan_meta_obj = get_user_scan_meta()
        scan_meta_obj.instrument.set(self, lambda _: {self.name: self.metadata()})

    def metadata(self):
        """ 
        this is about metadata publishing to the h5 file AND ICAT
        #self.reals[0].controller.ref_params['radius']
        #self.ref_params
        """
        
        meta_dict = dict()

        for key in self.ref_params.keys():
                meta_dict.update(
                        {
                                key: self.ref_params[key],
                        }
            )

        meta_dict["@NX_class"] = "NXspectro"

        return meta_dict

    def __info__(self):
        _infostr = super().__info__() + f": radius {self.ref_params['radius']} {self.dlong.name}: {self.dlong.position} "
        if self.ref_params['alpha'] != 0:
                _infostr = f"{_infostr} alpha: {self.ref_params['alpha']} "
        if self.ref_params['a_z_set'] != 0:
                _infostr = f"{_infostr} a_z_set: {self.ref_params['a_z_set']} "
        if self.ref_params['det_deviation_x'] != 0:
                _infostr = f"{_infostr} det_deviation_x: {self.ref_params['det_deviation_x']} "
        if self.ref_params['dz_correction'] != 0:
                _infostr = f"{_infostr} dz_correction: {self.ref_params['dz_correction']} "
        if self.ref_params['dx_offset'] != 150:
                _infostr = f"{_infostr} dx_offset: {self.ref_params['dx_offset']} "
        if self.ref_params['dz_offset'] != 150:
                _infostr = f"{_infostr} dz_offset: {self.ref_params['dz_offset']} "

        return _infostr

    def calc_to_real(self, positions_dict):
        real_dict = dict()
        theta = positions_dict["bragg"]
        
        radius = self.ref_params.get('radius')
        det_deviation_x = self.ref_params.get('det_deviation_x')
        alpha = self.ref_params['alpha']
        a_z_set = self.ref_params['a_z_set']
            
        if not numpy.isnan(theta):        
            Vecs = _xes_eh2 (radius, theta, 0, a_z_set, det_deviation_x, alpha)
            dx,dz = self._dx_dz_calc(Vecs[1], self.dlong.position)
            real_dict["y"] = dx
            real_dict["z"] = dz
            real_dict["th"] = math.degrees(Vecs [4])
        else:
            for axis in self.reals:
                real_dict[self._axis_tag(axis)] = axis.position
            
        return real_dict

    def calc_from_real(self, positions_dict):
        return {"bragg" : positions_dict['th']/2}


    def _dx_dz_calc(self, Dvec, dlong_pos):

        dx=Dvec[0]
        dz=Dvec[2]
        
#        print("dx abs is {} and dz abs is {}".format(dx, dz))
        
        dlong_angle  = math.radians(self.dlong_angle);
        dlong_base_x = self.dlong_base_x
        dlong_base_z = self.dlong_base_z -self.azlong.position

#        print("dlong_angle is {} and dlong_base_x is {} and dlong_base_z is {}".format(dlong_angle, dlong_base_x, dlong_base_z))

        dlong_center_x = dlong_pos * math.cos(dlong_angle) + dlong_base_x
        dlong_center_z = dlong_pos * math.sin(dlong_angle) + dlong_base_z

#        print("dlong_center_x is {} and dlong_center_z is {}".format(dlong_center_x, dlong_center_z))

        dx_offset = self.ref_params.get('dx_offset')
        dz_offset = self.ref_params.get('dz_offset')
        dz_correction = self.ref_params.get('dz_correction')
        
#        print("dx_offset is {} and dz_offset is {} and dz_correction is {}".format(dx_offset, dz_offset, dz_correction))

        stage_dx_pos = dx - dlong_center_x + dx_offset 
        stage_dz_pos = dz - dlong_center_z + dz_offset + dz_correction

        return stage_dx_pos, stage_dz_pos
    
    
    def dlong_calc_middle(self, th_1, th_2):
        ''' 
        required if dlong is not moved during scans. The scan range is between low and high.
        A middle position is determined for dlong which is used itself to calculate the motor positions for stage_dx and stage_dz
        '''
        
        th_low = min(th_1, th_2)
        th_high = max(th_1, th_2)
        
        print(f'at low angle {th_low}')
        pos_lo, dvec_lo =  self._dlong_calc(th_low)
        print(f'at high angle {th_high}')
        pos_hi, dvec_hi =  self._dlong_calc(th_high)
            
        pos_middle = abs(pos_hi - pos_lo) / 2 + min(pos_hi, pos_lo)

        print(f'dlong at low angle at {pos_lo:.2f}')
        print(f'dlong at high angle at {pos_hi:.2f}')
        print(f'dlong middle position is at {pos_middle:.2f}')

        try:
                _dx_pos_lo, _dz_pos_lo = self._dx_dz_calc(dvec_lo, pos_middle)
                _dx_pos_hi, _dz_pos_hi = self._dx_dz_calc(dvec_hi, pos_middle)

                def _check_lim (range, value):
                        if value == min(list(range)+[value]):
                                return f'- OUT OF RANGE {range}'
                        if value == max(list(range)+[value]):
                                return f'- OUT OF RANGE {range}'
                        return ''
                
                print(f'\nFor this middle position of dlong we get:')
                print(f'stage dx at low angle at {_dx_pos_lo:.2f} {_check_lim(self._tagged["y"][0].limits, _dx_pos_lo)}')
                print(f'stage dx at high angle at {_dx_pos_hi:.2f} {_check_lim(self._tagged["y"][0].limits, _dx_pos_hi)}')
                print(f'stage dz at low angle at {_dz_pos_lo:.2f} {_check_lim(self._tagged["z"][0].limits, _dz_pos_lo)}')
                print(f'stage dz at high angle at {_dz_pos_hi:.2f} {_check_lim(self._tagged["z"][0].limits, _dz_pos_hi)}')

        except RuntimeError as e:
                print (e.args[0])
                
            
        return pos_middle


    def _dlong_calc(self, theta):
            
        vecs = _xes_eh2(self.ref_params['radius'], theta, 0, 0, 0, 0)
        dvec = vecs[1]
                
        dx=dvec[0]  
        dz=dvec[2]
                
        dlong_angle  = np.deg2rad (self.dlong_angle)
        dlong_travel = self.dlong_travel
        dlong_base_x = self.dlong_base_x
        dlong_base_z = self.dlong_base_z - self.azlong.position

        dlong_low_limit = self.dlong.low_limit
        dlong_high_limit = self.dlong.high_limit
        if dlong_low_limit > dlong_high_limit:
                dlong_low_limit,  dlong_high_limit = dlong_high_limit ,dlong_low_limit  
        
       
        # make lines for dlong stage and m=-1 through detector point
        m = np.tan(dlong_angle)
        b = dlong_base_z-dlong_base_x*m
        mn = np.tan(np.deg2rad(-45))
        bn = dz-mn*dx

        # calculate intersection point between dlong and m-1 line: This is where dlong will be driven.
        dlong_center_x = (bn-b)/(m-mn)
        dlong_center_z = m*dlong_center_x+b

        dlong_center_x = max (dlong_center_x, dlong_base_x)
        dlong_center_x = min (dlong_center_x, dlong_travel*np.cos(dlong_angle)+dlong_base_x)
        dlong_center_z = dlong_base_z if dlong_center_z < dlong_base_x else dlong_center_z
        dlong_center_z = min(dlong_center_z, dlong_travel*np.sin(dlong_angle) + dlong_base_z)

        dlong_pos = (dlong_center_z-dlong_base_z) / np.sin(dlong_angle) # This may be unnecessary as it is already done before if base is the lowest possible position

        if dlong_pos < dlong_low_limit:
                print('WARNING: Hitting low software limit on dlong. Recalculating dlong positions.')
                dlong_pos = dlong_low_limit + 1
                dlong_center_x=dlong_pos*np.cos(dlong_angle)+dlong_base_x;
                dlong_center_z=dlong_pos*np.sin(dlong_angle)+dlong_base_z;
                
        if dlong_pos > dlong_high_limit:
                print('WARNING: Hitting high sofware limit on dlong. Recalculating dlong positions.')
                dlong_pos = dlong_high_limit - 1;
                dlong_center_x=dlong_pos*np.cos(dlong_angle)+dlong_base_x;
                dlong_center_z=dlong_pos*np.sin(dlong_angle)+dlong_base_z;

        print(f'dlong distance from base is {dlong_pos:.2f}')
        print(f'distance x from base is {dlong_center_x:.2f}')
        print(f'distance z from base is {dlong_center_z:.2f}')

        stage_dx_pos, stage_dz_pos = self._dx_dz_calc(dvec, dlong_pos)
        
        print(f'stage dx at {stage_dx_pos:.2f}')
        print(f'stage dz at {stage_dz_pos:.2f}\n')
        
        return dlong_pos, dvec 


class MMSpectro(SpectrometerBase):

    def __init__(self, *args, **kwargs):
        SpectrometerBase.__init__(self, *args, **kwargs)
        self.ref_params = HashObjSetting(f"MMSpectro:{self.name}")

    def dlong_calc_middle (self, ene_low, ene_high):
        th_range = list(map(self.energy2bragg,[ene_low, ene_high]))
        return self.motors['detector'].controller.dlong_calc_middle (*th_range)

    def dlong_calc (self, energy):
        theta = self.energy2bragg(energy)
        return self.motors['detector'].controller._dlong_calc (theta)[0]

