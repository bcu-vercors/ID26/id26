# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import numpy as np
import tabulate

from bliss.config import settings
from bliss.controllers.motor import CalcController
from bliss.common.axis import Axis
from bliss.scanning.scan_meta import get_user_scan_meta

class SpectrometerBase(CalcController):

    def __init__(self, *args, **kwargs):

        CalcController.__init__(self, *args, **kwargs)

        #self.xtals = self.config.get("xtals", None)
        #if self.xtals is None:
        #   raise RuntimeError(f"No XtalManager configured")
        #else:
        #    if len(self.xtals.xtal_names) == 0:
        #        raise RuntimeError(f"No Crystals Defined in th XtalManager")

        self._initialize()
        self._init_meta_data_publishing()
        
    def _initialize(self):
        #self.initialize()
        self._motors = {}          
      
        #xtal = self.xtals.xtal_sel
        #if xtal is not None:
        #    self.xtal_sel = xtal # required by energy_motor controller : monochromator_calcmotor.EnergyCalcMotor ???
        #else:
        #    print ('xtal_change required')
            
        self._frozen = settings.HashSetting('mmspectro_tracking', default_values={})

    @property
    def motors(self):
        if self._motors:
            return self._motors
        
        self._motors["ene"] = self.energy_calc.energy_axis
        
        for axis in self.config.config_dict['axes']:
            tag = axis['tags'].split()[-1]
            if isinstance(axis['name'],str):
                self._motors [tag] = self.get_axis(axis['name'])
            elif isinstance(axis['name'], Axis):
                self._motors [tag] = axis['name']
            
        # if self._motors["ene"] is not None:
        #     self._motors["ene"].controller.set_mono(self)
        
        return self._motors 

    def _init_meta_data_publishing(self):
        """this is about metadata publishing to the h5 file"""
        if not self.name:
            user_warning(
                "to publish metadata the spectrometer needs a name in config"
            )
            return
        scan_meta_obj = get_user_scan_meta()
        scan_meta_obj.instrument.set(self, lambda _: {self.name: self.metadata()})

    def metadata(self):
        """ 
        this is about metadata publishing to the h5 file AND ICAT
        #self.reals[0].controller.ref_params['radius']
        #self.ref_params
        """
        
        meta_dict = dict()

        meta_dict.update  (
            {
                'crystal': self.xtal_sel,
                'dspacing': self.xtals.xtal[self.xtal_sel].d,
            }
        )
        for axis in self.reals:
            if isinstance(axis.controller, SpectroAnalyser):
                ananame = axis.controller.name
                meta_dict.update (
                    {
                        ananame: 'TRACKED' if self.is_tracked(ananame) else 'OFF',
                    }
                )

        meta_dict["@NX_class"] = "NXspectro"

        return meta_dict


            
    def _get_axis_from_tag (self, tag):
        for axis in self.config.config_dict['axes']:
            if tag in axis['tags'].split():
                if isinstance(axis['name'],str):
                    return self.get_axis(axis['name'])
                elif isinstance(axis['name'], Axis):
                    return axis['name']


    def calc_to_real(self, positions_dict):
        real_dict = dict()
        angle = positions_dict["bragg"]

        for axis in self.reals:
            tag = self._axis_tag(axis)
            if not np.isnan(angle) and self.is_tracked(axis.controller.name):        
                real_dict[tag] = angle
            #else:
            #    real_dict[self._axis_tag(axis)] = axis.position

        return real_dict

    def calc_from_real(self, positions_dict):
        return {"bragg" : positions_dict ['detector']} 


    def calculate (self, energy):
        info_str = f"Spectrometer: {self.name}\n\n"
        info_str += self.info_xtals()
        info_str += self.info_motor(energy)
        print (info_str)

    
    def __info__(self):
        info_str = f"Spectrometer: {self.name}\n\n"
        info_str += self.info_xtals()
        info_str += self.info_motor()
        return info_str

    def info_xtals(self):
        info_str = ""
        if self.xtals is not None:
            info_str += self.xtals.__info__()
        info_str += f"\n"
        return info_str
        
    def info_motor(self, enecalc = None):
        
        #self.motors["spectro_reals"] = self._tagged['real']
        #self.motors['bragg'] = self._get_axis_from_tag('bragg')

        bragg = self.motors["bragg"].position
        #energy = self.bragg2energy(bragg)

        #
        # TITLE
        #
        
        title = ["", "calculated", "current"]
        rows  = []
        
        # ENE POSITION ROW

        if self.motors["ene"] is not None:
            row = [self.motors["ene"].name]
            val = enecalc if enecalc is not None else self.motors["ene"].position
            unt = self.motors["ene"].unit
            row.append(f"{val:.3f} {'' if unt is None else unt}")
            #unt = self.motors["ene"].unit
            #val = self.motors["ene"].position
            #row.append(f"{val:.3f} {'' if unt is None else unt}")
            rows.append(row)
            
        # BR ROW
        row = [self.motors["bragg"].name]             #all crystal are the same
        val0 = self.energy2bragg(enecalc) if enecalc is not None else self.motors["bragg"].position
        unt = self.motors["bragg"].unit
        row.append(f"{val0:.3f} {'' if unt is None else unt}")
        if enecalc:
            val = self.motors["bragg"].position
            unt = self.motors["bragg"].unit
            row.append(f"{val:.3f} {'' if unt is None else unt}")
        rows.append(row)

        # DET and ANA ROWS
        #if self.motors["spectro_reals"] is not None:
        #    for spectroth in self.motors["spectro_reals"]:
        for spectroth in self._tagged['real']:
                controller = spectroth.controller
                try:
                    pos_dict = controller.calc_to_real({'bragg':val0})
                except RuntimeError as e:
                    print (e.args[0])
                    pos_dict = {}
                    
                rows.append([])
                rows.append([controller.__info__()])
                for axis in controller.reals:
                    row = [axis.name]
                    tag = controller._axis_tag(axis)
                    name = controller.name
                    if name in self._frozen and self._frozen[name] == True:
                        row.append("FROZEN")
                    else:
                        if tag in pos_dict:
                            pos = pos_dict[tag]
                        else:
                            pos = np.nan
                        unt = axis.unit    
                        row.append(f"{pos:.3f} {'' if unt is None else unt}")
                    row.append(f"{axis.position:.3f} {'' if unt is None else unt}")
                    rows.append(row)
                
                
        space = ["", "", "", "", "", "", "", ""]
        
        return tabulate.tabulate(rows, headers=title, tablefmt="plain")

    @property
    def energy_calc(self):
        return self.config.get("energy_calc", None)
    
    
    ###############################################################
    ###
    ### Xtals
    ###
    
    @property
    def xtals(self):
        return self.energy_calc.xtals

    @property
    def xtal_sel(self):
        return self.energy_calc.xtal_sel

    def xtal_change(self, xtal):
        return self.energy_calc.xtal_change(xtal)
     
    def energy2bragg(self, ene):
        return self.energy_calc.xtals.energy2bragg(ene)
        
    def bragg2energy(self, bragg):
        return self.energy_calc.xtals.bragg2energy(bragg)
        
    ################################################################
    ###
    ### Analysers Tracking
    ###
    
    def track_on(self, *names):
        all = list(map(lambda x:x.controller.name if isinstance(x.controller,SpectroAnalyser) else None, self.reals))
        all.remove(None)
        if len(names) == 0:
            self._frozen.clear()
        else:
            for name in names:
                if name in all:
                    if name in self._frozen:
                        self._frozen[name] = False
                else:
                    print(f"Analyser valid names are: {all}")
                    raise ValueError(f"{name} not a known analyser.")
                
    def track_off(self, *names):
        all = list(map(lambda x:x.controller.name, self.reals))
        if len(names) == 0:
            names = all
        for name in names:
            if name in all:
                if isinstance(self.reals[all.index(name)].controller, SpectroAnalyser):
                    self._frozen[name] = True
            else:
                all = list(map(lambda x:x.controller.name if isinstance(x.controller,SpectroAnalyser) else None, self.reals))
                all.remove(None)
                print(f"Analyser valid names are: {all}")
                raise ValueError(f"{name} not a known analyser.")
    
    def is_tracked(self, name):
        if name in self._frozen.keys():
            return not self._frozen[name]
        else:
            return True
        
    def tracked(self):
        all = list(map(lambda x:x.controller.name if isinstance(x.controller,SpectroAnalyser) else None, self.reals))
        all.remove(None)
        for key in self._frozen.keys():
            if self._frozen[key]:
                all.remove(key)
        return all


class SpectroDetector(CalcController):

    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)
        #self.radius = self.config.get("radius", float)

        self.angle_corry = self.config.get('angle_corry', default=0.0)
        self.angle_corrz = self.config.get('angle_corrz', default=0.0)
        
    def __info__(self):
            _infostr = f"{self.name}"
            return _infostr


    
class SpectroAnalyser(CalcController):
    def __init__(self, *args, **kwargs):
        
        CalcController.__init__(self, *args, **kwargs)
        
        #self.radius = self.config.get("radius", float) 
        self.xpos = self.config.get("xpos", float)
        self.angle_corry = self.config.get('angle_corry', default=0.0)
        self.angle_corrz = self.config.get('angle_corrz', default=0.0)

    def __info__(self):
        _infostr = f"{self.name}: radius {self.radius} y: {self.xpos} "
        return _infostr
     
