from bliss.common.standard import info
from bliss.config.wardrobe import ParametersWardrobe
from bliss.config.settings import QueueSetting, OrderedHashSetting, scan
from bliss.shell.cli.user_dialog import UserChoice, UserInput, UserIntInput, UserFloatInput
from bliss.shell.cli.pt_widgets import BlissDialog


def check_name (func):
    def wrapper (name):
        bli = name.split(':')
        if name.startswith('parameters:'):
            name = bli[0] +':'+ bli[1]
        else:
            name = 'parameters:' + bli[0]

        return func (name)
    return wrapper

@check_name
def purge_wardr (name):
    instances = QueueSetting(name)
    inst_names = instances.get()

    for instance in inst_names:
        pr = OrderedHashSetting("%s:%s" % (name, instance))
        pr.clear()
        instances.remove(instance)  # removing from Queue
    
    instances.clear()

@check_name    
def read_wardr (name):
    instances = QueueSetting(name)
    inst_names = instances.get()

    for instance in inst_names:
        print (instance)
        pr = OrderedHashSetting("%s:%s" % (name, instance))
        for k in pr.keys():
            print (k,pr[k])

def list_wardr (wildchared = '*'):
    for item in scan(f'parameters:{wildchared}'):
        print(item)
        

def switch_instance (func):
    def wrapper (*args, **kwargs):
        if args[0].instance_name:
            args[0].parameters.switch(args[0].instance_name)
        return func (*args, **kwargs)
    return wrapper

def _dialog (title, parameters_dict, callbacks ={}):
    uilist = []
    for param in parameters_dict:

        validator = parameters_dict [param]
        if isinstance (validator, int):
            uilist.append([UserIntInput (param , param + ' : ' ,parameters_dict[param])])
        elif isinstance(validator, float):
            uilist.append([UserFloatInput (param , param + ' : ' ,parameters_dict[param])])
        else:
            uilist.append([UserInput (param , param + ' : ' ,parameters_dict[param])])
               
    ret = BlissDialog( uilist, title=title).show()

    if ret is not False:
            
        for param in parameters_dict:
            if isinstance(parameters_dict[param], list):
                ret[param]=eval(ret[param])
        for param in callbacks:
            if param in ret:
                if ret[param] != parameters_dict[param]:
                    ret[param] = callbacks[param].__call__(ret[param])
            else:
                ret[param] = callbacks[param].__call__(ret)
                
    return ret 



class ID26Parameters:
    """
    Base class to handle beamline persistant parameters.
    """

    def __init__(self, param_name, param_defaults=None, no_setup=[], callbacks = {}):
        """
        Reads the parameter set from Redis and add the default values when necessary
        """
        self.param_name = param_name
        self.instance_name = None
        self.param_setup = {}
        self.callbacks = callbacks


        if self.param_name != None:
            self.parameters = ParametersWardrobe(self.param_name)

        if param_defaults != None:
            for k,v in param_defaults.items():
                    nosetup = (k in no_setup)
                    callback = callbacks [k] if k in callbacks else None
                    self._add(k, v, nosetup = nosetup, callback = callback)


    @switch_instance
    def __info__ (self):
        string = info(self.parameters)
        string = self.param_name + '  ' + string if self.instance_name is None else string
        return string
            
    def _add (self, key, value, nosetup = False, callback = None):

        if nosetup is True and key in self.param_setup:
            self.param_setup.pop(self.param_setup.index(key))
            
        if nosetup is False and key not in self.param_setup:
            self.param_setup[key]=value
            
        if key not in self.parameters.to_dict(export_properties=True):
            self.parameters.add (key, value)
        
        if callable(callback):
            self.callbacks[key]=callback
        else:
            if key in self.callbacks:
                self.callbacks.pop(key)
            
        
    @switch_instance
    def show(self):
        if self.param_name != None:
            self.parameters.show_table()


       
            
    @switch_instance
    def setup(self):
        if self.param_name is None:
            return

        title = self.instance_name + ' Setup' if self.instance_name else self.param_name + ' Setup'
        parameters_dict = self.parameters.to_dict(export_properties=True)
        ret = _dialog (title, self.param_setup, self.callbacks)

        #print ('parameters_dict, ie old stuff',parameters_dict)
        #print ('ret, just exiting menu',ret)
        
        if ret is not False:
            parameters_dict.update(ret)
            #print ('parameters_dict, updated with ret',parameters_dict)
            self.parameters.from_dict(parameters_dict)

    @switch_instance
    def save(self, directory, prefix = '', suffix = ''):
        """
        save in a file current instance of a parameter set         
        """
        if self.param_name != None and len(directory)>1:
            file_name = prefix + self.param_name + suffix +'.yml'
            sep = '' if directory [-1] == '/' else '/'
            file_name = directory + sep + file_name
            self.parameters.to_file(file_name)

            print ('Saving {2} instance of {1} on: {0}'.format(file_name, self.param_name, self.parameters.current_instance)) 

    @switch_instance
    def load(self, directory, prefix = '', suffix = ''):
        """
        loads from a previously saved file 
        directory must be specified
        loads only instance referenced by the calling object. 
        """
        if self.param_name != None and len(directory)>1:
            file_name = prefix + self.param_name + suffix +'.yml'
            sep = '' if directory [-1] == '/' else '/'
            file_name = directory + sep + file_name
            self.parameters.from_file (file_name, instance_name = self.parameters.current_instance) 


    @switch_instance
    def copy (self, instance_name):
        """
        """
        if instance_name in self.parameters.instances:
            instances = self.parameters._get_all_instances()
            copy_dict = instances [instance_name]
            self.parameters.from_dict(copy_dict)
        else:
            print ("Can't find instance '{0}' in parameters set {1}'" .format(self.instance_name, self.param_name)) 
           
            

    def _reset_parameters(self):
        """
        Deletes all parameters of the object from Redis
        to re-start with the default values
        """
        if self.param_name != None:
            parameters_dict = self.parameters.to_dict()
            for k in parameters_dict.items():
                self.parameters.remove("."+k[0])

        self.param_setup.clear()        
        self.callbacks.clear()        

        self.__init__(self.param_name)        




def list_wardr_parameters ():
    for item in scan("parameters:*id26params_*"):
        print(item)
        

class ID26ParametersInstance(ID26Parameters):

        def __init__(self, param_name, instance_name, defaults ={}):
            print("Initialising instance {0} of {1} parameters set ".format(instance_name, param_name))
            param_name = "id26params_" + param_name
            super().__init__(param_name, defaults)
            self.instance_name = instance_name

            
class ID26GlobalInstance (dict):

    def __init__ (self, param_name, instance_name, default_values={}):
        self._p = ID26ParametersInstance (param_name, instance_name, default_values)
        self._p.parameters.switch(instance_name)
        self.parameters = self._p.parameters #to have switch_instance decorator working
        self.instance_name = instance_name   #to have switch_instance decorator working
        super().__init__ (self._p.parameters.to_dict())

    @switch_instance
    def __setitem__ (self, key, value):
        if key not in self._p.parameters.to_dict():
            self._p.parameters.add(key,value)
        else:
            d=self._p.parameters.to_dict()
            d[key]=value
            self._p.parameters.from_dict(d)

        super().__setitem__ (key, value)

    def remove (self, *key):
        for k in key:
            self._p.parameters.remove('.{0}'.format(k))

    @switch_instance
    def copy (self, obj):
        self._p.parameters.from_dict(obj)
        self.update(obj.items())

    def show(self):
        self._p.show()



