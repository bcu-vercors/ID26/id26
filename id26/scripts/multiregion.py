import sys
from typing import Callable, List, Sequence, Tuple

import numpy

from bliss import setup_globals
from bliss.common.measurementgroup import _get_counters_from_measurement_group
from bliss.common.protocols import Scannable
from bliss.common.types import (
    _int,
    _float,
    _countables,
)
from bliss.common.utils import typecheck
from bliss.controllers.counter import CalcCounterController, SamplingCounterController
from bliss.controllers.ct2.client import CT2Controller
from bliss.controllers.lima.lima_base import Lima
from bliss.controllers.mca.base import BaseMCA
from bliss.scanning.scan import Scan
from bliss.scanning.chain import AcquisitionChain, ChainPreset, ChainIterationPreset
from bliss.scanning.toolbox import ChainBuilder
from bliss.scanning.acquisition.motor import VariableStepTriggerMaster
from bliss.scanning.scan_progress import ScanProgress

_region_intervals_list = Sequence[Tuple[_float, _float, _int, _float]]


class RegionChainIteratorPreset(ChainPreset):
    class RegionIterator(ChainIterationPreset):
        def __init__(
            self,
            last_region: int,
            current_region: int,
            iteration_nb: int,
            region_start_callback: callable,
            point_callback: callable,
        ):
            self._last_region = last_region
            self._current_region = current_region
            self._iteration_nb = iteration_nb
            self._region_start_callback = region_start_callback
            self._point_callback = point_callback

        def start(self):
            if self._point_callback:
                self._point_callback(self._iteration_nb)
            if self._current_region > self._last_region:
                if self._region_start_callback:
                    self._region_start_callback(self._current_region)

    def __init__(
        self,
        regions: _region_intervals_list,
        region_start_callback: callable,
        point_callback: callable,
    ):
        self._regions = regions
        self._region_start_callback = region_start_callback
        self._point_callback = point_callback

    def get_iterator(self, acq_chain):
        iteration_nb = 0
        last_region = 0

        while True:
            current_region = get_current_region(self._regions, iteration_nb)
            yield self.RegionIterator(
                last_region,
                current_region,
                iteration_nb,
                self._region_start_callback,
                self._point_callback,
            )
            iteration_nb += 1
            last_region = current_region


def multiregion_get_acquisition_chain(
    master,
    npoints: int,
    time_points: Tuple[_float],
    counter_args: _countables,
    mca_trigger_mode: str = "SOFTWARE",
    mca_block_size: int = 5,
    p201_master: CT2Controller = None
):
    chain = AcquisitionChain(parallel_prepare=True)
    builder = ChainBuilder(counter_args)

    acq_node_params = {
        "npoints": npoints,
        "acq_expo_time": time_points,
    }
    acq_child_params = {
        "count_time": 1,
        "npoints": 1,
    }

    node_p201 = None
    for node in builder.get_nodes_by_controller_type(CT2Controller):
        node.set_parameters(acq_params=acq_node_params)
        for child_node in node.children:
            child_node.set_parameters(acq_params=acq_child_params)

        if p201_master:
            if node.controller == p201_master:
                node_p201 = node
        else:
            node_p201 = node

        chain.add(master, node)

    if node_p201 is None and p201_master:
        raise RuntimeError(
            f"Could not find the specified CT2 controller `{p201_master.name}` designated as master in the acqusition chain"
        )

    if node_p201 is None:
        raise RuntimeError(
            "This scan needs a `CT2` like device with support for variable time per point"
        )

    acq_params = {
        "acq_nb_frames": npoints,
        # Need to set the expo time to the maximum of the scan to avoid trigger timeouts
        "acq_expo_time": max(time_points),
        "acq_trigger_mode": "EXTERNAL_GATE",
        "acq_mode": "SINGLE",
        "prepare_once": True,
        "start_once": True,
        "wait_frame_id": range(npoints)

    }
    for node in builder.get_nodes_by_controller_type(Lima):
        latency_time = node.controller.proxy.latency_time*1.01
        acq_params["acq_expo_time"] = max(time_points) - latency_time
        if npoints > 65535:
            raise RuntimeError("Number of frames larger than allowed maximum (65535)")
        node.set_parameters(acq_params=acq_params)
        chain.add(node_p201, node)

    acq_params = {
        "npoints": npoints,
        "trigger_mode": mca_trigger_mode,
        "read_all_triggers": False,
        "block_size": mca_block_size,
        "preset_time": npoints if mca_trigger_mode == "SOFTWARE" else 0.1,
    }

    for node in builder.get_nodes_by_controller_type(BaseMCA):
        node.set_parameters(acq_params=acq_params)
        chain.add(node_p201, node)

    acq_params = {
        "count_time": time_points,
        "npoints": npoints,
    }
    for node in builder.nodes:
        if isinstance(node.controller, SamplingCounterController):
            node.set_parameters(acq_params=acq_params)
            chain.add(node_p201, node)

    acq_params = {
        "count_time": time_points,
    }
    for node in builder.nodes:
        if isinstance(node.controller, CalcCounterController):
            node.set_parameters(acq_params=acq_params)
            chain.add(node_p201, node)

    return builder, chain


def generate_positions_from_regions(
    regions: _region_intervals_list,
) -> Tuple[List[_float], List[_float]]:
    motor_positions = []
    time_points = []
    for region in regions:
        region_motor_positions = numpy.linspace(region[0], region[1], region[2])
        time_points.extend(numpy.full(len(region_motor_positions), region[3]))
        motor_positions.extend(region_motor_positions)

    return (motor_positions, time_points)


def get_current_region(regions: _region_intervals_list, current_point: int) -> int:
    total_points = 0
    for region_id, region in enumerate(regions):
        region_points = region[2]
        total_points += region_points
        if current_point < total_points:
            return region_id

    return 0


@typecheck
def multiregion(
    motor: Scannable,
    regions: _region_intervals_list,
    *counter_args: _countables,
    stab_time: _float = None,
    mca_trigger_mode: str = None,
    mca_block_size: _int = None,
    run: bool = True,
    scan_info: dict = {},
    p201_master: CT2Controller = None,
    region_start_callback: Callable = None,
    point_callback: Callable = None,
) -> Scan:
    """A multregion scan with variable time per point

    multiregion(motor, (region, region), diode)

    Args:
        motor: Motor to scan
        regions: List of regions of format (start, stop, points, time)
        *counter_args: List of counters, will use ACTIVE_MG if empty

    Kwargs:
        region_start_callback: Callable(new_region: int) will be called when the region changes
        point_callback: Callable(point: int) will be called when the point changes

    """
    name = "multiregion"
    motor_positions, time_points = generate_positions_from_regions(regions)
    npoints = len(motor_positions)

    if not counter_args:
        measurement = setup_globals.ACTIVE_MG
        counter_args = _get_counters_from_measurement_group(measurement)

    master = VariableStepTriggerMaster(motor, motor_positions, stab_time=stab_time)
    builder, chain = multiregion_get_acquisition_chain(
        master,
        npoints,
        time_points,
        counter_args,
        mca_trigger_mode=mca_trigger_mode,
        mca_block_size=mca_block_size,
        p201_master=p201_master
    )
    print(chain._tree)

    scan_progress = MRScanProgress(regions, npoints, motor, builder)
    mr_scan_info = {
        "title": f"{name}: {motor.name} {regions} points: {npoints}",
        "type": name,
        **scan_info,
    }
    mr_scan_info.setdefault("instrument", {})
    mr_scan_info["instrument"]["multiregion"] = {
        "regions": regions,
        "@NX_class": "NXcollection",
    }

    scan = Scan(
        chain,
        name=name,
        scan_info=mr_scan_info,
        scan_progress=scan_progress,
    )

    # Allow adding hook when region changes
    region_preset = RegionChainIteratorPreset(
        regions=regions,
        region_start_callback=region_start_callback,
        point_callback=point_callback,
    )
    scan.acq_chain.add_preset(region_preset)

    if run:
        scan.run()

    return scan


class MRScanProgress(ScanProgress):
    def __init__(
        self, regions: _region_intervals_list, npoints: int, motor: Scannable, builder
    ):
        super().__init__()
        self.regions = regions
        self.npoints = npoints
        self.motor = motor
        self.controllers = [
            cont_node.acquisition_obj.name
            for cont_node in builder.get_top_level_nodes() if cont_node.acquisition_obj
        ]

    def scan_new_callback(self):
        title = self.scan_info["title"]
        print(f"\n  {title}\n")

        if self.motor is not None:
            display_str = f"   {self.motor.name:<8} "
        else:
            display_str = "  "

        for cont_name in self.controllers:
            if cont_name == "generic_tg_controller":
                cont_name = "tango"
            display_str += f" {cont_name:<8} "

        display_str += " Region "

        print(display_str)

    def progress_callback(self):
        if self.motor is not None:
            display_str = f"  {self.motor.position:8.4f} "
        else:
            display_str = "  "

        points = 0
        for acq_object in self.acq_objects:
            if isinstance(acq_object.device, Lima):
                last_image_ready = self.data.get(f"{acq_object.device.name}:last_image_ready", -1) + 1
                display_str += f" {last_image_ready:>8} "
            else:
                if acq_object.name in self.controllers:
                    if acq_object.channels:
                        first_channel = acq_object.channels[0]
                        try:
                            points = len(self._scan.streams[first_channel.fullname])
                        except Exception as e:
                            pass
                        display_str += f"{points:>8} "

        # points are zero offset, regions `re 1 offset for display`
        region = get_current_region(self.regions, points - 1 if points > 0 else 0)
        region_str = f"{region+1}/{len(self.regions)}"
        display_str += f" {region_str:>8}"

        print(display_str, end="\r")
        sys.stdout.flush()

    def on_scan_end(self, *args):
        self.progress_callback()
        print("\n")
