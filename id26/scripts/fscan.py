import numpy
import gevent
import functools

from bliss import setup_globals
from bliss.common.cleanup import cleanup, axis as cleanup_axis
from bliss.common.logtools import (
    log_error,
    log_warning,
    log_debug,
)

from id26.controllers.fscan_controller import IDlinkedConstantVelocity



try:
    FSCAN = setup_globals.fastscan
    FSCAN_PAR = FSCAN.parameters
    ACTIVE_MG = setup_globals.ACTIVE_MG
    FSCAN2 = setup_globals.fscanconfig_pm600.ftimescan_pm600
except Exception as e:
    print("cannot load fastscan:", e)
    FSCAN_PAR = {}
    FSCAN_PAR["id_linked"] = 0
    FSCAN_PAR["id_sync"] = 0



# SIM TESTS 240610
# from id26.dev.fscan_test import SIM_IDlinkedConstantVelocity as IDlinkedConstantVelocity
# type from with test session: FSCAN.dcm = setup_globals.dcm2


def _retry_fscan_once_on_exception(func):
    @functools.wraps(func)
    def wrapper(*a, **b):
        try:
            return func(*a, **b)
        except KeyboardInterrupt as e:
            raise e
        except Exception as e:
            log_warning(
                func,
                "_retry_fscan_once_on_exception: {0} failed with {1} exception, will retry once.".format(
                    func.__name__,
                    e,
                ),
            )
            gevent.sleep(0.1)
            FSCAN.reset()
            return func(*a, **b)

    return wrapper


def reset_id_sync():
    """
    Reset ID synchronisation with cable
    """
    FSCAN._reset_id_sync()


# @_retry_fscan_once_on_exception
def dfscan(
    energy_start,
    energy_end,
    duration,
    energy_step,
    *counters,
    save=True,
    #        save_images=None,
):

    if FSCAN.energy.position == numpy.nan:
        log_error("Energy position is nan - cannot move")
        print("Hints:")
        print(f" - Is your monochromator {FSCAN.dcm.name} correctly initialized ?")
        if FSCAN.dcm._xtals.xtal_sel is None:
            print(
                f" - Does it have a crystal selected ? ({FSCAN.dcm.name}.xtal_change (), is TY correctly positionned ?.) "
            )
        print(f" - try to move to absolute position {FSCAN.energy.name}")
        raise RuntimeError

    energy_start += FSCAN.energy.position
    energy_end += FSCAN.energy.position

    def _cleanup(value=FSCAN_PAR["mono_move_back"]):
        FSCAN_PAR["mono_move_back"] = value

    FSCAN_PAR["mono_move_back"] = 0
    axis_list = [FSCAN.mono]

    if FSCAN_PAR["id_linked"]:
        axis_list += list(FSCAN._get_id_tracked())

    with cleanup(_cleanup):
        with cleanup(*axis_list, restore_list=(cleanup_axis.POS,)):

            fscan(
                energy_start,
                energy_end,
                duration,
                energy_step,
                *counters,
                save=save,
                #                save_images=save_images,
            )


'''
# chasing OSError 24 @trap_OSError(*ser_list)
#

from id26.helpers.open_fds import trap_OSError

ser_list = [
    FSCAN.mono.controller.sock,
    FSCAN.opiom._boards['opiom_cc1']._cnx,
    ]

try:
    ser_list.append(setup_globals.qgchi2.controller.moco._cnx)
except Exception:
    pass
'''
#===================
# @_retry_fscan_once_on_exception
# @trap_OSError(*ser_list)
def fscan(
    energy_start,
    energy_end,
    duration,
    energy_step,
    *counters,
    save=True,
    # save_images=None,
    scan_technique=None,
    scan_info=None,
    **kwargs,
):
    """
    Start hardware triggered time scan on detectors with MUSST card as the master, but itself triggered by the PM600 controller and IDs.
    To select the detectors, enable/disable them using the ACTIVE_MG measurement group.
    Args:
        energy_start:
        energy_end:
        duration:
        energy_step:
        save: choose to save or not the data [default: True]
    Returns:
        Scan: the scan handler
    """

    log_debug(FSCAN, "mono is {0}".format(FSCAN.mono.state))

    with cleanup(FSCAN.mono, restore_list=(cleanup_axis.VEL,), error_cleanup=True):
        nbpoints, exp_time, duration, id_parameters = FSCAN.prepare(
            energy_start, energy_end, duration, energy_step
        )

        log_debug(FSCAN, "mono is {0}".format(FSCAN.mono.state))

        group = FSCAN.program_trajectories(
            energy_start,
            energy_end,
            duration,
            energy_step,
        )  # from 600us to some seconds for PM600

        # the data acquisition takes longer because of delay (0.5s) between MUSST start and actual motion of mono:
        nbpoints_musst = int(nbpoints + FSCAN_PAR["acq_add_time"] / exp_time)
        FSCAN._save_last_scan()  # call after scan prepartion is done and emptied in reset

    xname = "hdh_energy"

    scan_info = {
        "title": f"fscan {xname} {energy_start} {energy_end} {duration} {energy_step}",
        "start": energy_start,
        "stop": energy_end,
        "npoints": nbpoints,
        "type": "fscan",
        "count_time": exp_time,
    }

    if scan_technique:
        scan_info.update(scan_technique)
    if scan_info:
        scan_info.update(scan_info)

    """
    if save_images is None:
            FSCAN2.pars.save_images = FSCAN_PAR.get('save_images',None)

    """

    print("measuring", ACTIVE_MG.name)

    FSCAN2.pars.sampling_time = FSCAN_PAR.get("slow_sampling_time", 1.0)
    FSCAN2.pars.start_delay = FSCAN.synchro_delay
    FSCAN2.pars.save_flag = save

    FSCAN2.master._parameters["mono_trajectory"] = group.trajectories[0]
    FSCAN2.master._parameters["event_trajectory"] = FSCAN.event_trajectory_started
    FSCAN2.master._parameters["id_trajectory"] = IDlinkedConstantVelocity(
        {
            "id_speed_dict": id_parameters,
            "id_sync": FSCAN_PAR["id_sync"],
            "wago": FSCAN.wago,
            "id_max_speed": FSCAN_PAR["id_max_speed"],
        }
    )
    FSCAN2.master._parameters["id_linked"] = FSCAN_PAR["id_linked"]
    FSCAN2.master._parameters["duration"] = FSCAN_PAR["duration"]

    with cleanup(FSCAN._error_cleanup, error_cleanup=True):
        FSCAN2(
            exp_time,
            nbpoints_musst,
            scan_info=scan_info,
            x=f"{FSCAN2.master.musst.name}:{xname}",
        )
        FSCAN._cleanup()

    print('SCAN IS DONE.')

    return FSCAN2.scan


def energytosteps(energy):
    mono = FSCAN.dcm.motors["bragg"]
    return mono.user2dial(FSCAN.dcm.energy2bragg(energy)) * mono.steps_per_unit


def stepstoenergy(steps):
    mono = FSCAN.dcm.motors["bragg"]
    return FSCAN.dcm.bragg2energy(mono.dial2user(steps / mono.steps_per_unit))


def fscan_check_traj(e0):
    mr0 = FSCAN.mono.trajectory_prog.get().index("DP0") + 1
    mr1 = FSCAN.mono.trajectory_prog.get().index("EP0")
    ene = e0
    yield ene
    for mr in list(
        map(
            lambda x: int(x.replace("MR", "")),
            FSCAN.mono.trajectory_prog.get()[mr0:mr1],
        )
    ):
        ene = stepstoenergy(energytosteps(ene) + mr)
        yield ene
