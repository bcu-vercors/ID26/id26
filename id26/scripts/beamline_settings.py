import yaml
from bliss.common.standard import info
from bliss.config.settings import HashSetting, scan, logger

from id26.scripts.beamline_parameters import _dialog

KEY = 'id26Variables_'

def check_name (func):
    def wrapper (name):
        if name.startswith(KEY):
            name = name[len(KEY):]
        return func (name)
    return wrapper

@check_name
def purge_variables (name):
    instance = ID26Settings (name)
    instance.clear()

@check_name
def read_variables (name):
    instance = ID26Settings (name)
    print (info(instance))

def list_variables():
    for item in scan(f"*{KEY}*"):
        print(item)

def str2(bli):
    return str(bli)


class ID26Settings(HashSetting):
    """
    Base class providing tools to handle persistant variables.
    default values are nor stored in redis.    
    callbacks are called from setup dialod UI when the parameter they attach to is changed.
    nosetup prevents some parameters to appear in setup dialog UI.

    Usage :
    
    myvars = ID26Settings ('script1_keys', defaults = {
             'key1': None, 
             'key2': None, 
    })

    myvars.setup()
    myvars.save()
    myvars.load()
    myvars.remove(key)
    myvars.clear()
 
    """
    
    def __init__(self, name, defaults = {}, callbacks = {}, nosetup =[]):
        """
        Reads the parameter set from Redis if any and add the default values when necessary; 
        configure setup dialog UI with callbacks and nosetup
        """
        super().__init__(KEY+name, default_values=defaults,write_type_conversion=str2)

        self._param_name = name
        self._call_backs = callbacks.copy()
        self._no_setup = nosetup

        for k,v in callbacks.items():
            if k not in defaults:
                print (f'{k} not in default values - {self._call_backs.pop(k)} callback removed')
            elif not callable(v):
                print (f'{k}:{self._call_backs.pop(k)} not a valid callback')
                
        for k in nosetup:
            if k not in defaults:
                self._no_setup.pop(k)

                    
    def __info__(self):
        string = f'{type(self).__name__}: {self._param_name} \n'
        string += '* means default value not stored in Redis\n\n'
        string += 'values:\n'
        for k in sorted(self.keys()):
                v = self[k]
                string += f'- {k} : {v} {"*" if self.connection.hget(self.name,k) is None else ""} \n'
        return string

    def __dir__(self):
        return 'setup', 'save', 'load', 'remove', 'clear'
        
    def __setitem__(self, key, value):
        if key in self._call_backs:
            value = self._call_backs[key].__call__(value)
        super().__setitem__(key, value)


    def update(self, values):
        for k in values.keys():
            if k in self._call_backs:
                values[k] = self._call_backs[k].__call__(values[k])
        super().update(values)

    def setup(self):
        '''
        setup dialog UI 
        '''
        title = self._param_name + ' Setup' 
        parameters_dict = self.get_all() 
        for key in self._no_setup:
            if key in parameters_dict:
                parameters_dict.pop(key)

        ret = _dialog (title, parameters_dict)

        if ret is not False:
            self.update(ret)

    def save(self, directory, prefix = '', suffix = ''):
        """
        save to yml file
        directory (path) must be specified
        a prefix or a suffix can be added
        filename is : {prefix}id26Variable_{name}{suffix}.yml
        """
        if len(directory)>1:
            file_name = prefix + self.name + suffix +'.yml'
            sep = '' if directory [-1] == '/' else '/'
            file_name = directory + sep + file_name

            data_to_dump = {
                f'{type(self).__name__}': self._param_name ,
                "values": self.get_all(),
                }
 
            yml_data = yaml.dump(data_to_dump, default_flow_style=False, sort_keys=False)
            with open(file_name, "w") as file_out:
                file_out.write(yml_data)
            print (f'{self._param_name} saved to file:{file_name}')    
        

    def load(self, directory, prefix = '', suffix = '', donotupdate = False):
        """
        load from yml file
        directory (path) must be specified
        a prefix or a suffix can be added
        filename is : {prefix}id26Variable_{name}{suffix}.yml
        donotupdate is True: only read the file, do not store its content into variables.
        """
        if len(directory)>1:
            file_name = prefix + self.name + suffix +'.yml'
            sep = '' if directory [-1] == '/' else '/'
            file_name = directory + sep + file_name

            with open(file_name) as file:
                try:
                   
                    dict_in = yaml.load(file.read(), Loader=yaml.FullLoader)
                    if dict_in.get("ID26Settings") != self._param_name:
                        logger.warning("Setting name is different")
                        
                    values = dict_in["values"]

                    if donotupdate is False: 
                        print ("Loading '{0}' from '{1}'".format(self._param_name, file_name))
                        self.update(values)
                    else:
                        print ("file '{1}' content {0}".format(dict_in, file_name))

                    
                except KeyError:
                    print ("file '{0}' not a ID26Settings file." .format(file_name)) 


