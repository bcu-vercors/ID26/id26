import numpy as np
import matplotlib.pyplot as pl

from silx.math.fit import fitmanager

# define our fit function: a generic polynomial of degree 4
def linear_fcn(x, x0, a):
    return x0 + x*a

# define an estimation function to that returns initial parameters
# and constraints
def esti(x, y):
    p0 = np.array([0., 1])
    cons = np.zeros(shape=(5, 3))
    return p0, cons


# main function that uses the calibration
def det_dt_correct(raw_val, normalization_time, fit_tau, fit_c):
    # takes measured detector counts (whatever the counting time)
    # counting time is needed because fit_tau is calculated/defined for countrate (i.e. counts/sec)
    # returns corrected array
    

    if (normalization_time.any() == 0):
    # this should never happen anyways!
        print("det_dt_correct : ERROR : division by zero : normalization_time is null  :   value set to 0 !!!!!!!!!!!!!!!!!!!!!!!!")
        return None
    else:
        if ((1 - ((raw_val/normalization_time) * fit_tau) ).any() ==0 ):
            # this would happen if det*tau close to 1, then anyways correction invalid!
            print("det_dt_correct : ERROR : division by ZERO : (1 - ((raw_val/normalization_time) * fit_tau) is null)")
            return None
        else:
            _corrected_val = raw_val / (1 - ((raw_val/normalization_time) * fit_tau ))
    return _corrected_val



def det_dt_calib_fit(det_dat, mon_dat, time_dat, mon_min = 10000, plotting = True):
    '''
    Function that fits the det_dt_calib data and returns the fit result.
    Parameters:
    det_dat: np.array(length n)
    mon_dat: np.array(length n)
    time_dat: np.array(length n)
    mon_min: float
    - minimum value of the monitor counts for the point to be used in the fit.
    Default value 10000.    (question of statistics)
    
    Returns:
        fit_tau, fit_c
# TODO: where to store the result?
        '''
 
    valid_mon_dat = mon_dat[mon_dat>=mon_min]  # this kicks out data where I0 is less than mon_min because below that dark counts become important
    print(f'Number of points used : {len(valid_mon_dat)}/{len(mon_dat)}')
    valid_det_dat = det_dat[mon_dat>=mon_min]
    valid_time_dat = time_dat[mon_dat>=mon_min]

  
    x_data = valid_det_dat/valid_time_dat
    y_data = valid_det_dat/valid_mon_dat


    # Fitting
    # pay attention:
    #fitting is using det/t as x and det/mon as y
    fit = fitmanager.FitManager()
    fit.setdata(x=x_data, y = y_data)
    fit.addtheory("linear",
                  function=linear_fcn,
                  parameters=["a0", "a1"],
                  estimate=esti)
    fit.settheory('linear')
    fit.configure(WeightFlag=True)
    fit.estimate()
    fit.runfit()
    start_dat = fit.gendata(x_data, estimated = True)
        # TODO: plot to Flint: #ax.plot(x_data,start_dat, label = 'estimate')
        # TODO: what if fit not successful?
    yfit = fit.gendata(x_data)
    a0, a1 = fit.get_fitted_parameters()
    fit_tau = -a1/a0
    fit_c = a0
    det_dat_corr = det_dt_correct(valid_det_dat, valid_time_dat, fit_tau, fit_c) # in principle here needed only for the plotting
    print('tau',fit_tau, fit_c)


# plotting
# pay attention:
#fitting is using det/t as x and det/mon as y, plotting is using:
#panel 0:  showing fit, i.e. mon/t as x and det/mon as y
#(for clarity not the x from fit)
# last panel is loglog!
    if plotting == True:
        fig,axarr = pl.subplots(nrows= 3, ncols = 1, figsize = (6,10))
        axarr[0].plot(valid_mon_dat/valid_time_dat, y_data,'o', color = 'magenta', label = 'raw data (used)')
        axarr[0].scatter(mon_dat/time_dat,det_dat/mon_dat,  edgecolors = 'magenta', facecolors = "none", label = 'non-valid data')
        axarr[2].loglog(valid_mon_dat/valid_time_dat,valid_det_dat,'o', color = 'magenta', label = 'raw')
        axarr[1].plot(valid_mon_dat/valid_time_dat,valid_det_dat,'o', color = 'magenta', label = 'raw')  
        axarr[0].plot(valid_mon_dat/valid_time_dat,yfit,'-',color = 'magenta', label = 'linear fit')
        axarr[2].loglog(valid_mon_dat/valid_time_dat,yfit*valid_mon_dat,'-', color = 'magenta', label = 'fit')
        axarr[1].plot(valid_mon_dat/valid_time_dat,yfit*valid_mon_dat,'-', color = 'magenta', label = 'fit')
        axarr[2].loglog(valid_mon_dat/valid_time_dat,det_dat_corr,'-o', color = 'blue', label = 'corrected')
        axarr[1].plot(valid_mon_dat/valid_time_dat,det_dat_corr,'-o', color = 'blue', label = 'corrected')
        axarr[0].set_ylim((np.min(y_data), np.max(y_data)))
        axarr[0].set_xlabel('Time normalized mon counts')
        axarr[0].set_ylabel('det/mon counts')
        axarr[1].set_xlabel('Time normalized mon counts')
        axarr[1].set_ylabel('Detector counts')
        axarr[0].legend()
        axarr[1].legend()
        axarr[2].set_xlabel('Time normalized mon counts (log)')
        axarr[2].set_ylabel('Detector counts (log)')
        pl.show()
        input('Are you happy (I do not care and close the window no matter what)?')
        pl.close('all')
    # back to the main function
    return fit_tau, fit_c


