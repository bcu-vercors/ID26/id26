import os
from bliss import current_session

from id26.controllers.xas import Xanes, Exafs
from id26.controllers.xes import Xes, Rixs, CorrectionScan
from id26.controllers.measurement import Circle, Rectangle

CLASSES = {
        'xanes': Xanes,
        'exafs': Exafs,
        'xes': Xes,
        'rixs': Rixs,
        'cc_scan': CorrectionScan,
}

PATTERN_SHAPES = {
        'circle': Circle,
        'rectangle': Rectangle,
        }


class ExpDict (dict):
        pass


def generate (hutch):
        scan_saving = current_session.scan_saving
        env_dict = current_session.env_dict
        
        scan_path = scan_saving.base_path + '/'
        scan_path += scan_saving.proposal_dirname + '/'
        scan_path += scan_saving.beamline + '/'

        print(f'scanning {scan_path}parameteriser files ...')
        
        path = scan_path + 'parameteriser/experiment/'
        _generate_experiment_objects (path, env_dict)

        path = scan_path + 'parameteriser/measurement/'
        _generate_measurements_objects (path, env_dict, hutch)

        print ("\n=== sample patterns: ===\n")
        path = scan_path + 'parameteriser/sample/'
        _generate_sample_patterns_objects (path, env_dict, hutch)

        print ("\n=== generated objects: ===\n")
        list_objects()
        
def _generate_experiment_objects (path, env_dict):

        
        for filename in os.listdir(path):
            if filename.endswith('.yml'):
                name = filename.split('.yml')[0].split(f'experiment_')[1]
                ii = ExpDict()
                env_dict.update({f'{name}': ii})


def _generate_measurements_objects (path, env_dict, hutch):
        
        for filename in os.listdir(path):
            if filename.endswith('.yml'):
                for _type in CLASSES.keys():
                        if filename.startswith(_type):
                                scan_type = _type
                                break
                        else:
                                scan_type = "not found or"
                try:
                        Klass = CLASSES [scan_type]
                except Exception as e:
                        print (f'{filename}: {e} - DISCARDED')
                        continue
                name = filename.split('.yml')[0].split(f'{scan_type}_')[1]
                ii = Klass(name, hutch)
                env_dict.update({f'{name}_{scan_type}': ii})
                try:
                        if scan_type == 'cc_scan':
                                ii.main_scan = env_dict[ii.get('main_scan')]
                except KeyError as e:
                        print(f'ERROR: {name}_{scan_type}: {e} is not an existing scan (yet).')
                except Exception as e:
                        print(f'ERROR: {name}_{scan_type}: {e}')

def _generate_sample_patterns_objects (path, env_dict, hutch):
        
        pattern_name_list = list()
        for filename in os.listdir(path):
            if filename.endswith('.yml'):
                shape, name = filename.split('.yml')[0].split(f'_')
                try:
                        Klass = PATTERN_SHAPES [shape]
                except Exception as e:
                        print (f'{filename}: {e} - DISCARDED')
                        continue
                ii = Klass(name, hutch)
                env_dict.update({f'{name}': ii})
                #print(f'{filename} generate object {name}:{ii.__class__}')
                try:
                        junk = ii.create_pattern (verbose=True)
                        pattern_name_list.append(name)
                except Exception as e:
                        print (e)
                
        env_dict.update({f'all_sample_patterns': pattern_name_list})
        print(f'\nall_sample_patterns: {pattern_name_list}')



                
def list_objects ():
        env_dict = current_session.env_dict
        
        for k, v in env_dict.items():
                if isinstance(v, ExpDict):
                        print('experiment:', k)
                        continue
                for n, c in CLASSES.items():
                        if isinstance(v,c):
                                try:
                                        junk = v.check(verbose=False)
                                        msg = f'{n}: {bcolor.BOLD}{k}{bcolor.END} - READY'
                                except Exception as e:
                                        msg = f'{bcolor.RED}{n}: {k} - faulty{bcolor.END} : {e}'
                                print(msg)
                                break

                        
class bcolor:
    PURPLE = "\033[95m"
    CYAN = "\033[96m"
    DARKCYAN = "\033[36m"
    BLUE = "\033[94m"
    GREEN = "\033[92m"
    YELLOW = "\033[93m"
    RED = "\033[91m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"
    END = "\033[0m"
