import numpy as np

from bliss.common.standard import info
from bliss import setup_globals
from bliss.shell.cli.user_dialog import UserChoice
from bliss.shell.cli.pt_widgets import BlissDialog, button_dialog
from bliss.common.scans import timescan
from bliss.shell.standard import umv
from bliss.scanning import scan
from bliss import current_session
from bliss.common.logtools import log_warning
from id26.scripts.beamline_settings import purge_variables

from id26.scripts.position_instances import PositionInstances 
from id26.scripts.beamline_parameters import switch_instance
from id26.scripts.fscan import fscan, reset_id_sync
from id26.scripts.beamline_settings import ID26Settings, list_variables, purge_variables
from id26.controllers.radiation_damage import axes2counters, PositionManager , RadiationDamagePreset

U35A = setup_globals.u35a
U35B = setup_globals.u35b
U35C = setup_globals.u35c
T35A = setup_globals.t35a
T35B = setup_globals.t35b
ENEUND = setup_globals.eneund
ENERGY = setup_globals.energy
FSCAN_PAR = setup_globals.fscan_par
DEFAULT_POSITION_MANAGER = setup_globals.pm
MONO =  setup_globals.mono
FASTSCAN = setup_globals.fastscan
ACTIVE_MG = setup_globals.ACTIVE_MG

# choose slits, spectrometer, ... according to the session / hutch in use
if setup_globals.current_session.name in ['optics', 'eh2']:
            SVG = setup_globals.e2vg
            SHG = setup_globals.e2hg
            XES_EN = setup_globals.xes_en
elif setup_globals.current_session.name in ['texs']:
            SVG = setup_globals.tsvg
            SHG = setup_globals.tshg
            XES_EN = setup_globals.xes_en_texs





# usage:
# myxanes = Xanes('myxanes')
# myexafs = Exafs('myexafs')
# myxanes.scan(60)

def empty_serial_line():
    try:
        ans = MONO.controller.sock.readline()
        while(ans):
            log_warning(
              MONO,
              "empty_serial_line:{0}".format(ans)
            )
            ans = MONO.controller.sock.readline()
    except:
        pass

def move_undulators(energy_target):
    #umv(eneund, energy_target) # this would move also mono, not needed
    # we need only to move "tracked" undulators to positions corresponding to "energy_target"
    xas_pos = PositionInstances('start_energy',U35A, U35B, U35C)
    # get a list of "tracked" undulators:
    undu_list = []
    motion = ()
    for one_undu in setup_globals.get_available_undulators():
        print(one_undu.name)
        if (one_undu.name[0] == 'u' and one_undu.track() == True):
            undu_list = undu_list+ [one_undu.name]
    # calculate desired gap openings for each "tracked" undulator and save into "xas_pos" Position Instance
    for undname in undu_list:
        pos = setup_globals.name_to_obj(undname).position
        calc_position = ENEUND.controller.undu.energy2undulator(setup_globals.name_to_obj(undname),energy_target)
        if abs(calc_position-pos) > ENEUND.controller.approx:
          xas_pos.position(setup_globals.name_to_obj(undname),calc_position)
          motion += (setup_globals.name_to_obj(undname), calc_position)             
    xas_pos.move(*motion)

class ID26Measurement(ID26Settings):
    def __init__(self, name=None, position_manager = None):
        # in most of the cases we do not want to save anything to redis!
        # so, give it "dummy" name
        if name is None:
            name = 'dummy' 
        redis_name = 'id26Variables_'+name
        super().__init__(name,  defaults=self.build_default_parameters())
        try: 
            purge_variables(redis_name)
            print(f'{redis_name} existed in Redis, it has just been cleared.')
        except:
            pass        
        
        self._current_sample = None
        self._id26_experiment = None
        if position_manager is None:
            self._position_manager = DEFAULT_POSITION_MANAGER
        else:
            self._position_manager = position_manager
        self._move_scan_mode = False  # False not to move. "in" to move within a scan, "between" to move between scans
        # these are needed for concentration correction
        # possible problem: current values not retained if bliss restarted!
        # should they be put into the dictionary params?
        
        self._first_correction_index = None
        self._nbpts = None
    
    def build_default_parameters(self):
        default_params = {}
        default_params['t35a'] = None
        default_params['t35b'] = None
        default_params['shg'] = None
        default_params['svg'] = None
        default_params['number_scans'] = None
        default_params['retry'] = False
        # these needed to move on the sample
        default_params['sample_mot1'] = None
        default_params['sample_mot2'] = None
        # these needed for correction scans
        default_params['energy_excite_above'] = None
        default_params['xes_main_max'] = None

        return default_params
        
    def scan(self, *args, **kwargs):
        raise NotImplementedError
    
    
    @property
    def current_sample(self):
        return self._current_sample
        
    @current_sample.setter
    def current_sample(self, pname):
        if self._position_manager.has_pattern(pname):
            self._current_sample = pname
            xmot = current_session.config.get(self.get_param('sample_mot1'))
            ymot = current_session.config.get(self.get_param('sample_mot2'))
            self._radiation_damage = RadiationDamagePreset(xmot, ymot, self._position_manager, pname)

    @property
    def first_correction_index(self):
        return self._first_correction_index
        
    @first_correction_index.setter
    def first_correction_index(self, value):
        if self._current_sample:
            self._position_manager.set_index(self._current_sample, value)

    @property
    def correction_points(self):
        return self._nbpts
        
    @correction_points.setter
    def correction_points(self, value):
        if self._current_sample:
            self._nbpts = value



    @property
    def move_scan_mode(self):
        return self._move_scan_mode
        
    @move_scan_mode.setter
    def move_scan_mode(self, value):
        if value not in [False,'in', 'between']:
            raise ValueError(f"scan_mode should be in [False,'in', 'between']")
        
        self._move_scan_mode = value

    @property
    def id26_experiment(self):
        return self._id26_experiment
        
    @id26_experiment.setter
    def id26_experiment(self, value):
    # TODO: add check that value of correct type
        self._id26_experiment = value
        
    def goto_index(self, index):
        if self._current_sample:
            self._position_manager.set_index(self._current_sample, index)
            
    def get_current_index(self):      
        return self._position_manager.get_index(self._current_sample)
        
    def get_param(self, key):
        value = self.get(key)
        if value is None:
            
            if self._id26_experiment is not None:
                value = self._id26_experiment.get(key)
                if value is None:
                    print(f"The parameter {key} is None !")
                    return None
            else:
                print(f"id26_experiment is None!")
                return None
        else:
            # we need to identify a list passed to the function. It gets automatically converted into a string: we need to convert back
            if isinstance(value, str):
                value = value.strip()
                if value.startswith('['):
                    return [ float(x) for x in value[1:-1].split(',')]
        
        return value
        
        

class ID26XAS(ID26Measurement):

    def __init__(self, name = None):
        # in most of the cases we do not want to save anything to redis!
        # so, give it "dummy" name
        if name is None:
            name = 'dummy' 
        redis_name = 'id26Variables_'+name
        super().__init__(name)
        try: 
            purge_variables(redis_name)
            print(f'{redis_name} existed in Redis, it has just been cleared.')
        except:
            pass
                    
    def build_default_parameters(self):
        default_params = {}
        default_params['energy_edge'] = None
        default_params['shg'] = None
        default_params['svg'] = None
        default_params['start_energy'] = None
        default_params['end_energy'] = None
        default_params['int_energy'] = None
        default_params['scan_time'] = None
        default_params['number_scans'] = None
        default_params['xes_en'] = None
        default_params['retry'] = False
        return default_params

 
    def scan(self, total_scanning_time = None, number_of_scans = None, xes_energy = None, retry = None):
        '''
        "Spec Usage: xanes|exafs|pre total_scanning_time [number of scans] [xes_en]"
        "or define reasonable scan paramters in GUI."
        XANES_SCAN_TIME = $1 # must be >0
        XANES_NUMBER_SCANS = $2 # defaults to 1, cannot be 0
        XANES_XES_EN = $3 # defaults to 0 
        '''

        if (total_scanning_time==0):
            raise RuntimeError(
                "total_scanning_time cannot be 0"
        )    

        if total_scanning_time is None:
            total_scanning_time = self.get_param('scan_time') 
        if number_of_scans is None:
            number_of_scans = self.get_param('number_scans')
        if xes_energy is None:
            xes_energy = self.get_param('xes_en')

        if retry is None:
            retry = self.get_param('retry')
        
        if self.get_param('svg') is not None:
            SVG.move(self.get_param('svg'))
        else:
            print('svg not given, NOT moving vertical slit')
        if self.get_param('shg') is not None:
            SHG.move(self.get_param('shg'))
        else:
            print('shg not given, NOT moving horizontal slit')
            
        if xes_energy is not None: #either given in scan argument or defined in the 'xes_en' key
            self.move_spectrometer(xes_energy)
        else:
            print('xes_energy not given, NOT moving the spectrometer')            
        self.hook_pre_fscanloop()

        # make sure all parameters for scan are defined:
        if self.get_param('start_energy') is None:
            raise ValueError('start energy not defined. Please, add the [\'start_energy\'] dictionary item.')
        if self.get_param('end_energy') is None:
            raise ValueError('end energy not defined. Please, add the [\'end_energy\'] dictionary item.')
        if self.get_param('int_energy') is None:
            raise ValueError('int energy not defined. Please, add the [\'int_energy\'] dictionary item.')                    
           
        print("Will do {0} loop(s)".format(number_of_scans))
        for i in range(number_of_scans):
            if retry:
                ok = False
                while not ok:
                    try:
                        self.hook_fscanloop()
                        print("NOT IMPLEMENTED YET check gauges 5 and open valve 4")
                        my_fscan = fscan(self.get_param('start_energy'),self.get_param('end_energy'),total_scanning_time,self.get_param('int_energy'))
                        self.hook_post_fscanloop()
                        ok = True
                    except scan.ScanAbort as e:
                        print('Scan abort')
                        raise e
                    except KeyboardInterrupt as e:
                        print('KeyboardInterrupt')
                        raise e

                    except:
                        ok = False
                        empty_serial_line()
            else: # no retry
                self.hook_fscanloop()
                my_fscan = fscan(self.get_param('start_energy'),self.get_param('end_energy'),total_scanning_time,self.get_param('int_energy'))
                self.hook_post_fscanloop()
        return my_fscan

    def hook_pre_fscanloop(self):
        if self._move_scan_mode == 'between':
            x, y = self._position_manager.get_next_position(self._current_sample)
            # and assign "first index attribute"
            self._first_correction_index = self.get_current_index()+1  # really +1 ?
            print(f"===== moving ({self.get_param('sample_mot1')}, {self.get_param('sample_mot2')}) to ({x}, {y}) (counts={self._position_manager.get_cycle_counts(self.current_sample)})\n")
            umv(self._radiation_damage.xmot, x, self._radiation_damage.ymot, y)
            
    def hook_post_fscanloop(self):
        pass
        # how to make sure I go back to the original energy position?

    def hook_fscanloop(self):
        pass
          
    def move_spectrometer(self, xes_energy):
        if xes_energy:
            print(f'when correctly implemented, moving {XES_EN.name} to {xes_energy}')
# TODO: make sure the move moves all real axis in the xes_en!!!            
            XES_EN.move(xes_energy)
            
       
    def auto_offset (self):
        print("NOT IMPLEMENTED YET auto_offset")

    def rocking_curve(self):
        pass
    
    def beam_check(self):
        print("NOT IMPLEMENTED YET beam_check")
        
class Xanes(ID26XAS):
    '''
    X-ray Absorption Near-Edge Spectroscopy 
    During scan, the undulator gap remains fixed at the energy chosen in energy_edge
    (a good value is to set it at 2/3 of the scan range). 
    '''

    def hook_pre_fscanloop(self):
        super().hook_pre_fscanloop()
        if self['energy_edge'] is not None:
            move_undulators(self.get_param('energy_edge'))
        if self.get_param('t35a') is not None:
            T35A.move(self.get_param('t35a'))    
#        self.auto_offset() # not done here
        FSCAN_PAR['id_linked'] = 0
        FSCAN_PAR['id_sync'] = 0
        print ("NOT IMPLEMENTED YET Xmap activation")

    def hook_post_fscanloop(self):
        print ("NOT IMPLEMENTED YET Xmap de-activation")

    def rocking_curve(self):
        print ("XANES NOT IMPLEMENTED YET rc: mv energy start_energy;rc") #rc includes beamcheck
        
class Exafs(ID26XAS):
    '''
    Extended X-ray Absorption Fine Structure Spectroscopy 
    During scan, the undulator gap changes to maximize the incoming beam flux. 
    '''
    
    #def build_default_parameters(self):
    #    default_params = super().build_default_parameters()
    #    default_params['id_linked'] = None
    #    default_params['undu_couple'] = None
    #    return default_params

# decorator commented off by BD because it was not working. No idea why! instance_name was missing ...

#    @switch_instance
    def rocking_curve(self):
        print ("EXAFS NOT IMPLEMENTED YET rc: mv energy end_energy") #rc includes beamcheck
        move_undulators(self['end_energy'])
        print ("EXAFS NOT IMPLEMENTED YET rc: rc") #rc includes beamcheck

#    @switch_instance
    def hook_fscanloop(self):
        self.auto_offset ()
        FSCAN_PAR['id_linked'] = 1
        FSCAN_PAR['id_sync'] = 1
        # make it always true(slave_on): TODO figure out how to handle this later...
        if self.get_param('retry') is True:
            ok = False
            while not ok:
                try:
                    if self.get_param('t35a') is not None:
                        #T35A.move(self.get_param('t35a'))
                        umv(T35A, self.get_param('t35a'))
                    ok = True
                except:
                    pass
        else: # no retry
            if self.get_param('t35a') is not None:
                #T35A.move(self.get_param('t35a'))    
                umv(T35A, self.get_param('t35a'))
                       
        if self.get_param('retry') is True:
            ok = False
            while not ok:
                try:
                    if self.get_param('t35b') is not None:
                        #T35B.move(self.get_param('t35b'))
                        umv(T35B, self.get_param('t35b'))
                    ok = True
                except:
                    pass
        else: # no retry
            if self.get_param('t35b') is not None:
                #T35B.move(self.get_param('t35b'))     
                umv(T35B, self.get_param('t35b'))                    
                        
            

#    @switch_instance
    def hook_post_fscanloop(self):
# TODO: how to regain the original "id_linked" and "id_sync" values?
        FSCAN_PAR['id_linked'] = 0
        FSCAN_PAR['id_sync'] = 0


        
class ID26StepXAS(ID26Measurement):

    def __init__(self, name):
        super().__init__(name)
        
    def build_default_parameters(self):
        default_params = {}
        default_params['shg'] = None
        default_params['svg'] = None
        default_params['start_energy'] = None
        default_params['end_energy'] = None
        default_params['int_energy'] = None
        default_params['energy_edge'] = None
        default_params['xes_en'] = None
        default_params['acq_time'] = None
        default_params['number_scans'] = None
        default_params['sample_mot1'] = None
        default_params['sample_mot2'] = None
        default_params['retry'] = False
        return default_params
        
    def auto_offset (self):
        print("NOT IMPLEMENTED YET auto_offset")

 
    def scan(self, acq_time = None, number_of_scans = None, start_energy = None, end_energy = None, int_energy= None,xes_energy = None):
        '''
        step scan XAS-type
        '''
        
        # first save energy position 
        ENERGY.sync_hard()
        energy_before= ENERGY.position 
        
        # retain original tracking values    
        u35a_track = U35A.track()
        u35b_track = U35B.track()
        u35c_track = U35C.track()          
            
        # also undulators positions
        before_step_xas_pos = PositionInstances('before_step_xas',U35A, U35B, U35C)
        before_step_xas_pos.update(U35A, U35B, U35C)

        
        # tolerance value for undulators position
        
        tol = 0.02     
        
        # first check all necessary params/arguments defined 
        if (acq_time==0):
            raise RuntimeError("Acquisition time cannot be 0")
            
        if acq_time is None:
            acq_time = self.get_param('acq_time')
            
        if acq_time is None:
            raise ValueError('Acquisition time not defined')

        if number_of_scans is not None:
            _number_of_scans = number_of_scans
        else:
            _number_of_scans = self.get_param('number_scans')
        if _number_of_scans is None: # if not defined by user at all
            _number_of_scans = 1
            
        if start_energy is None:
            start_energy = self.get_param('start_energy')
            
        if end_energy is None:
            end_energy = self.get_param('end_energy')
            
        if int_energy is None:
            int_energy = self.get_param('int_energy')

        # motion of undulators is taken care of in hook_pre_ascanloop
       
        if xes_energy is None:
            xes_energy = self.get_param('xes_en')
                    
        if xes_energy is not None: #either given in scan argument or defined in the 'xes_en' key
            umv(XES_EN, xes_energy)
        else:
            print('xes_energy not given, NOT moving the spectrometer')            
        
        #if (xes_main_max is None and self._move_scan_mode == 'in'):
            #print('Xes_main_max is not defined for the correction scan')
            #raise         
           
        if self.get_param('svg') is not None:
            SVG.move(self.get_param('svg'))
        if self.get_param('shg') is not None:
            SHG.move(self.get_param('shg'))
        
            # make sure all parameters for scan are defined:
        if start_energy is None:
            raise ValueError('Start energy not defined. Please, add the [\'start_energy\'] dictionary item.')
        if end_energy is None:
            raise ValueError('End energy not defined. Please, add the [\'end_energy\'] dictionary item.')
        if int_energy is None:
            raise ValueError('Step energy not defined. Please, add the [\'int_energy\'] dictionary item.')           
 
        self.hook_pre_ascanloop()
        print("Will do {0} loop(s)".format(_number_of_scans))
        
        no_intervals = np.ceil((end_energy-start_energy)/int_energy)
        print('1st print start,end, no_intervals', start_energy, end_energy, no_intervals)
             
        if self._move_scan_mode == 'in':
            mot_counters = axes2counters(self._radiation_damage.xmot, self._radiation_damage.ymot)
            try:
                ACTIVE_MG.add(mot_counters[0])
            except ValueError:
                print(f'{mot_counters[0].name} probably is already in the ACTIVE_MG')
            try:
                ACTIVE_MG.add(mot_counters[1])
            except ValueError:
                print(f'{mot_counters[1].name} probably is already in the ACTIVE_MG') 
                
        if self._move_scan_mode == 'between':
            x, y = self._position_manager.get_next_position(self._current_sample)
            self._first_correction_index = self.get_current_index()
            print(f"===== moving ({self.get_param('sample_mot1')}, {self.get_param('sample_mot2')}) to ({x}, {y}) (counts={self._position_manager.get_cycle_count(self.current_sample)})\n")
            umv(self._radiation_damage.xmot, x, self._radiation_damage.ymot, y)
                    
        for i in range(_number_of_scans):
             
            print("NOT IMPLEMENTED YET check gauges 5 and open valve 4")
            
            if self._move_scan_mode == 'in':
                self._first_correction_index = self.get_current_index()
                my_xas_scan = setup_globals.ascan(ENERGY,start_energy, end_energy ,int(no_intervals),acq_time, run = False)
                my_xas_scan.acq_chain.add_preset(self._radiation_damage)
                first_index = self._radiation_damage.pm.get_index(self._current_sample)
                print(first_index)    
                my_xas_scan.run() # run main scan
                no_points = int(no_intervals)+1#len(my_xes_scan.get_data()['sec'])
                self._nbpts = no_points
                ok = True
            else: #move_scan_mode False or "between"        
                my_xas_scan = setup_globals.ascan(ENERGY,start_energy, end_energy,int(no_intervals),acq_time, run = True)
                ok = True
                print(start_energy, end_energy, int(no_intervals), acq_time)

            self.hook_post_ascanloop()
                    
        
        
        
# TODO: also undulators
        # restore original undulator tracking          
        U35A.track(u35a_track)
        U35B.track(u35b_track)
        U35C.track(u35c_track)  
        
        # go back to initial position 
        
        umv(ENEUND,energy_before)
        
        # Check that IDs went back to original position
        #TO DO
        #for one_undu in setup_globals.get_available_undulators():
            #print(one_undu.name)
            #if (abs(one_undu.position-before_step_xas_pos.position(one_undu))>= tol):
                #before_step_xas_pos.move(one_undu)

            
            
     
            

class StepXanes(ID26StepXAS):

    '''
    X-ray Absorption Near-Edge Spectroscopy 
    During scan, the undulator gap remains fixed at the energy chosen in energy_edge
    (a good value is to set it at 2/3 of the scan range). 
    '''

    def hook_pre_ascanloop(self):
#        super().hook_pre_ascanloop()
        energy_edge = self.get_param('energy_edge')
        if energy_edge is not None:
            umv(ENEUND,energy_edge)
        else:
            print('Warning: energy_edge is not defined, not moving undulators to energy edge values')
        # untrack     
        U35A.track(False)
        U35B.track(False)
        U35C.track(False)
        print ("NOT IMPLEMENTED YET Xmap activation")


    def hook_post_ascanloop(self):
        #super().hook_post_ascanloop()
        pass

    def rocking_curve(self):
        print ("XANES NOT IMPLEMENTED YET rc: mv energy start_energy;rc") #rc includes beamcheck

class StepExafs(ID26StepXAS):
    '''
   EXAFS
    During scan, the undulators follow monochromator according to calibration
    '''

    def hook_pre_ascanloop(self):
#        super().hook_pre_ascanloop()
        # force track 
        U35A.track(True)
        U35B.track(True)
        U35C.track(True)
        print ("NOT IMPLEMENTED YET Xmap activation")


    def hook_post_ascanloop(self):
        #super().hook_post_ascanloop()
        pass

    def rocking_curve(self):
        print ("XANES NOT IMPLEMENTED YET rc: mv energy start_energy;rc") #rc includes beamcheck

