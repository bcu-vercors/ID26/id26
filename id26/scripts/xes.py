import numpy as np
import time

from bliss.common.standard import info
from bliss import setup_globals
from bliss.shell.cli.user_dialog import UserChoice
from bliss.shell.cli.pt_widgets import BlissDialog, button_dialog
from bliss.common.scans import timescan, loopscan
from bliss.shell.standard import umv

from id26.scripts.fscan import fscan
from id26.scripts.xas import ID26Measurement, move_undulators
from id26.scripts.position_instances import PositionInstances 
from id26.controllers.radiation_damage import axes2counters, PositionManager , RadiationDamagePreset
from id26.scripts.beamline_settings import purge_variables

pm = PositionManager('positions_manager')
DEFAULT_POSITION_MANAGER = pm
U35A = setup_globals.u35a
U35B = setup_globals.u35b
U35C = setup_globals.u35c
ENEUND = setup_globals.eneund
ENERGY = setup_globals.energy
#ENEUND = setup_globals.energy # cheating on MDTs
FSCAN_PAR = setup_globals.fscan_par
ACTIVE_MG = setup_globals.ACTIVE_MG

# choose slits, spectrometer, ... according to the session / hutch in use
if setup_globals.current_session.name in ['optics', 'eh2']:
            SVG = setup_globals.e2vg
            SHG = setup_globals.e2hg
            XES_EN = setup_globals.xes_en
            #XES_EN = setup_globals.simumot
            DSLIT = setup_globals.dslit
            #HSPECTRO = setup_globals.hspectrometer
elif setup_globals.current_session.name in ['texs']:
            SVG = setup_globals.tsvg
            SHG = setup_globals.tshg
            XES_EN = setup_globals.xes_en_texs

def general_len(x):
    print(type(x))
    if type(x) == float or type(x) == int:
        return 1
    elif type(x) == list or type(x) == tuple or type(x) == np.ndarray:
        return len(x)
    else:
        print('unrecognized input type')
        return
    


# xes_regions to be replaced by "z3scan"

def xes_regions(xes_energies, ene_steps , acq_times, one_rd = None, *counter_args):    
    '''
         xes_regions USAGE:"
         Allows to define a sequence of scans with different stepsize and acquisition time.
         Usually we use 3 regions but more is possible. N is the number of regions.
         You must define :"
          * a list of n+1 energies Ei defining the three regions "
            xes_energies = [xes_en_1, xes_en_2, xes_en_3, xes_en_4, ...]
          * a list of n  energy intervals of consecutive points (Ene_step_i) in each region"
            ene_steps = [ene_step_1, ene_step_2, ene_step_3, ...] 
          * a list of n acquistion times per point (time_i) for each xes region"
            acq_times =  [time_1, time_2, time_3, ...]
           where:"
             * <E1> <E2> <E3> <E4> : energy limits of the three regions (in keV)"
             * <Ene_step_1> <Ene_step_2> <Ene_step_3> : size of Energy steps for each region (in keV)"
             * <time_1> <time_2> <time_3> : integration time for each region (in seconds)"
          "
         Example: "
            xes_regions([7.02, 7.07, 7.10, 7.125],[.0001,.0003, .0001], [.5, 5, 5], my_mg) 
         <E1> and <E4> define the total energy range of the xes scan"
         En2, En3 and E4 define the three regions of interest"
         !!!! ACHTUNG: E1, E2, E3, E4 as well as ei1, ei2, ei3 are in keV !!!!"
         Currently this will make n scans. You have to stitch them together manually
    '''

#TODO: indicate where wrong ordere ,which  acquisition time
    no_points_list = []
    if np.all(np.sort(xes_energies) == xes_energies):
        print("The energies are in ascending order")
    elif np.all(np.sort(xes_energies) == xes_energies[::-1]):
        print("The energies are in descending order")        
    else:
        print('Wrong order of xes_en energies. Check your input.')
        return
    if any(t < 0 for t in acq_times):
        print('You have set a negative acquisition time. Check your input.')
    else:
        pass
    _npts = []
    exp_time = 0
    for idx, one_ene in enumerate(xes_energies[:-1]):
        one_npts =  np.abs(int((xes_energies[idx+1]-one_ene)/ene_steps[idx]))
        _npts = np.append(_npts, one_npts)
        print("the number of intervals in region {:d} is {:d} ".format(idx+1, one_npts))
        exp_time+=one_npts*acq_times[idx]
  
    print("The total acquisition time (I do not account for bliss overhead... see wikipedia optimistic bias) is {:.2f} seconds". format(exp_time*1.33))
    for idx, one_ene in enumerate(xes_energies[:-1]):
        print('going to run: ascan(xes_en, {:f}, {:f}, {:d}, {:f})'.format(one_ene, xes_energies[idx+1], int(_npts[idx]), acq_times[idx]))
        if one_rd is None:
            one_scan = setup_globals.ascan(XES_EN, one_ene, xes_energies[idx+1], int(_npts[idx]), acq_times[idx], *counter_args)    
        else:
            one_scan = setup_globals.ascan(XES_EN, one_ene, xes_energies[idx+1], int(_npts[idx]), acq_times[idx], run = False, *counter_args)
            one_scan.acq_chain.add_preset(one_rd)
            one_scan.run()
        no_points_list = no_points_list + [int(_npts[idx])+1]
    return no_points_list

def regions_correction_scans(no_points_list, acq_times, one_rd):
# one_rd already positioned at a correct index, energy and xes_en already in place

    for idx, one_acq_time in enumerate(acq_times):
        one_correction_scan = loopscan(no_points_list[idx], one_acq_time, run = False)   
                        # back to the starting index
        one_correction_scan.acq_chain.add_preset(one_rd)
        #print(f'1st index in the {idx}th corrections scan: {one_rd.pm.get_index(self._current_sample)}')
        one_correction_scan.run() # run a single correction scan
        
# xes_regions to be replaced by "z3scan"

def xes_regions_simple(xes_energies, ene_steps , acq_times, *counter_args):   
    # without RadiationDamage object. It should do the same as xes_regions(one_rd=None) 
    '''
         xes_regions USAGE:"
         Allows to define a sequence of scans with different stepsize and acquisition time.
         Usually we use 3 regions but more is possible. N is the number of regions.
         You must define :"
          * a list of n+1 energies Ei defining the three regions "
            xes_energies = [xes_en_1, xes_en_2, xes_en_3, xes_en_4, ...]
          * a list of n  energy intervals of consecutive points (Ene_step_i) in each region"
            ene_steps = [ene_step_1, ene_step_2, ene_step_3, ...] 
          * a list of n acquistion times per point (time_i) for each xes region"
            acq_times =  [time_1, time_2, time_3, ...]
           where:"
             * <E1> <E2> <E3> <E4> : energy limits of the three regions (in keV)"
             * <Ene_step_1> <Ene_step_2> <Ene_step_3> : size of Energy steps for each region (in keV)"
             * <time_1> <time_2> <time_3> : integration time for each region (in seconds)"
          "
         Example: "
            xes_regions([7.02, 7.07, 7.10, 7.125],[.0001,.0003, .0001], [.5, 5, 5], my_mg) 
         <E1> and <E4> define the total energy range of the xes scan"
         En2, En3 and E4 define the three regions of interest"
         !!!! ACHTUNG: E1, E2, E3, E4 as well as ei1, ei2, ei3 are in keV !!!!"
         Currently this will make n scans. You have to stitch them together manually
    '''

#TODO: indicate where wrong ordere ,which  acquisition time

    if np.all(np.sort(xes_energies) == xes_energies):
        print("The energies are in the correct order")
    else:
        print('Wrong order of xes_en energies. Check your input.')
        return
    if any(t < 0 for t in acq_times):
        print('You have set a negative acquisition time. Check your input.')
    else:
        pass
    _npts = []
    exp_time = 0


    for idx, one_ene in enumerate(xes_energies[:-1]):
        one_npts =  int((xes_energies[idx+1]-one_ene)/ene_steps[idx])
        _npts = np.append(_npts, one_npts)
        print("the number of intervals in region {:d} is {:d} ".format(idx+1, one_npts))
        exp_time+=one_npts*acq_times[idx]
  
    print("The total acquisition time (I do not account for bliss overhead... see wikipedia optimistic bias) is {:.2f} seconds". format(exp_time*1.33))
    for idx, one_ene in enumerate(xes_energies[:-1]):
        print('going to run: ascan(xes_en, {:f}, {:f}, {:d}, {:f})'.format(one_ene, xes_energies[idx+1], int(_npts[idx]), acq_times[idx]))
        setup_globals.ascan(XES_EN, one_ene, xes_energies[idx+1], int(_npts[idx]), acq_times[idx], *counter_args)

def order_multiple_xes_vars(start_xes_en, end_xes_en, step_xes_en, acq_time, hutch):
    # check length of arrays and validity of multiple_xes_vars entries. Re-order if necessary.
    # valid is something like this:
    # start_xes_en: list of n elements, ascending/descending
    # end_xes_en: single float
    # step_xes_en: list of n elements
    # acq_time: list of n elements
    if general_len(end_xes_en) > 1:
        print('end_xes_en must be a single value (float)')
        multiple_xes_en_scan = None
    if (general_len(start_xes_en) == 1 and general_len(step_xes_en) == 1 and general_len(acq_time) == 1): #single scan
        multiple_xes_en_scan = False # single xes_en scan
        if hutch == 'eh1':
            if start_xes_en < end_xes_en:
                (start_xes_en, end_xes_en) = (end_xes_en, start_xes_en)
    elif (general_len(end_xes_en) == 1 and general_len(start_xes_en) == general_len(step_xes_en) and general_len(start_xes_en) == general_len(acq_time) ):
        multiple_xes_en_scan = True # correct length of lists/arrays
        xes_energies = np.append(start_xes_en,end_xes_en)
        if hutch == 'eh1':
            if np.all(np.sort(xes_energies) == xes_energies):
                print("The energies are in ascending order, must re-order")
                xes_energies = xes_energies [::-1]
                start_xes_en = xes_energies[:-1]
                end_xes_en = xes_energies[-1]
                step_xes_en = step_xes_en [::-1]
                acq_time = acq_time [::-1]
            elif np.all(np.sort(xes_energies) == xes_energies[::-1]):
                print("The energies are in descending order, perfect for TEXS")
            else:
                print('ATTENTION: the energies are not ordered, descending order imposed, steps and acq times retained, this may be wrong!')
                xes_enegies = np.sort(xes_energies)[::-1]
                start_xes_en = xes_energies[:-1]
                end_xes_en = xes_energies[-1]              
        elif hutch == 'eh2':
            if np.all(np.sort(xes_energies) == xes_energies):
                print("The energies are in ascending order, perfect for EH2")
            elif np.all(np.sort(xes_energies) == xes_energies[::-1]):
                print("The energies are in descending order, must re-order")
                xes_energies = xes_energies [::-1]
                start_xes_en = xes_energies[:-1]
                end_xes_en = xes_energies[-1] 
                step_xes_en = step_xes_en [::-1]
                acq_time = acq_time [::-1]               
            else:
                print('ATTENTION: the energies are not ordered, ascending order imposed. Steps and acq times retained, this may be wrong!')
                xes_enegies = np.sort(xes_energies)  
                start_xes_en = xes_energies[:-1]
                end_xes_en = xes_energies[-1]   
    return (start_xes_en, end_xes_en, step_xes_en, acq_time, multiple_xes_en_scan) 

class Xes(ID26Measurement):

    def __init__(self, name=None):
        # in most of the cases we do not want to save anything to redis!
        # so, give it "dummy" name
        if name is None:
            name = 'dummy' 
        redis_name = 'id26Variables_'+name
        super().__init__(name)
        try: 
            purge_variables(redis_name)
            print(f'{redis_name} existed in Redis, it has just been cleared.')
        except:
            pass 

    def build_default_parameters(self):
        default_params = {}
        default_params['shg'] = None #needed here? Or sufficient in the experiment?
        default_params['svg'] = None
        default_params['start_xes_en'] = None
        default_params['end_xes_en'] = None
        default_params['step_xes_en'] = None
        default_params['acq_time'] = None
        default_params['number_scans'] = None
        default_params['sample_mot1'] = None #check if needed here, it should be associated with self._radiation_damage 
        default_params['sample_mot2'] = None
        default_params['retry'] = False
        default_params['energy_excite_above'] = None
        default_params['xes_main_max'] = None
        return default_params
        
    def auto_offset (self):
        print("NOT IMPLEMENTED YET auto_offset")

 
    def scan(self, acq_time = None, number_of_scans = None, inc_energy = None, xes_main_max = None):
        '''
        single xes_en scan or multi-region xes_en scan
        if self['start_xes_en'], self['step_xes_en'] and self['acq_time'] are lists or numpy arrays
        '''
        # first check all necessary params/arguments defined
                 
            
        if (acq_time==0):
            raise RuntimeError("acquisition time cannot be 0")    

        if number_of_scans is not None:
            _number_of_scans = number_of_scans
        else:
            _number_of_scans = self.get_param('number_scans')
        if _number_of_scans is None: # if not defined by user at all
            _number_of_scans = 1
            
        if inc_energy is None:
            inc_energy = self.get_param('energy_excite_above')

        if inc_energy is not None:
            umv(ENEUND,inc_energy)
        else:
            print('Warning: incident energy / energy_excite_above not defined, not moving incident energy.')

        if self._move_scan_mode in ['in','between']:
            try:
                self._position_manager.has_pattern(self._current_sample) # checks if PM is defined and current_sample is valid
            except:
                raise NameError("Your move_scan_mode requires current_sample to be defined! Didn't you forget to define it?")   
            
        if xes_main_max is None:
            xes_main_max = self.get_param('xes_main_max')
        
# This is to be in the CorrectionScan
#        if (xes_main_max is None and self._move_scan_mode == 'in'):
#            print('Xes_main_max is not defined for the correction scan')
#            raise         
           
        if self.get_param('svg') is not None:
            SVG.move(self.get_param('svg'))
        if self.get_param('shg') is not None:
            SHG.move(self.get_param('shg'))
        
 
        print("Will do {0} loop(s)".format(_number_of_scans))
        
        start_xes_en = self.get_param('start_xes_en')
        end_xes_en = self.get_param('end_xes_en')
        step_xes_en = self.get_param('step_xes_en')
   
        if acq_time is None:
            acq_time = self.get_param('acq_time')
            
        if acq_time is None:
            raise ValueError('Acq time not defined')
        
        # find out whether this is a single or multi xes_en scan and if multi, check order of params and re-order if necessary
        if setup_globals.current_session.name in ['optics', 'eh2']:
            hutch = 'eh2'
        elif setup_globals.current_session.name in ['texs']:
            hutch = 'eh1'
        (start_xes_en, end_xes_en, step_xes_en, acq_time, multiple_xes_en_scan) = order_multiple_xes_vars(start_xes_en, end_xes_en, step_xes_en, acq_time, hutch)

        if multiple_xes_en_scan is None: # something went wrong:
            print('len([\'start_xes_en\'] = ',general_len(self['start_xes_en']))
            print('len([\'step_xes_en\'] = ',general_len(self['step_xes_en']))
            print('len([\'acq_time\'] = ',general_len(self['acq_time']))
            print('Xes object key value lists/arrays [\'start_xes_en\'], [\'step_xes_en\'] and [\'acq_time\'] do not have the same length or are not in the correct order. Check and try again.')
            raise ValueError
                        
        if self._move_scan_mode == 'in':
            mot_counters = axes2counters(self._radiation_damage.xmot, self._radiation_damage.ymot)
            try:
                ACTIVE_MG.add(mot_counters[0])
            except ValueError:
                print(f'{mot_counters[0].name} probably is already in the ACTIVE_MG')
            try:
                ACTIVE_MG.add(mot_counters[1])
            except ValueError:
                print(f'{mot_counters[1].name} probably is already in the ACTIVE_MG') 
                
        if self._move_scan_mode == 'between':
            x, y = self._position_manager.get_next_position(self._current_sample)
            self._first_correction_index = self.get_current_index()
            print(f"===== moving ({self.get_param('sample_mot1')}, {self.get_param('sample_mot2')}) to ({x}, {y}) (counts={self._position_manager.get_cycle_count(self.current_sample)})\n")
            umv(self._radiation_damage.xmot, x, self._radiation_damage.ymot, y)
        my_xes_scans  = []            
        for i in range(_number_of_scans):
             
            # first a single xes_en scan:
            if multiple_xes_en_scan == False:
                no_intervals = np.abs(np.ceil((end_xes_en-start_xes_en)/step_xes_en)) # make sure positive number
                if self._move_scan_mode == 'in':
                    self._first_correction_index = self.get_current_index()
                    my_xes_scan = setup_globals.ascan(XES_EN,start_xes_en, end_xes_en,int(no_intervals),acq_time, run = False)
                    my_xes_scan.acq_chain.add_preset(self._radiation_damage)
                    first_index = self._radiation_damage.pm.get_index(self._current_sample)
                    print(first_index)                  
                    my_xes_scan.run() # run main scan
                    no_points = int(no_intervals)+1#len(my_xes_scan.get_data()['sec'])
                    self._nbpts = no_points
                else: #move_scan_mode False or "between"
                    my_xes_scan = setup_globals.ascan(XES_EN,start_xes_en, end_xes_en,int(no_intervals),acq_time, run= True)
                print(start_xes_en, end_xes_en, int(no_intervals), acq_time)
                my_xes_scans = my_xes_scans + [my_xes_scan] # for the return function
                
            elif multiple_xes_en_scan == True: #zscan in future, now several scans via "xes_regions" function
                xes_energies = np.append(start_xes_en,end_xes_en)
                #first_index =  self._radiation_damage.pm.get_index(self._current_sample)
                if self._move_scan_mode == 'in':
                    one_rd = self._radiation_damage
                    self._first_correction_index = self.get_current_index()
                else:
                    one_rd = None
                    
                no_points_list = xes_regions(xes_energies, step_xes_en , acq_time, one_rd) # run main scans (several)
                self._nbpts = sum(no_points_list) # correction scan will be run on the total number of points
            else: #neither a single nor multiple scan
                print('this should never happen')
        return my_xes_scans


 
    def scan_without_cc(self, acq_time = None, number_of_scans = None, inc_energy = None):
# TO BE REMOVED WHEN "normal" one tested
        '''
        single xes_en scan or multi-region xes_en scan
        if self['start_xes_en'], self['step_xes_en'] and self['acq_time'] are lists or numpy arrays
        '''
            
        if (acq_time==0):
            raise RuntimeError("acquisition time cannot be 0")    

        if number_of_scans is not None:
            _number_of_scans = number_of_scans
        else:
            _number_of_scans = self.get_param('number_scans')
            
        if inc_energy is not None:
            self['incident_energy'] = inc_energy

        if self.get_param('svg') is not None:
            SVG.move(self.get_param('svg'))
        if self.get_param('shg') is not None:
            SHG.move(self.get_param('shg'))
        
        if self['incident_energy'] is not None:
            ENEUND.move(self.get_param('incident_energy'))

        
            
        print("Will do {0} loop(s)".format(_number_of_scans))
        
        start_xes_en = self.get_param('start_xes_en')
        end_xes_en = self.get_param('end_xes_en')
        step_xes_en = self.get_param('step_xes_en')
        
                
        if acq_time is None:
            acq_time = self.get_param('acq_time')
            
        if self._move_scan_mode == 'in':
            mot_counters = axes2counters(self._radiation_damage.xmot, self._radiation_damage.ymot)
            try:
                ACTIVE_MG.add(mot_counters[0])
            except ValueError:
                print(f'{mot_counters[0].name} probably is already in the ACTIVE_MG')
            try:
                ACTIVE_MG.add(mot_counters[1])
            except ValueError:
                print(f'{mot_counters[1].name} probably is already in the ACTIVE_MG') 
        if self._move_scan_mode == 'between':
            x, y = self._position_manager.get_next_position(self._current_sample)
            print(f"===== moving ({self.get_param('sample_mot1')}, {self.get_param('sample_mot2')}) to ({x}, {y}) (counts={self._position_manager.get_cycle_count(self._current_sample)})\n")
            umv(self._radiation_damage.xmot, x, self._radiation_damage.ymot, y)
                    
        for i in range(_number_of_scans):
             
            print("NOT IMPLEMENTED YET check gauges 5 and open valve 4")
            
            if general_len(start_xes_en) == 1: # single xes_en scan as in Xes main
                no_intervals = np.ceil((end_xes_en-start_xes_en)/step_xes_en)
                if self._move_scan_mode == 'in':
                    my_xes_scan = setup_globals.ascan(XES_EN,start_xes_en, end_xes_en,int(no_intervals),acq_time, run = False)
                    my_xes_scan.acq_chain.add_preset(self._radiation_damage)
                    first_index = self._radiation_damage.pm.get_index(self._current_sample)
                    my_xes_scan.run() # run main scan
                    no_points = int(no_intervals)+1#len(my_xes_scan.get_data()['sec'])
#TODO:                    # move energy to energy_above
                    umv(ENEUND, self.get_param('energy_excite_above'))
                    # move xes_en to xes_main_max
                    umv(XES_EN, self.get_param('xes_main_max'))
                    correction_scan = loopscan(no_points, acq_time, run = False)
                    # back to the starting index
                    self.goto_index(first_index)
                    correction_scan.acq_chain.add_preset(self._radiation_damage)
                    print(f'1st index in the corrections scan', self._radiation_damage.pm.get_index(self._current_sample))
                    correction_scan.run() # run the correction scan
                else: #move_scan_mode False or "between"
                    my_xes_scan = setup_globals.ascan(XES_EN,start_xes_en, end_xes_en,int(no_intervals),acq_time)
                return my_xes_scan
            else: #zscan in future, now several scans via "xes_regions" function
                if (general_len(end_xes_en) == 1 and general_len(start_xes_en) == general_len(step_xes_en) and general_len(start_xes_en) == general_len(acq_time) ):
                    xes_energies = np.append(start_xes_en,end_xes_en)
                    xes_regions(xes_energies, step_xes_en , acq_time)
                else:
                    print('len([\'start_xes_en\'] = ',general_len(self['start_xes_en']))
                    print('len([\'step_xes_en\'] = ',general_len(self['step_xes_en']))
                    print('len([\'acq_time\'] = ',general_len(self['acq_time']))
                    print('Xes object key value lists/arrays [\'start_xes_en\'], [\'step_xes_en\'] and [\'acq_time\'] do not have the same length. Check and try again.')
                    return


        
class Rixs(ID26Measurement):
    
    def __init__(self, name = None):
        # in most of the cases we do not want to save anything to redis!
        # so, give it "dummy" name
        if name is None:
            name = 'dummy' 
        redis_name = 'id26Variables_'+name
        super().__init__(name)
        try: 
            purge_variables(redis_name)
            #print(f'{redis_name} existed in Redis, it has just been cleared.')
        except:
            pass
                
    def build_default_parameters(self):
        default_params = {}
        default_params['shg'] = None
        default_params['svg'] = None
        default_params['dslit'] = None
# RIXS scan ranges for different kinds of RIXS scans
# incident energy for both types of RIXS scans:        
        default_params['xas_start'] = None
        default_params['xas_end'] = None
        default_params['xas_int'] = None  
# a) using fscan2
        default_params['xes_start'] = None
        default_params['xes_end'] = None
        default_params['xes_int'] = None
# b) using xes_en scans at fixed incident energy defined as energy transfer (ET)
        default_params['et_scan'] = False # default is via fscans
        default_params['et_start'] = None
        default_params['et_end'] = None
        default_params['et_int'] = None
             
        default_params['scan_time'] = None   # beware, scan time is for fscan several seconds, for rixs_et it is time per point       
        default_params['number_scans'] = None             
        default_params['number_scans_pos'] = None   # per position on the sample (if radiation damage)     
        default_params['number_xes_pos'] = None 
        return default_params


    def scan(self, scan_time = None, number_of_scans = None, et_scan = None):
    # todo: if scan_time not defined or zero
 
# TODO : when not in a MDT    

        if self.get_param('xas_end') is not None:
            move_undulators(self.get_param('xas_end'))  # best resolution before undulator peak
            print('moving undulators') # this should go back when the line above works!


# TODO: move together both!

        if self.get_param('svg') is not None:
            SVG.move(self.get_param('svg'))
        if self.get_param('shg') is not None:
            SHG.move(self.get_param('shg'))
            
        if number_of_scans is not None:
            _number_of_scans = number_of_scans
        else:
            _number_of_scans = self.get_param('number_scans')
        if _number_of_scans is None: # if not defined by user at all
            _number_of_scans = 1              

            
        if self.get_param('number_scans_pos') is None:
            number_scans_pos = 1
        else:
            number_scans_pos = self.get_param('number_scans_pos')

        if et_scan is None:
            et_scan = self.get_param('et_scan')
       
            
        if scan_time is None:
            scan_time = self.get_param('scan_time')
        if scan_time is None: #still
            if et_scan == False:
                scan_time = 20 # 20s per fscan
            else:
                scan_time = 1 # 1s per point in xes scan
                
            
            
        if self.get_param('dslit') is not None:
            DSLIT.sync_hard()
            print("Moving dslit from {:} to {:}\n".format(DSLIT.position,self.get_param('dslit')))
            DSLIT.move(self.get_param('dslit'))  


        if (self.get_param('xas_int') is None):
            self['xas_int'] = 0.0001
    
        # retain fsh auto_status
        fsh_before = setup_globals.fsh.state_string
        start_time=time.time();
        
        # check input params
        if self._move_scan_mode in ['in','between']:
            if self._current_sample is None:
                print('Current sample not defined. Cannot apply radiation damage macros.')
                raise ValueError 
                
        if self._move_scan_mode == 'in' and et_scan == False:
                print("Cannot use 'in' move_scan_mode within fscan(s)")                
        
        if self._move_scan_mode in ['between', 'in']:
            mot_counters = axes2counters(self._radiation_damage.xmot, self._radiation_damage.ymot)
            try:
                ACTIVE_MG.add(mot_counters[0])
            except ValueError:
                print(f'{mot_counters[0].name} probably is already in the ACTIVE_MG')
            try:
                ACTIVE_MG.add(mot_counters[1])
            except ValueError:
                print(f'{mot_counters[1].name} probably is already in the ACTIVE_MG') 
      
        my_rixs_scans  = []  
                  
        if et_scan == False:
            print('rixs by fscans')
            print("*************************************************************")
            print("                  Start of rixs                         ")
            print("*************************************************************")
    # TODO: check all params defined 
            if self._move_scan_mode == 'in':
                print('Cannot use IN move_scan_mode in fscans. Change rixs[\'et_scan\'] or move_scan_mode.')
                raise ValueError
            # need to remember first index for the correction scan
            elif self._move_scan_mode == 'between':
                self._first_correction_index = self._position_manager.get_index(self._current_sample)           
            for hans in range(_number_of_scans):
                #rccnt = 0; if (RCYESNO) {rc}
                setup_globals.auto_offsets()
                xes_energies = np.arange(self.get_param('xes_start'), (self.get_param('xes_end')+self.get_param('xes_int')/2), self.get_param('xes_int'))
                for xes4rixs in xes_energies:
                    print("Moving xes_en to {}\n".format(xes4rixs))
                    XES_EN.move(xes4rixs)

        # TODO: rccnt and rc
        # TODO: beamcheck
                    if self._move_scan_mode == 'between':
                        x, y = self._position_manager.get_next_position(self._current_sample)
                        print(f"===== moving ({self.get_param('sample_mot1')}, {self.get_param('sample_mot2')}) to ({x}, {y}) (counts={self._position_manager.get_cycle_count(self.current_sample)})\n")
                        umv(self._radiation_damage.xmot, x, self._radiation_damage.ymot, y)  

                    for hans_s in np.arange(0, number_scans_pos):
                        
        #TODO:                id26check_gauges(5); _open_valve(4) # we had problems with penning 5 tripping
                        my_fscan = fscan(self.get_param('xas_start'),self.get_param('xas_end'),self.get_param('scan_time'),self.get_param('xas_int'))
                        print('fscan', self.get_param('xas_start'),self.get_param('xas_end'),self.get_param('scan_time'),self.get_param('xas_int'))
            if self._move_scan_mode == 'between': # run CorrectionScan but here we have to recover number of points
                current_index = self._position_manager.get_index(self._current_sample)
                # following line fails if _first_correction_index was -1 (but is something big on a second run)
                # self._nbpts = current_index-self._first_correction_index
                # replaced by simpler:
                self._nbpts = len(xes_energies)*_number_of_scans
                    
        elif et_scan == True: # et_scan == True: rixs_et scan 
            print('rixs by ET scans')
            print( "*************************************************************")
            print("                  Start of rixs_et                         ")
            print("*************************************************************")
            midrange = self['xas_start']+(self['xas_end']-self['xas_start'])/2.
            npts = abs(int((self['et_end']-self['et_start'])/self['et_int']))  # PG modified, in case scan high to low

            if self._move_scan_mode in ['in', 'between']:
                    self._first_correction_index = self._position_manager.get_index(self._current_sample)            
            for hans in range(_number_of_scans):
                #rccnt = 0; if (RCYESNO) {rc}
                #rdcnt=0;
                setup_globals.auto_offsets()
                for xas4rixs in np.arange(self.get_param('xas_start'), (self.get_param('xas_end')+self.get_param('xas_int')/2), self.get_param('xas_int')):            
        #            if (BEAMCHECK) {id26beam_check( 10, (fabs(RIXS_ET_END-RIXS_ET_START)/RIXS_ET_INT+1)*(RIXS_SCAN_TIME+2.5) + 10 )}
    # TODO                michkon 15 15
                    print("\n\n*** New RIXS_ET energy point. ***\n", xas4rixs)
                    print("Moving energy to {:}".format(xas4rixs))
                    ENERGY.move(xas4rixs) # or ENEUND?
    #                if (rccnt==20 && RCYESNO) {rc; rccnt=0}
    #                if (time()-start_time > 1800 && RCYESNO) {rc; start_time=time()}
                    if setup_globals.current_session.name in ['texs']:
                        et_xes_start = xas4rixs-self.get_param('et_end')
                        et_xes_end = xas4rixs-self.get_param('et_start')
                    else:  # hspectro                    
                        et_xes_start = xas4rixs-self.get_param('et_end') + hspectro_params['ene_diff']
                        et_xes_end = xas4rixs-self.get_param('et_start') + hspectro_params['ene_diff']
#                    if (et_xes_start > et_xes_end):  # PG: Why this?
#                       mal = et_xes_start
#                       et_xes_start = et_xes_end
#                       et_xes_end = mal
                    print("Scan emission spectrometer from {:} to {:}\n".format(et_xes_start, et_xes_end))
                    XES_EN.move(et_xes_start)

    # TODO:         # check wich samp_rad_damage
                    # and move accordingly!
    #TODO:                id26check_gauges(5); _open_valve(4) # we had problems with penning 5 tripping
                    #print('scan to run', et_xes_start, et_xes_end, npts, scan_time)
                    if self._move_scan_mode == 'between':
                        x, y = self._position_manager.get_next_position(self._current_sample)
                        print(f"===== moving ({self.get_param('sample_mot1')}, {self.get_param('sample_mot2')}) to ({x}, {y}) (counts={self._position_manager.get_cycle_count(self.current_sample)})\n")
                        umv(self._radiation_damage.xmot, x, self._radiation_damage.ymot, y) 
                        my_xes_scan = setup_globals.ascan(XES_EN,et_xes_start, et_xes_end, npts, scan_time)
                        print('xes_en scan', et_xes_start, et_xes_end, npts, scan_time)
                        
                    elif self._move_scan_mode == 'in':
                        my_xes_scan = setup_globals.ascan(XES_EN,et_xes_start, et_xes_end, npts, scan_time, run = False)
                        print('xes_en_scan', et_xes_start, et_xes_end, npts, scan_time)
                        my_xes_scan.acq_chain.add_preset(self._radiation_damage)
                        first_index = self._radiation_damage.pm.get_index(self._current_sample)
                        print('first index in the current scan', first_index)
                        my_xes_scan.run() # run main scan
# correction scan must be run separately
                    else: #move_scan_mode False 
                        my_xes_scan = setup_globals.ascan(XES_EN,et_xes_start, et_xes_end, npts, scan_time)
                        #print('move_scan_mode False', et_xes_start, et_xes_end, npts, scan_time)
            if self._move_scan_mode in ['in', 'between']: # run CorrectionScan separately but here we have to recover the correct number of points
                    current_index = self._position_manager.get_index(self._current_sample)
                    self._nbpts = current_index-self._first_correction_index
        else: #et_scan neither True nor False
            print('this should never happen')
                    
    # restore             
        if fsh_before == 'Auto':
            setup_globals.fsh.auto_on()
        elif fsh_before == 'Closed':
            setup_globals.fsh.close()
            setup_globals.fsh.auto_off()
        else: # open
            print('attention, your fsh is open')

        print("*************************************************************")
        print("                  End of rixs                         ")
        print("*************************************************************")

class CorrectionScan(ID26Measurement):
# TODO: what to do with "xes_regions" correction scan?
    def __init__(self, name = None):
        # in most of the cases we do not want to save anything to redis!
        # so, give it "dummy" name
        if name is None:
            name = 'dummy' 
        redis_name = 'id26Variables_'+name
        super().__init__(name)
        try: 
            purge_variables(redis_name)
            #print(f'{redis_name} existed in Redis, it has just been cleared.')
        except:
            pass
        self._main_scan = None 

    def build_default_parameters(self):
        default_params = {}
        default_params['energy_excite_above'] = None
        default_params['shg'] = None
        default_params['svg'] = None
        default_params['acq_time'] = None
        default_params['sample_mot1'] = None
        default_params['sample_mot2'] = None
        default_params['retry'] = False
        default_params['xes_main_max'] = None
        return default_params

    @property
    def main_scan(self):
        return self._main_scan
        
    @main_scan.setter
    def main_scan(self, value):
        # TODO check that value is a valid main scan
        self._main_scan = value
        
 
    def scan(self, acq_time = None, inc_energy = None, xes_main_max = None, first_index = None, last_index = None, nbpts = None):
        # make sure pname is defined, here it is automatically the main scan current_sample:
        self._current_sample = self._main_scan._current_sample
            
        if self._current_sample is None:
            raise ValueError('current sample not defined')
            
        if acq_time is None:
            acq_time = self.get_param('acq_time')
        if acq_time is None:
            acq_time = self._main_scan.get_param('acq_time')
        if acq_time is None:
            acq_time = 1 #s
            
        if inc_energy is None:
            try:
                inc_energy =  self.get_param('energy_excite_above')
            except:
                print('Energy_excite_above not defined in the CorrectionScan, trying elsewhere.')
        if inc_energy is None:
            try:
                inc_energy = self._main_scan.get_param('energy_excite_above')
            except:
                print('Energy_excite_above not defined in the main scan either, the macro will not work.')                
            
        if xes_main_max is None:
            try:
                xes_main_max = self.get_param('xes_main_max')
            except:
                raise ValueError('Xes_main_max not defined in the CorrectionScan, please, add xes_main_max value.')

        if first_index is None:
            first_index = self._main_scan._first_correction_index
            print('first index in the main scan', first_index)
            
        if (last_index is None and nbpts is None): # all automatic from pm position
            nbpts = self._main_scan._nbpts
        elif nbpts is None: # probably last index given: beware if more points than pattern spots!
            if last_index is None:
                last_index = self._main_scan.get_current_index()
            else:
                nbpts = last_index - first_index+1
        print(f'number of points is ', nbpts)
        
        # check all params defined 
        if xes_main_max is None:
            print('XES energy not defined, aborting')
            raise ValueError
            
        if inc_energy is None:
            print('incident energy is not defined, aborting')
            raise ValueError
            
        if first_index is None:
            print('First index not defined, aborting')
            raise ValueError
        if nbpts is None:
            print('Number of points in the correction scan is not defined, aborting')
            raise ValueError
        
        # move incident and emission energy
        umv(XES_EN, xes_main_max) 
        umv(ENEUND, inc_energy)             
        # make sure motor positions recorded in the scan
        if self.main_scan is not None:
            xmot = self._main_scan._radiation_damage.xmot
            ymot = self._main_scan._radiation_damage.ymot
        else:
            xmot = self.get_param('sample_mot1')
            ymot = self.get_param('sample_mot2')
        
        mot_counters = axes2counters(xmot, ymot)
        try:
            ACTIVE_MG.add(mot_counters[0])
        except ValueError:
            print(f'{mot_counters[0].name} probably is already in the ACTIVE_MG')
        try:
            ACTIVE_MG.add(mot_counters[1])
        except ValueError:
            print(f'{mot_counters[1].name} probably is already in the ACTIVE_MG') 
        
        self._radiation_damage = RadiationDamagePreset(xmot, ymot, self._position_manager, self._current_sample) # this makes the cc RD independent of the main scan RD
        # back to the starting index
        if first_index == -1: # if sample not run yet
            self.goto_index(len(self._main_scan._radiation_damage.pm._positions[self._current_sample])-1) # go to the last point of the pattern so that the next position is the first one
        else:
            self.goto_index(first_index)
        print(f'1st index in the correction scan {self.get_current_index()}')
        correction_scan = loopscan(nbpts, acq_time, run = False)
        correction_scan.acq_chain.add_preset(self._radiation_damage) # must be run on the cc RD not to lose the main scan indexing!
        correction_scan.run() # run the correction scan
        return correction_scan

