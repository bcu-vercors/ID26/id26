# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from functools import partial
from bliss import global_map
from bliss.config import settings
from bliss.comm.util import get_comm
from bliss.common.logtools import log_debug, log_info
from bliss.common.utils import autocomplete_property
from bliss.common.soft_axis import SoftAxis
from bliss.common.axis import AxisState
from bliss.shell.formatters.table import IncrementalTable
from bliss.common.protocols import IterableNamespace

"""
Bliss controller for gasdet.

-   class: GasDet
    package: id26.controllers.gasdet
    plugin: bliss
    name: texsdet
    timeout: 10
    tolerance: 0.1
    low_limit: 0.2
    high_limit: 0.5
    tcp:
        url: texsdet.esrf.fr:5000

    axes:
        - axis_name: tgd01l
          tag: l1
        - axis_name: tgd02l
          tag: l2
        - axis_name: tgd03l
          tag: l3
        - axis_name: tgd04l
          tag: l4
        - axis_name: tgd05l
          tag: l5
        - axis_name: tgd06l
          tag: l6
        - axis_name: tgd07l
          tag: l7
        - axis_name: tgd08l
          tag: l8
        - axis_name: tgd09l
          tag: l9
        - axis_name: tgd10l
          tag: l10
        - axis_name: tgd11l
          tag: l11
        - axis_name: tgd12l
          tag: l12
        - axis_name: tgd13l
          tag: l13
        - axis_name: tgd14l
          tag: l14
        - axis_name: tgd15l
          tag: l15
        - axis_name: tgd16l
          tag: l16

        - axis_name: tgd01h
          tag: h1
        - axis_name: tgd02h
          tag: h2
        - axis_name: tgd03h
          tag: h3
        - axis_name: tgd04h
          tag: h4
        - axis_name: tgd05h
          tag: h5
        - axis_name: tgd06h
          tag: h6
        - axis_name: tgd07h
          tag: h7
        - axis_name: tgd08h
          tag: h8
        - axis_name: tgd09h
          tag: h9
        - axis_name: tgd10h
          tag: h10
        - axis_name: tgd11h
          tag: h11
        - axis_name: tgd12h
          tag: h12
        - axis_name: tgd13h
          tag: h13
        - axis_name: tgd14h
          tag: h14
        - axis_name: tgd15h
          tag: h15
        - axis_name: tgd16h
          tag: h16

        
"""

class GasDet:
    def __init__(self, name, config):
        self._name = name
        self._config = config
        self._timeout = config.get("timeout", 3.0)
        self._comm = get_comm(self._config)
        self._chan_range = [1, 16]
        self._thld_settings = settings.HashObjSetting(f"GasDet:{self._name}")
        self._cached_thresh = {}
        self._CACHING = True
        
        global_map.register(self, children_list=[self._comm])

        self._ALL_CMDS = {
            # COMMAND    :  (     INFO                              ,  ARGS )
            
            '#CNTRL'     :  ('Set the voltage reference of the DACs', 'None'),        # MANDATORY AT INIT

            '#STHLDL'    :  ('Set the LDL threshold'                , 'all/DAC1/.../DAC16  str(voltage)v'),
            '#STHLDH'    :  ('Set the LDH threshold'                , 'all/DAC1/.../DAC16  str(voltage)v'),
            
            '?GTHLDL'    :  ('Get the LDL threshold'                , 'DAC1/.../DAC16'),
            '?GTHLDH'    :  ('Get the LDH threshold'                , 'DAC1/.../DAC16'),
            
            '?APPNAME'   :  ('Check communication'                  , 'None'),
            '?DACSTA'    :  ('Get DACs status (all DACs)'           , 'None'),
            '?Q7TEMP'    :  ('Read temperature of FPGA card'        , 'None'),
            
            #'#WINDISC'   :  ('Window definition'                    , 'all/DAC1/.../DAC16  str(low_voltage)v str(high_voltage)v'),
            #'#RESETCNT'  :  ('Reset the counter'                    , '??? None or all/cnt1/.../cnt16 ???'),
            #'?READCNT'   :  ('Reads out the counter'                , 'all/cnt1/.../cnt16'),
            
            #'#PULSEDELAY':  ('Delay of the pulse (units: 10 ns)'    , 'str(pulse_value)'),
            #'#CMPFQ'     :  ('Set the output pulse width'           , 'str(frequency)MHz'),
            #'#TPFQ'      :  ('Settings of the test pulse'           , 'even/odd str(frequency)KHZ'),
            #'#TESTPULSE' :  ('Test pulse start/stop (around 0.37 V)', 'even/odd on/off'),
            #'#ONETP'     :  ('One test pulse only (not tested)'     , 'even/odd on/off'),
        }

        # --- pseudo axes ------
        self._create_soft_axes(config)

        self.initialize()

    def initialize(self):
        
        self._comm.open()
        self._comm.flush()

        # initialize the controller (MANDATORY), expected response == b'CNTRL OK'
        cmd = '#CNTRL'
        ans = self.send_cmd(cmd)
        if ans != 'CNTRL OK':
            raise RuntimeError(f"Unexpected response from {self._name} to cmd: '{cmd}'\nReceived: {ans}")

        self._apply_settings_to_hardware()

    def flush_com(self):
        self._comm.flush()

    def send_cmd(self, cmd, lines_to_read=1):

        if cmd.split(' ')[0] not in self._ALL_CMDS.keys():
            raise ValueError(f"Unknown command {cmd} !")

        cmd = f"{cmd}\n".encode()

        if lines_to_read <= 0:
            raise ValueError(f"The 'lines_to_read' argument must be an integer > 0 !")

        # elif lines_to_read == 0:
        #     self._comm.write(cmd, timeout=self._timeout)
        #     return

        elif lines_to_read == 1:
            ans = self._comm.write_readline(cmd, timeout=self._timeout).decode()
            self._check_answers([ans])

        else:
            ans = [ x.decode() for x in self._comm.write_readlines(cmd, lines_to_read, timeout=self._timeout) ]
            self._check_answers(ans)

        return ans

    def get_ldl(self, channel):
        """
        Returns low threshold value of a given channel.
        args:
            channel: int
        """
        tag = f"l{channel}"
        value = self._cached_thresh.get(tag)
        if value is None or not self._CACHING:
            value = self._read_ldl(channel)
            #take the occasion to update the cache but not the settings
            self._cached_thresh[tag] = value
        return value
        
    def get_ldh(self, channel):
        """
        Returns high threshold value of a given channel.
        args:
            channel: int
        """
        tag = f"h{channel}"
        value = self._cached_thresh.get(tag)
        if value is None or not self._CACHING:
            value = self._read_ldh(channel)
            #take the occasion to update the cache but not the settings
            self._cached_thresh[tag] = value
        return value

    def set_ldl(self, channel, value):
        """
        Set the low threshold value of a given channel.
        Use channel = 'all' to set all channels at once.
        args:
            channel: int or 'all'
        """

        value = float(value)
        ans = self._write_ldl(channel, value)

        if channel == 'all':
            values = { f"l{i}":value for i in range(self._chan_range[0], self._chan_range[1]+1) }
        else:
            values = { f"l{channel}":value }
            
        self._update_cache_and_settings(values)
        return ans

    def set_ldh(self, channel, value):
        """
        Set the high threshold value of a given channel.
        Use channel = 'all' to set all channels at once.
        args:
            channel: int or 'all'
        """

        value = float(value)
        ans = self._write_ldh(channel, value)

        if channel == 'all':
            values = { f"h{i}":value for i in range(self._chan_range[0], self._chan_range[1]+1) }
        else:
            values = { f"h{channel}":value }
            
        self._update_cache_and_settings(values)
        return ans

    def get_all(self):
        """
        Returns the threshold values for all channels as a dict {tag:value}.
        With tag = '{l or h}{channel_number}' , e.g. 'l1' or 'h12'.
        """

        if self._CACHING:
            return self._cached_thresh
        else:
            values = self._read_all()
            #take the occasion to update the cache and settings
            self._update_cache_and_settings(values)
            return values
            
    def sync_hard(self):
        """ Synchronize the cache and settings of threshold values with the values from hardware """
        self._update_cache_and_settings( self._read_all() )
        for tag in self._soft_axes:
            self._axes_state[tag] = AxisState("READY")

    def get_fpga_temperature(self):
        # expected response: '?Q7TEMP 67'
        return float(self.send_cmd("?Q7TEMP").split(' ')[1])

    def __info__(self):
        values = self.get_all()
        rg = range(self._chan_range[0], self._chan_range[1]+1)
        header = f"GasDet: {self._name} ({self._config.get('tcp')['url']})\n"
        labels = ["DAC channel", "Low (V)", "High (V)"]
        tab = IncrementalTable([labels])
        tab.set_column_params(0, {"flag": ""})
        [tab.add_line([i, values[f'l{i}'] , values[f'h{i}']]) for i in rg]
        tab.resize(minwidth=10, maxwidth=100)
        tab.add_separator(sep="-", line_index=1)
        return "\n".join([header, str(tab)])

    def __str__(self):
        # this is for the mapping: it needs a representation of instance
        return super().__repr__()

    def _read_all(self):
        """ Read all the DAC low and high thresholds values from hardware.
            Returns a dict {tag:value} with tag = '{l or h}{channel_number}'.
        """

        #using a dict because the controller can return the values in different orders
        values = {} 
        lines = self.send_cmd("?DACSTA", lines_to_read=18)

        # --- example of expected response ---
        
        # [b'?DACSTA $', 
        # b'dac1: 0.200111V --> 0.500141V', 
        # b'dac2: 0.200111V --> 0.500141V', 
        # b'dac3: 0.195087V --> 0.500141V', 
        # b'dac4: 0.180157V --> 0.500141V', 
        # b'dac5: 0.193077V --> 0.500141V', 
        # b'dac6: 0.190063V --> 0.500141V', 
        # b'dac7: 0.197527V --> 0.500141V', 
        # b'dac8: 0.200111V --> 0.500141V', 
        # b'dac9: 0.200111V --> 0.500141V', 
        # b'dac10: 0.195087V --> 0.500141V', 
        # b'dac11: 0.185182V --> 0.500141V', 
        # b'dac12: 0.200111V --> 0.500141V', 
        # b'dac13: 0.198102V --> 0.500141V', 
        # b'dac14: 0.200111V --> 0.500141V', 
        # b'dac15: 0.200111V --> 0.500141V', 
        # b'dac16: 0.200111V --> 0.500141V', 
        # b'$']

        if len(lines) != 18:
            msg = f"Wrong response to '?DACSTA', expect to receive 18 lines but only {len(lines)} lines received !\n"
            msg += "\n".join(lines)
            raise RuntimeError(msg)
        
        for line in lines[1:-1]:
            chan, ldl, sep, ldh = line.split(' ') 
            chan = int(chan[3:-1])
            ldl = float(ldl[:-1])
            ldh = float(ldh[:-1])

            values[f"l{chan}"] = ldl
            values[f"h{chan}"] = ldh

        return values
    
    def _read_ldl(self, channel):
        channel = self._check_channel(channel)
        cmd = "?GTHLDL DAC%d"%channel
        # example of expected response: '?GTHLDL 0.200111V'
        return float(self.send_cmd(cmd).split(' ')[1][:-1])

    def _read_ldh(self, channel):
        channel = self._check_channel(channel)
        cmd = "?GTHLDH DAC%d"%channel
        # example of expected response: '?GTHLDH 0.500141V'
        return float(self.send_cmd(cmd).split(' ')[1][:-1])

    def _write_ldl(self, channel, value):
        if channel == 'all':
            cmd = "#STHLDL all %sv"%value
        else:
            channel = self._check_channel(channel)
            cmd = "#STHLDL DAC%d %sv"%(channel, value)
        # expected response: 'STHLDL OK'
        return self.send_cmd(cmd)

    def _write_ldh(self, channel, value):
        if channel == 'all':
            cmd = "#STHLDH all %sv"%value
        else:
            channel = self._check_channel(channel)
            cmd = "#STHLDH DAC%d %sv"%(channel, value)
        # expected response: 'STHLDH OK'
        return self.send_cmd(cmd)

    def _update_cache_and_settings(self, values):
        """ Update the cache and the settings from a dict {tag:value} """
        for tag, value in values.items():
            self._cached_thresh[tag] = value
            self._thld_settings[tag] = value

    def _apply_settings_to_hardware(self):
        """ set the controller with threshold values stored in Redis """

        # check the thresholds settings exist in redis
        try:
            # if yes, retreive the values and apply to the controller and feed the cache
            for channel in range(self._chan_range[0], self._chan_range[1]+1):
                ldl = self._thld_settings[f"l{channel}"]
                ldh = self._thld_settings[f"h{channel}"]

                self.send_cmd( "#STHLDL DAC%d %sv"%(channel, ldl) )
                self._cached_thresh[f"l{channel}"] = ldl

                self.send_cmd( "#STHLDH DAC%d %sv"%(channel, ldh) )
                self._cached_thresh[f"h{channel}"] = ldh

        #else: #it is the very first time or redis db have been flushed
        except Exception as e:
            print(f"warning: {self._name} failed to retreive threshold values from redis settings with error:\n")
            print(e.args[0])
            print("Reading values from the controller itself!")
            cache_status = self._CACHING
            self._CACHING = False
            self.get_all()
            self._CACHING = cache_status

    def _check_answers(self, answers):
        #errors = []
        for ans in answers:
            if 'ERROR' in ans:
                #errors.append(ans)
                raise RuntimeError(ans)
        
        # if errors:
        #     msg = "\n".join(errors)
        #     raise RuntimeError(f"{msg}\n")

    def _check_channel(self, channel):
        channel = int(channel)
        if channel<self._chan_range[0] or channel>self._chan_range[1]:
             raise ValueError(f"The 'channel' number must be in range {self._chan_range} !")
        return channel

    # ---- SOFT AXIS METHODS TO MAKE THE ACE SCANABLE -----------

    def _create_soft_axes(self, config):
        
        axes_conf = config.get("axes", [])

        self._ax_tol = config.get("tolerance", 0.1)
        self._ax_low = config.get("low_limit", 0.2)
        self._ax_high = config.get("high_limit", 0.5)

        self._axes_state = {}
        self._soft_axes = {}

        for conf in axes_conf:

            name = conf["axis_name"].strip()
            tag = conf["tag"].strip()

            if tag[0] not in ['l','h']:
                msg  = f"Wrong tag '{tag}' for axis {name}!\n"
                msg += f"The tag must start with 'l' for low threshold or 'h' for high threshold\n"
                raise ValueError(msg)
                
            self._check_channel(tag[1:])

            self._soft_axes[tag] = SoftAxis(
                name,
                self,
                position=partial(self._axis_position, tag=tag),
                move=partial(self._axis_move, tag=tag),
                stop=partial(self._axis_stop, tag=tag),
                state=partial(self._axis_state, tag=tag),
                low_limit=self._ax_low,
                high_limit=self._ax_high,
                tolerance=self._ax_tol,
                unit="V",
            )
            
            self._axes_state[tag] =  AxisState("READY")
            
    def _axis_position(self, tag):
        """ Return the current position of the associated soft axis"""
        channel = int(tag[1:])
        if tag.startswith('l'):
            return self.get_ldl(channel)
        elif tag.startswith('h'):
            return self.get_ldh(channel)
        else:
            raise ValueError(f"axis_position: unknown tag {tag}")

    def _axis_move(self, pos, tag):
        """ Set the position of the associated soft axis"""

        if (pos < self._ax_low) or (pos > self._ax_high):
            ValueError(
                f"Error: cannot move outside limits [{self._ax_low}, {self._ax_high}] "
            )
        
        channel = int(tag[1:])
        self._axes_state[tag] = AxisState("MOVING")
        
        if tag.startswith('l'):
            self.set_ldl(channel, pos)
        elif tag.startswith('h'):
            self.set_ldh(channel, pos)

        self._axes_state[tag] = AxisState("READY")

    def _axis_stop(self, tag):
        """ Stop the motion of the associated soft axis """
        pass

    def _axis_state(self, tag):
        """ Return the current state of the associated soft axis.
        """

        # Standard axis states:
        # MOVING : 'Axis is moving'
        # READY  : 'Axis is ready to be moved (not moving ?)'
        # FAULT  : 'Error from controller'
        # LIMPOS : 'Hardware high limit active'
        # LIMNEG : 'Hardware low limit active'
        # HOME   : 'Home signal active'
        # OFF    : 'Axis is disabled (must be enabled to move (not ready ?))'

        return self._axes_state[tag]

    @autocomplete_property
    def axes(self):
        return IterableNamespace(**self._soft_axes)

    def _run_test(self):

        from bliss.controllers.simulation_diode import simulation_diode

        def approx(value, digit=3):
            fac = 10**digit
            return int(value*fac)/fac

        self.flush_com()
        self.sync_hard()
        
        print(self.__info__())
        
        for caching in [False, True]:

            self._CACHING = caching

            self.set_ldl('all', 0.2)
            self.set_ldh('all', 0.5)
            for i in range(self._chan_range[0], self._chan_range[1]+1):
                assert approx(self.get_ldl(i)) == 0.2
                assert approx(self.get_ldh(i)) == 0.5

            
            for i in range(self._chan_range[0], self._chan_range[1]+1):
                self.set_ldl(i, 0.25)
                self.set_ldh(i, 0.45)
                assert approx(self.get_ldl(i)) == 0.25
                assert approx(self.get_ldh(i)) == 0.45

            print(self.__info__())
            print(f"fpga temp is {self.get_fpga_temperature()}")

            ax = list(self.axes)[0]
            ax.move(0.3)
            assert approx(ax.position) == 0.3

        print("All tests ok :)")
