from bliss.common import axis
from id26.scripts.beamline_settings import ID26Settings

class ID26MonoMusstEncoders:

    def __init__ (self, name, config_dict):

        self.offset = ID26Settings (name, {'dial':0, 'user':0})
        
        self.axis = config_dict['axis']
        self.musst_name = config_dict['musst_controller'].name
        self.musst_channel = config_dict['musst_channel']
        self.steps_per_unit = config_dict['steps_per_unit']
        self.direction = config_dict['direction']
        self.name = name
        
    def __info__(self):
        string = f'{type(self).__name__}: {self.name} \n'  

        string += 'offsets:\n'
        string += f'    dial: {self.offset["dial"]}\n'
        string += f'    user: {self.offset["user"]}\n'
        string += f'\naxis      :         {self.axis.name}'
        string += f'\nmusst     :         {self.musst_name}'
        string += f'\nchannel   :         {self.musst_channel}'
        string += f'\nfactor    :         {self.steps_per_unit}'
        string += f'\ndirection :         {self.direction}'
        string += f'\n'
                                                                                                           
        return string

        
    def calibrate (self, energy=None):
        if energy:
            theta = self.dcm.energy2bragg(energy)
            self.axis.position = theta
        else:
            self.axis.sync_hard()
        self.offset['user'] = self.axis.position*self.steps_per_unit - self.read()
        self.offset['dial'] = self.axis.dial*self.steps_per_unit - self.read()

    def read (self):
        return self.direction*int(self.musst_controller.putget('?CH CH{0}'.format(self.musst_channel)).split()[0])
        
    def angle (self):
        return (self.read() + self.offset['user']) / self.steps_per_unit
        
    def energy (self):
        return (self.dcm.bragg2energy(self.angle()))

class ID26MonoMusstEncodersFix(ID26MonoMusstEncoders):

    def __init__ (self, name, config_dict):
        super().__init__(name, config_dict)
        self.axis.controller._initialize_axis(self.axis)
        
    def read (self):
        return self.direction*int(self.axis.controller.read_position(self.axis))
        
    

