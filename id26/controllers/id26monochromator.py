import numpy as np
import scipy.constants as codata

from bliss.common.logtools import log_warning
from bliss.common.utils import add_object_method
from bliss.controllers.monochromator.monochromator import Monochromator
from bliss.controllers.monochromator.tracker import EnergyTrackingObject
from bliss.controllers.monochromator.calcmotor import EnergyTrackerCalcMotor


class BackwardCompatibility:

    # xtals has been renamed into _xtals with same func
    @property
    def xtals(self):
        print(f"WARNING: {self.name}.xtals is going to be deprecated.")
        print(f"         please use {self.name}._xtals from now on.")
        log_warning(self, "Deprecated xtals called")
        return self._xtals


class ID26Monochromator(Monochromator, BackwardCompatibility):
    def _load_config(self):
        """Load Configuration"""

        super()._load_config()

        self._xtal_offset = self._xtals.get_xtals_config("offset")
        self._ty_offset = self.config.get("ty_offset", 0)
        self._ty_approx = self.config.get("ty_approx", 0.001)
        self._distances = self.config.get("distances", None)

    #################################################################
    ###
    ### Manage change between ID26 xtals
    ###

    def _xtal_is_in(self, xtal):
        ty_pos = self.xtal2ty(xtal)
        offset = abs(ty_pos - self._motors["ty"].position)

        return True if offset <= self._ty_approx else False

    def _xtal_change(self, xtal):

        if not self._xtal_is_in(xtal):
            # self._motors["ty"].mov(eself.xtal2ty(xtal))
            print(f'Please move {self._motors["ty"].name} to {self.xtal2ty(xtal)}')

    def xtal2ty(self, xtal):
        # add fix offset on ty (14.9)
        new_ty_pos = self._ty_offset

        # Add xtals position
        new_ty_pos += self._xtal_offset[xtal]

        # get mono center position according to mirror angle (rotyz)
        tan_val = np.tan(2 * self._motors["mirror"].position / 1000)
        new_ty_pos += tan_val * (self._distances["mono"] - self._distances["hex1"])

        return new_ty_pos

    def energy2steps(self, energy):
        res = self.energy2bragg(energy) * self._motors["bragg"].steps_per_unit
        return res

    def steps2energy(self, steps):
        res = self.bragg2energy(steps / self._motors["bragg"].steps_per_unit)
        return res


def _polynom(coeff_list, x):
    y = 0
    for i, coef in enumerate(coeff_list):
        y += coef * np.power(x, i)
    return y


class ID26EnergyTrackingObject(EnergyTrackingObject):
    def __init__(self, config):
        super().__init__(config)

    def _ene2gap(self, energy, params):

        hc_over_e = (
            codata.Planck * codata.speed_of_light / codata.elementary_charge * 1e7
        )
        gap_off = params["TG0"]
        lambda_0 = params["TL0"]
        gamma_machine = params["TGAM"]
        B_0 = params["TB0"]
        harm = params["harmonic"]

        K = (
            codata.elementary_charge
            / (2 * np.pi * codata.electron_mass * codata.speed_of_light)
            * 1e-3
            * B_0
            * lambda_0
        )
        wavelength = hc_over_e / energy
        k = np.sqrt(4e-7 * harm * wavelength / lambda_0 * gamma_machine**2 - 2)
        undu = gap_off - lambda_0 / np.pi * np.log(k / K)

        return undu

    def _gap2ene(self, gap, params):

        hc_over_e = (
            codata.Planck * codata.speed_of_light / codata.elementary_charge * 1e7
        )
        gap_off = params["TG0"]
        lambda_0 = params["TL0"]
        gamma_machine = params["TGAM"]
        B_0 = params["TB0"]
        harm = params["harmonic"]
        K = (
            codata.elementary_charge
            / (2 * np.pi * codata.electron_mass * codata.speed_of_light)
            * 1e-3
            * B_0
            * lambda_0
        )
        wavelength = (
            lambda_0
            / (4e-7 * harm * gamma_machine**2)
            * (K**2 * np.exp(2 * np.pi * (gap_off - gap) / lambda_0) + 2)
        )
        ene = hc_over_e / wavelength

        return ene

    def _ene2ppth(self, energy, params):

        if "range" in params:
            if energy < params["range"][0] or energy > params["range"][1]:
                log_warning(
                    self,
                    "energy out of calibration range {0} for ppth1".format(
                        params["range"],
                    ),
                )
        return _polynom(params["polynom"], energy)

    #    def _ppth2ene (self, tth, params):
    #        return np.nan

    def _pth_center(self, energy, params):
        return np.degrees(np.arcsin(3.0108 / energy))


class ID26EnergyTrackerCalcMotor(EnergyTrackerCalcMotor):
    def energy_dial_discarded(self, energy_user):
        return energy_user
