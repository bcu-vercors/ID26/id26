import numpy

from bliss.common.axis import Axis
from bliss.common.cleanup import cleanup
from bliss.common.logtools import log_warning, log_error, log_debug
from bliss.common.scans.step_by_step import ascan

from id26.scripts.fscan import fscan, FSCAN_PAR, FSCAN
from id26.controllers.measurement import ID26Measurement, load_gui_param

SCAN_MODES = {
    'fluo': 'fluorescence',
    'ets': 'et_step',
    'etc': 'et_continuous',
    }


def _cleanup_undu_tracking(undu_tracking = None):
    if undu_tracking is not None:
        FSCAN_PAR['id_linked'], FSCAN_PAR['id_sync'] = undu_tracking

class Rixs(ID26Measurement):
    
    def __init__(self, name, measurement):
        
        super().__init__(name, measurement)
        self._instance_type = 'rixs'
        self.hspectro = None

        
    @load_gui_param
    def check (self, scan_time = None, number_scans = None, scan_mode = None, verbose = True):

        scan_time = self.get('scan_time') if scan_time is None else scan_time
        number_scans = max(self.get('number_scans',1) ,1) if number_scans is None else number_scans
        scan_mode = self.get('scan_mode') if scan_mode is None else scan_mode
        
        if scan_mode in SCAN_MODES:
            scan_mode = SCAN_MODES[scan_mode]

        assert scan_mode in SCAN_MODES.values(), "Please specify scan_mode within ['fluorescence','et_step','et_continuous'] (et: energy transfert)"

        if scan_mode.startswith('fluo'):
            assert self.get('xes_start') is not None, 'Please specify xes_start and xes_end parameters for et_start and et_end calculation'
            assert self.get('xes_end') is not None, 'Please specify xes_end parameter'
            assert self.get('xes_int'), 'Please specify non null xes_int'
        else:
            assert self.get('et_start') is not None, 'Please specify et_start and et_end parameters'
            assert self.get('et_end') is not None, 'Please specify et_end parameter'
            assert self.get('et_int'), 'Please specify non null et_int'
            
        assert self.get('xas_start') is not None, 'Please specify xas_start and xas_end parameters'
        assert self.get('xas_end') is not None, 'Please specify xas_end parameter'
            
        if scan_time is None: 
            scan_time = 1 if scan_mode.startswith('fluo') else 20
        else:
            scan_time = abs(scan_time)
            assert scan_time, "Scan Acquisition time cannot be 0"

        if self.move_scan_mode is not None:
            assert self.current_sample is not None,'current_sample not defined. Cannot apply any radiation damage protection.'

        if self.move_scan_mode == 'point' and 'step' not in scan_mode:
            self.move_scan_mode = 'scan_segment'

        self['number_scans_pos'] = 1 if self.get('number_scans_pos') is None else self.get('number_scans_pos')
        self['xas_int'] = 0.0001 if self.get('xas_int') is None else self.get('xas_int')

        if self.get('undu_tracking') is None:
            self['undu_tracking']=False
            
        if verbose:
            
            print ('number of scans:',number_scans)
            print ('radiation protection:', self.move_scan_mode)
            print ('Start Incident Energy:',self['xas_start'])
            print ('End Incident Energy:',self['xas_end'])
            print ('Incident Energy Step:',self['xas_int'])
            if scan_mode.startswith('et'):
                print ('energy transfert scan: Per point Acq. time(s):', scan_time)
            else:
                print ('Fscan Acq. time(s):', scan_time)
                if self.move_scan_mode:
                    print ('number of scan per sample position:',self['number_scans_pos'])
            if scan_mode.startswith('et'):
                print ('energy transfert Start:',self['et_start'])
                print ('energy transfert End:',self['et_end'])
                print ('energy transfert Step:',self['et_int'])
            else:
                print ('Start emission Energy:',self['xes_start'])
                print ('End emission Energy:',self['xes_end'])
                print ('emission Energy Step:',self['xes_int'])
            print ('shg:',self.get('shg'))
            print ('svg:', self.get('svg'))
            print ('hutch:', self.hutch)

        return scan_time, number_scans, scan_mode 
        
    
    def scan(self, scan_time = None, number_scans = None, scan_mode = None):

        scan_time, number_scans, scan_mode = self.check (scan_time, number_scans, scan_mode, False)

        motion_dict = {
            self.svg: self['svg'],
            self.shg: self['shg'],
        }
        #TODO sort out : Daiquiri does not have any dslit entry
        if isinstance (self.dslit, Axis) and 'dslit' in self:
            motion_dict[self.dslit] = self['dslit']
        self.move_together (motion_dict)
        #TODO wait=False

        self.radiation_damage_protect_init ()
        self.radiation_damage_protect_in_active_mg ()

        #Energy Transfert False : fscan (XAS) for each XES  
        #Energy Transfert True:   ascan (XES_calc) for each XAS

        with cleanup (
                self.radiation_damage_protect_nopoints ,
                self.radiation_damage_protect_in_active_mg_reset ,
        ):

            for i in range(number_scans):
                self.auto_offset()
                self.radiation_damage_protect_scan(protect_type='full')
                if scan_mode.startswith('fluo'):
                    return self._fluo_scan (scan_time)
                elif 'step' in scan_mode:
                    return self._et_step_scan (scan_time)
                elif 'cont' in scan_mode:
                    return self._et_continuous_scan (scan_time)

    
        
    def _fluo_scan(self, scan_time):

        rixs_scan_list = []
        e_energies = numpy.arange(self['xes_start'], (self['xes_end']+self['xes_int']/2), self['xes_int'])
        
        with cleanup (_cleanup_undu_tracking, undu_tracking=(FSCAN_PAR['id_linked'], FSCAN_PAR['id_sync'])):
            FSCAN_PAR['id_linked'] = self['undu_tracking']
            FSCAN_PAR['id_sync'] = self['undu_tracking']
                      
            for e_ene in e_energies:
                print("Moving xes_en to {}\n".format(e_ene))
                self.xes_en.move(e_ene)
                for j in range(self['number_scans_pos']): #number of scan per sample position
                      self.radiation_damage_protect_scan (protect_type='segment')
                      rixs_scan_list.append(fscan(self['xas_start'],self['xas_end'],scan_time,self['xas_int'], scan_info=self.scan_info))
                
        return rixs_scan_list
    
    def _et_step_scan (self, scan_time):
        rixs_scan_list = []
        npts = abs(round((self['et_end']-self['et_start'])/self['et_int']))
        a_energies = numpy.arange(self['xas_start'], (self['xas_end']+self['xas_int']/2), self['xas_int'])

        et_xes_start = a_energies-self['et_end']   
        et_xes_end = a_energies-self['et_start']   
        #TODO entry point for hspectro - what is it ? it is spectro_eh2 only, not relevant for texs spectro 
        #TODO whats that ? this ene_diff param is only used for rixs scans - why is it added to mmspectro ???
        if self.hspectro is not None:  # hspectro                    
            et_xes_start += self.hspectro.ref_params['ene_diff']
            et_xes_end += self.hspectro.ref_params['ene_diff']
        
        if self.hutch == 'eh1':
            if numpy.all(et_xes_start < et_xes_end):
                et_xes_start, et_xes_end = et_xes_end, et_xes_start # valid in texs
            assert numpy.all(et_xes_start > et_xes_end),f'et vectors not coherent start:{et_xes_start}, end: {et_xes_end}'
        else:
            if numpy.all(et_xes_start > et_xes_end):
                et_xes_start, et_xes_end = et_xes_end, et_xes_start # valid in eh2
            assert numpy.all(et_xes_start < et_xes_end),f'et vectors not coherent start:{et_xes_start}, end: {et_xes_end}'
                
        for a_ene, et_start, et_end in zip(a_energies, et_xes_start, et_xes_end):
            print("Moving {} energy to {:}".format(
                self.dcm.motors['energy_tracker'].name,
                a_ene,
            ))
            #
            # et_step_scan trackers must be en/disable from dcm
            #
            self.dcm.motors['energy_tracker'].move(a_ene) # or energy_undulator_motor/ENEUND?
                        
            print("Scan emission spectrometer from {:} to {:}\n".format(et_start, et_end))
            self.xes_en.move(et_start)
                    
            my_xes_scan = ascan(self.xes_en, et_start, et_end, npts, scan_time, scan_info=self.scan_info, run = False)

            self.radiation_damage_protect_scan(protect_type='segment')
            self.radiation_damage_protect_point(my_xes_scan)
                        
            if self.move_scan_mode == 'point':
                print('first index in the current scan', self._radiation_damage_preset.pm.get_index(self._current_sample))

            my_xes_scan.run()
            rixs_scan_list.append(my_xes_scan)
            
        return rixs_scan_list


     # Original way
     # # Should work
     
    # def _et_continuous_scan (self, scan_time): 
        # rixs_scan_list = []
        # fscan_start = []
        # fscan_end = []
        # xes_cut = self.get('xes_cut',0.001)
        # scan_time_per_point = scan_time/(self['xas_end']-self['xas_start'])*self['xas_int']
                               
        # xes_vector = numpy.arange(self['xas_end']-self['et_start']-xes_cut,
                                   # self['xas_start']-self['et_end']+xes_cut-self['et_int'],
                                   # -self['et_int'])

        # xas_vector = numpy.arange(self['xas_start'], self['xas_end']+self['xas_int'], self['xas_int'])
        
        # for idx, xes in enumerate(xes_vector):
            # if (xes+self['et_start']) <= self['xas_start'] and (xes+self['et_end']) <= self['xas_end']:
                # fscan_start.append(self['xas_start'])
                # fscan_end.append(xes+self['et_end'])
            # elif (xes+self['et_start']) > self['xas_start'] and (xes+self['et_end']) < self['xas_end']:
                # fscan_start.append(xes+self['et_start'])
                # fscan_end.append(xes+self['et_end'])
            # elif (xes+self['et_start']) <= self['xas_start'] and (xes+self['et_end']) > self['xas_end']: 
                # fscan_start.append(self['xas_start'])
                # fscan_end.append(self['xas_end'])
            # elif (xes+self['et_start']) > self['xas_start'] and (xes+self['et_end']) >= self['xas_end']:
                # fscan_start.append(xes+self['et_start'])
                # fscan_end.append(self['xas_end'])
            # else:
                # print('nothing done')
    
            # print(f"{xes:1.4f}" "   " f"{fscan_start[idx]:1.4f}" "   " f"{fscan_end[idx]:1.4f}" "   " f"{fscan_start[idx]-xes:1.4f}" "   " f"{fscan_end[idx]-xes:1.4f}" )
            
        # self.dcm.motors['energy_tracker'].move(self['xas_start'] + self['xas_end']/2)

        # for idx, xes in enumerate(xes_vector):
            # self.xes_en.move(xes)
            # self.radiation_damage_protect_scan (protect_type='segment')
            # fscan_scan_time = max((fscan_end[idx] - fscan_start[idx])/self['xas_int'] * scan_time_per_point,5)
            # rixs_scan_list.append(fscan(fscan_start[idx],fscan_end[idx], fscan_scan_time, self['xas_int'], scan_info=self.scan_info))
        
        # return rixs_scan_list
        
    def _et_continuous_scan (self, scan_time):  # MCL way
         
        rixs_scan_list = []
        
        scan_time_per_point = scan_time/(self['xas_end']-self['xas_start'])*self['xas_int']

        xes_cut = self.get('xes_cut',0.001)
        # why e_energies like this? Sami had to correct this
        e_energies = numpy.arange(self['xas_end']-self['et_start']-xes_cut ,
                                   self['xas_start']-self['et_end']+xes_cut-self['et_int'] ,
                                   -self['et_int'])
        
        
        et_xas_start = e_energies + self['et_start']
        et_xas_end = e_energies + self['et_end']
        
        for ix, e_ene in enumerate(e_energies):
            start = et_xas_start[ix]
            end = et_xas_end[ix]

            if start <= self['xas_start']:
                f1 = self['xas_start']
                if end <= self['xas_end']:
                    f2 = end
                else:
                    f2 = self['xas_end']
            else:
                f1 = start
                if end < self['xas_end']:
                    f2 = end
                else:
                    f2 = self['xas_end']
            
            et_xas_start[ix] = f1
            et_xas_end[ix] = f2
            
            print(f"{e_ene:1.4f}" "   " f"{f1:1.4f}" "   " f"{f2:1.4f}" "   " f"{f1-e_ene:1.4f}" "   " f"{f2-e_ene:1.4f}" )

        if self['undu_tracking'] is False:
            print("Moving energy trackers to {}\n".format((self['xas_start'] + self['xas_end'])/2))
            self.dcm.motors['energy_tracker'].move((self['xas_start'] + self['xas_end'])/2)

        with cleanup (_cleanup_undu_tracking, undu_tracking=(FSCAN_PAR['id_linked'], FSCAN_PAR['id_sync'])):
            FSCAN_PAR['id_linked'] = self['undu_tracking']
            FSCAN_PAR['id_sync'] = self['undu_tracking']
            
            for e_ene,start,end in zip (e_energies, et_xas_start, et_xas_end):
                print("Moving spectrometer to energy {}\n".format(e_ene))
                self.xes_en.move(e_ene)
                self.radiation_damage_protect_scan (protect_type='segment')
                fscan_scan_time = max((end - start)/self['xas_int'] * scan_time_per_point,5)
                try:
                    FSCAN._check(start, end, fscan_scan_time, self['xas_int'])
                except ValueError as e:
                    log_warning (
                        self,
                        "et_continuous_scan at {} rejected: {}".format(
                            e_ene,
                            e
                        ))
                    continue
                except AssertionError as e:
                    log_warning (
                        self,
                        "et_continuous_scan at {} rejected: {}".format(
                            e_ene,
                            e
                        ))
                    continue
            
                rixs_scan_list.append(fscan(start,end, fscan_scan_time, self['xas_int'], scan_info=self.scan_info))
        
        return rixs_scan_list
        
    
