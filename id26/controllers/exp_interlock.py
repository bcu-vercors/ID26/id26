"""
Reset Tango PLC interlock

Example yml file:

.. code-block:: yaml

    -
      class: ExpIL
      package: id26.controllers.exp_interlock
      name: expil
      uri: id20/v-il/0


"""

from bliss import global_map
from bliss.common.tango import DeviceProxy

class ExpIL:

    def __init__(self, name, config):
        self._tango_uri = config.get("uri")

        self.__name = name
        self.__config = config
        self.__control = DeviceProxy(self._tango_uri)

        global_map.register(self, children_list=[self.__control], tag=f"ExpIL:{name}")

    @property
    def proxy(self):
        """Return the proxy to the device server"""
        return self.__control

    @property
    def name(self):
        """A unique name"""
        return self.__name

    @property
    def config(self):
        """Config of shutter"""
        return self.__config

    @property
    def state(self) -> str:
        return self.__control.state().name

    @property
    def status(self):
        return self.__control.status()

    def __info__(self):
        info_str = f"`{self.name}` ({self._tango_uri}): {self.state}\n"
        info_str += self.status
        return info_str

    def reset(self):
        self.__control.Reset()
