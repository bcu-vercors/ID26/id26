# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
Convention for tables (tab2, tab3) calculations:

    1 - We are using a direct coordiante system (x,y,z) as follow
    
                    ^
                    | Z axis
                    | 
        X axis      |
          <----------
                   /
                  /
                 / Y axis
                /
                v

    2 - The rotation are name rotx, roty, rotz around respectively the 
        X, Y and Z axis with counter-clockwize positive value
    
    3 - all parameters relative to legs are following the same convention:
            - coordinates of "lega" are named: "ax", "ay"
            - coordinates of "legb" are named: "bx", "by"
            - ...
            
    4 - all parameters are given in meters
            
    5 - The calculation takes into account the "unit" defined for each
        real or calculated motors.
        if the "unit" field is missing, the universal units are used: 
        meters (m) for distance, radians (rad) for angles
"""

import numpy as np
import tabulate

from bliss.common.logtools import log_debug, log_info
from bliss.controllers.motor import CalcController
from bliss.physics.units import ur
from bliss.common.utils import ColorTags, BOLD 
from bliss.shell.standard import wm

"""
Table with 2 legs:

    - Definition:
        
        + the 2 legs are tagged "lega" and "legb"
        + the height of the table is tagged "trans"
        + X-axis is the axis passing thrue lega and legb, positive
          from a to b
        + The rotation is around Y-axis, perpendicular to
          the [lega-legb] axis (X-axis)
        + the rotation is tagged "ry"
        + C is the center of rotation and its height is represented by the
          position of the "dz" axis
        + C is the origin of the X/Y/Z coordinates system
        + "ax" is the coordinate of the leg "lega" in the X/Y/Z coordinates system
        + "bx" is the coordinate of the leg "legb" in the X/Y/Z coordinates system

    - Example if C is inside the 2 legs
        + "ax" is a negative value in meter
        + "bx" is a positive value in meter

                   Z-axis
    		            ^
    		            |
                  ______|______________ 
                 /      |             /
                /       |...ax..>|   /
     X axis    /        |        |  /     X axis
     <--------/-.-------C--------.-/----<------
             /  |<.bx../         |/
            /   |     /          |
           /____|____/__________/|       
                |   /            |
                |  v Y-axis      | 
                |                |
              legb             lega
              
        + YML file:
            - plugin: emotion
              package: bm23.tab2
              class: tab2
              name: btable
              ax:  1.250
              bx: -1.250
              axes:
                - name: $sim_btz1
                  tags: real lega
                - name: $sim_btz2
                  tags: real legb
                - name: sim_btz
                  tags: trans
                  unit: mm
                - name: sim_btr
                  tags: rot
                  unit: deg

- Example if C is before the 2 legs
    - "ax" is a positive value
    - "ay" is a positive value

                                        Z-axis
    		                              ^
    		                              |
                  _____________________   |
                 /                    /   |
                /<.......bx...............|
     X axis    /|                   /     |    X axis
     <--------/-.----------------.-/------C----------
             /  |                |/      /
            /   |                |<.ax../
           /____|_______________/|     /        
                |                |    /
                |                |   v Y axis
                |                |
              legb             lega
              
        + YML file:
            - plugin: emotion
              package: bm23.table
              class: tab2
              name: btable
              ax:  1.250
              bx: -1.250
              axes:
                - name: $sim_btz1
                  tags: real lega
                - name: $sim_btz2
                  tags: real legb
                - name: sim_btz
                  tags: tz
                  unit: mm
                - name: sim_btr
                  tags: ry
                  unit: deg
"""
class tab2(CalcController):
    
    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)
    
        self.ax = (self.config.config_dict.get("ax", None)*ur.m).magnitude
        self.bx = (self.config.config_dict.get("bx", None)*ur.m).magnitude
        
        log_debug(self, "ax = %f" % self.ax)
        log_debug(self, "bx = %f" % self.bx)

    def initialize(self):
        CalcController.initialize(self)
        
        # get all motor units
        self.lega_unit = self._tagged["lega"][0].unit
        self.legb_unit = self._tagged["legb"][0].unit
        self.tz_unit = self._tagged["trans"][0].unit
        self.ry_unit = self._tagged["rot"][0].unit
        
    def __info__(self):
        mystr = f"Type: tab2\n\n"
        mystr += f"ax[{self.ax}]\n"
        mystr += f"bx[{self.bx}]\n\n"
        title = []        
        user = []
        for axis in self.pseudos:
            title.append(f"{axis.name}[{axis.unit}]")
            user.append(f"{axis.position:.4f}")
        mystr += tabulate.tabulate([title, user], tablefmt="plain")
        mystr += "\n\n"
        title = []        
        user = []
        for axis in self.reals:
            title.append(f"{axis.name}[{axis.unit}]")
            user.append(f"{axis.position:.4f}")
        mystr += tabulate.tabulate([title, user], tablefmt="plain")
        return mystr
        
    def wa(self):
        mot_list = self.pseudos + self.reals
        wm(*mot_list)
        
    def calc_from_real(self, real_dict):

        log_info(self, "calc_from_real()")
        
        lega = (real_dict["lega"]*ur.parse_units(self.lega_unit)).to("m").magnitude
        legb = (real_dict["legb"]*ur.parse_units(self.legb_unit)).to("m").magnitude
        
        if not isinstance(lega, np.ndarray):
            lega = np.array([lega], dtype=float)
            legb = np.array([legb], dtype=float)
            
        ry = np.arctan((lega - legb) / (self.bx - self.ax))
        tz = lega + self.ax * np.tan(ry)
        
        ry = (ry*ur.rad).to(self.ry_unit).magnitude
        tz = (tz*ur.m).to(self.tz_unit).magnitude
        
        if len(lega) == 1:
            return {"trans": tz[0], "rot": ry[0]}
        return {"trans": tz, "rot": ry}

    def calc_to_real(self, calc_dict):

        log_info(self, "calc_to_real()")
        
        tz = (calc_dict["trans"]*ur.parse_units(self.tz_unit)).to("m").magnitude
        ry = (calc_dict["rot"]*ur.parse_units(self.ry_unit)).to("rad").magnitude
        
        if not isinstance(tz, np.ndarray):
            tz = np.array([tz], dtype=float)
            ry = np.array([ry], dtype=float)

        lega = tz - self.ax * np.tan(ry)
        legb = tz - self.bx * np.tan(ry)
        
        lega = (lega*ur.m).to(self.lega_unit).magnitude
        legb = (legb*ur.m).to(self.legb_unit).magnitude
        
        if len(tz) == 1:
            return {"lega": lega[0], "legb": legb[0]}
        return {"lega": lega, "legb": legb}
                
        
class tab3(CalcController):
    
    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)
         
        self.ax = (self.config.config_dict.get("ax", None)*ur.m).magnitude
        self.ay = (self.config.config_dict.get("ay", None)*ur.m).magnitude
        
        self.bx = (self.config.config_dict.get("bx", None)*ur.m).magnitude
        self.by = (self.config.config_dict.get("by", None)*ur.m).magnitude
        
        self.cx = (self.config.config_dict.get("cx", None)*ur.m).magnitude
        self.cy = (self.config.config_dict.get("cy", None)*ur.m).magnitude
        
        self.mat = np.array([[1, self.ay, -self.ax],[1, self.by, -self.bx], [1, self.cy, -self.cx]])
        self.invmat = np.linalg.inv(self.mat)

    def initialize(self):
        CalcController.initialize(self)
        
        # get all motor units
        self.lega_unit = self._tagged["lega"][0].unit
        self.legb_unit = self._tagged["legb"][0].unit
        self.legc_unit = self._tagged["legc"][0].unit
        self.tz_unit = self._tagged["tz"][0].unit
        self.rx_unit = self._tagged["rx"][0].unit        
        self.ry_unit = self._tagged["ry"][0].unit        
        
    def __info__(self):
        mystr = f"Type: tab3\n\n"
        mystr += f"ax[{self.ax}] ay[{self.ay}]\n"
        mystr += f"bx[{self.bx}] by[{self.by}]\n"
        mystr += f"cx[{self.cx}] cy[{self.cy}]\n\n"
        title = []        
        user = []
        for axis in self.pseudos:
            title.append(f"{axis.name}[{axis.unit}]")
            user.append(f"{axis.position:.4f}")
        mystr += tabulate.tabulate([title, user], tablefmt="plain")
        mystr += "\n\n"
        title = []        
        user = []
        for axis in self.reals:
            title.append(f"{axis.name}[{axis.unit}]")
            user.append(f"{axis.position:.4f}")
        mystr += tabulate.tabulate([title, user], tablefmt="plain")
        return mystr
        
    def wa(self):
        mot_list = self.pseudos + self.reals
        wm(*mot_list)

    def calc_from_real(self, real_dict):

        log_info(self, "calc_from_real()")
        
        lega = (real_dict["lega"]*ur.parse_units(self.lega_unit)).to("m").magnitude
        legb = (real_dict["legb"]*ur.parse_units(self.legb_unit)).to("m").magnitude
        legc = (real_dict["legc"]*ur.parse_units(self.legc_unit)).to("m").magnitude
        
        if not isinstance(lega, np.ndarray):
            lega = np.array([lega], dtype=float)
            legb = np.array([legb], dtype=float)
            legc = np.array([legc], dtype=float)

        for ind in range(len(lega)):
            real_pos = np.array([[lega[ind]], [legb[ind]], [legc[ind]]])
            calc_pos = self.invmat.dot(real_pos)
            
        tz = (calc_pos[0]*ur.m).to(self.tz_unit).magnitude
        rx = (calc_pos[1]*ur.rad).to(self.rx_unit).magnitude
        ry = (calc_pos[2]*ur.rad).to(self.ry_unit).magnitude
        
        if len(lega) == 1:
            return {"tz": tz[0], "rx": rx[0], "ry": ry[0]}
        return {"tz": tz, "rx": rx, "ry": ry}
        
    def calc_to_real(self, positions_dict):

        log_info(self, "calc_to_real()")
        
        tz = (real_dict["tz"]*ur.parse_units(self.tz_unit)).to("m").magnitude
        rx = (real_dict["rx"]*ur.parse_units(self.rx_unit)).to("rad").magnitude
        ry = (real_dict["ry"]*ur.parse_units(self.ry_unit)).to("rad").magnitude
        
        if not isinstance(tz, np.ndarray):
            tz = np.array([tz], dtype=float)
            rx = np.array([rx], dtype=float)
            ry = np.array([ry], dtype=float)
        
        for ind in range(len(tz)):
            calc_pos = np.array([[tz[ind]], [rx[ind]], [ry[ind]]])
            real_pos = self.mat.dot(calc_pos)
            
        lega = (real_pos[0]*ur.m).to(self.lega_unit).magnitude
        legb = (real_pos[1]*ur.m).to(self.legb_unit).magnitude
        legc = (real_pos[2]*ur.m).to(self.legc_unit).magnitude
        
        if len(tz) == 1:
            return {"lega": lega[0], "legb": legb[0], "legc": legc[0]}
        return {"lega": lega, "legb": legb, "legc": legc}
        

