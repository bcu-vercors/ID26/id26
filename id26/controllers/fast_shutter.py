import time
from enum import Enum
from bliss import setup_globals, global_map
from bliss.comm.serial import CommunicationError
#from bliss.common import event
from bliss.common.shutter import BaseShutter, BaseShutterState
#from bliss.config.channels import Channel
from bliss.config.channels import Cache
from bliss.config.settings import SimpleSetting
from bliss.scanning.chain import ChainPreset, ChainIterationPreset
from bliss.common import event
from bliss.common.logtools import log_debug
from bliss.common.protocols import HasMetadataForScan

'''
package: id26.controllers.fast_shutter
class: ID26FastShutter
name: fsh2
multiplexer: $multiplexer_opiom_cc1
opiom: $opiom_cc1
'''

ID26FastShutterState = Enum(
    "ID26FastShutterState",
    dict(
        {
            "AUTO": "Auto",
        },
        **{item.name: item.value for item in BaseShutterState},
    ),
)

 
#class ID26FastShutter_with_chainpreset:
class ID26FastShutter(BaseShutter, HasMetadataForScan):

    def __init__ (self, name, config_dict):
        self.__name = name
        self.__config = config_dict
        self.__control = config_dict['multiplexer']
        self.__state = Cache(self, "state", default_value=ID26FastShutterState.UNKNOWN, callback=self.__state_changed)
        global_map.register(self, children_list=[self.__control], tag=f"Shutter:{name}")

        self.opiom = config_dict['opiom']
        self.multiplexer = config_dict['multiplexer']
        self._auto = SimpleSetting ('fson',default_value = 0)
        self._delay = SimpleSetting ('fsdelay',default_value = 0.05)
        
#        self._preset = None
# to be replaced by a call from session_setup.py to
#            from id26.controllers.fast_shutter import FShutterChainIterationPreset
#            DEFAULT_CHAIN.add_preset(FShutterChainIterationPreset(fsh,name='FAST SHUTTER'))


    def __state_changed(self, state):
       """Send a signal when state changes"""
       event.send(self, "state", state)

      
    @property
    def name(self):
        """A unique name"""
        return self.__name

    @property
    def config(self):
        """Config of shutter"""
        return self.__config

    @property
    def state(self):
        try:
            if self._auto.get():
                self.__state.value = ID26FastShutterState.AUTO
            else:
                state = self.multiplexer.getOutputStat('SOFT_SHUTTER')
                if 'CLOSE' in state:
                    self.__state.value = ID26FastShutterState.CLOSED
                else:
                    self.__state.value = ID26FastShutterState.__members__[state]
        except CommunicationError:
            self.__state.value = ID26FastShutterState.UNKNOWN
            #raise RuntimeError(f"Communication Error with {self.opiom}")
        return self.__state.value
            
    def __info__(self):
        msg = "Shutter is " + self.state.value
        msg += "\nAutomatic opening during counting: "
        msg += 'ON' if self._auto.get() else 'OFF'
        msg += "\nAutomatic opening during zap: "
        msg += self.multiplexer.getOutputStat('FSON')
        msg += f'\ndelay for shutter opening is {self.delay*1000} ms'
        return(msg)


    def scan_metadata (self):
        return {'delay': self.delay, 'state': self.state_string}
    
    @property
    def delay(self):
        return self._delay.get()

    @delay.setter
    def delay (self, value):
        self._delay.set(value)
        
    def open (self):
        self.multiplexer.switch('soft_shutter','open')
        if not self._auto.get():
            self.__state.value = ID26FastShutterState.OPEN

    def close (self):
        self.multiplexer.switch('soft_shutter','close')
        if not self._auto.get():
            self.__state.value = ID26FastShutterState.CLOSED

    def open_fast (self):
        if self.opiom.comm_ack('IMA 0x01 0x01') != 'OK':
            raise RuntimeError ('Bad answer from opiom')
        if not self._auto.get():
            self.__state.value = ID26FastShutterState.OPEN

    def close_fast (self):
        if self.opiom.comm_ack('IMA 0x00 0x01') != 'OK':
            raise RuntimeError ('Bad answer from opiom')
        if not self._auto.get():
            self.__state.value = ID26FastShutterState.CLOSED

    def auto_on (self):
        self.multiplexer.switch('soft_shutter','close')
        self.multiplexer.switch('fson','on')
        self._auto.set(1)
        self.__state.value = ID26FastShutterState.AUTO

    def auto_off (self):
        #self.multiplexer.switch('soft_shutter','close')
        self.multiplexer.switch('fson','off')
        self._auto.set(0)
        print (f'fast shutter is {self.state}')

        
class FShutterChainPreset (ChainPreset):
    def __init__(self, sh_ctrl):
        ChainPreset.__init__(self)
        self.sh_ctrl = sh_ctrl
        print (f'{self.sh_ctrl.name} will automatically operate Fast Shutter')

    def prepare(self, acq_chain):
        pass

    def start(self, acq_chain):
        
        print (f'{self.sh_ctrl.name} opens Fast Shutter')
        self.sh_ctrl.open_fast()
        time.sleep(self.sh_ctrl.delay)
        
    def stop(self, acq_chain):
        print (f'{self.sh_ctrl.name} closes Fast Shutter')
        self.sh_ctrl.close_fast()

            
class FShutterChainIterationPreset(ChainPreset):
    
    def __init__(self, sh_ctrl):
        ChainPreset.__init__(self)
        self.sh_ctrl = sh_ctrl

    class Iterator(ChainIterationPreset):
        
        def __init__(self,iteration_nb, sh_ctrl):
            self.iteration = iteration_nb
            self.sh_ctrl = sh_ctrl
            
        def prepare(self):
            #print(f"Preparing iteration {self.iteration}")
            pass
        
        def start(self):
            if not self.sh_ctrl._auto.get():
                return 
            log_debug(
                self.sh_ctrl,
                'open_fast',
            )
            self.sh_ctrl.open_fast()
            time.sleep(self.sh_ctrl.delay)
            log_debug(
                self.sh_ctrl,
                'open_fast done',
            )
            
        def stop(self):
            if not self.sh_ctrl._auto.get():
                return 
            log_debug(
                self.sh_ctrl,
                'close_fast',
            )
            #print (f'{self.sh_ctrl.name} closes Fast Shutter')
            self.sh_ctrl.close_fast()
            log_debug(
                self.sh_ctrl,
                'close_fast done',
            )


                
    def get_iterator(self,acq_chain):
        iteration_nb = 0
        while True:
            yield FShutterChainIterationPreset.Iterator(iteration_nb, self.sh_ctrl)
            iteration_nb += 1

            

#ftimescan provisionnal bricolage
#ftimescan.add_chain_preset(FShutterFTimeScanChainPreset (fsh))

class FShutterFTimeScanChainPreset (ChainPreset):
    def __init__(self, sh_ctrl):
        ChainPreset.__init__(self)
        self.sh_ctrl = sh_ctrl
        if self.sh_ctrl._auto.get():
            print (f'{self.sh_ctrl.name} will automatically operate Fast Shutter')

    def prepare(self, acq_chain):
        pass

    def start(self, acq_chain):
        if self.sh_ctrl._auto.get():
           print (f'{self.sh_ctrl.name} opens Fast Shutter')
           if self.sh_ctrl.opiom.comm_ack('IMA 0x1 0x03') != 'OK':
              raise RuntimeError ('Bad answer from opiom')
           time.sleep(self.sh_ctrl.delay)
        
    def stop(self, acq_chain):
        if self.sh_ctrl._auto.get():
            print (f'{self.sh_ctrl.name} closes Fast Shutter')
            if self.sh_ctrl.opiom.comm_ack('IMA 0x2 0x03') != 'OK':
              raise RuntimeError ('Bad answer from opiom')
          

          
#opiom output counter
#from bliss.common.counter import SoftCounter
#c = SoftCounter(OpiomOutput(fsh.opiom, 'get_output')


class OpiomOutput:
    def __init__(self, opiom):
        self.ctrl = opiom

    def get_output(self):
        res= self.ctrl.comm("?O")
        res2= self.ctrl.registers()['IMA']
        #print (res,res2)
        return int(res,base=16)&8

    
          

          
#old way class ID26FastShutter:

#class ID26FastShutter:
class old_ID26FastShutter_with_opiom_fson:

    def __init__ (self, name, config_dict):
        self.opiom = config_dict['opiom']
        self.multiplexer = config_dict['multiplexer']
        self.name = name
        

    def __info__(self):
        msg = "Shutter is " + self.state()
        msg += "\nAutomatic opening during counting: "
        msg += 'ON' if self.opiom.registers()['IMA']&0x2 else 'OFF'
        return(msg)
        
    def state(self):
        #print ('.multiplexer.getOutputStat(\'SOFT_SHUTTER\')',self.multiplexer.getOutputStat('SOFT_SHUTTER'))
        if self.multiplexer.getOutputStat('FSON') == 'ON':
            return 'AUTO'
        else:
            return (self.multiplexer.getOutputStat('SOFT_SHUTTER'))

    def open (self):
        self.multiplexer.switch('soft_shutter','open')
        #self.multiplexer.switch('fson','off')

    def close (self):
        self.multiplexer.switch('soft_shutter','close')
        #self.multiplexer.switch('fson','off') 

    def open_fast (self):
        if self.opiom.comm_ack('IMA 0x01 0x03') != 'OK':
            raise RuntimeError ('Bad answer from opiom')

    def close_fast (self):
        if self.opiom.comm_ack('IMA 0x00 0x03') != 'OK':
            raise RuntimeError ('Bad answer from opiom')

    def auto_on (self):
        self.multiplexer.switch('soft_shutter','close')
        self.multiplexer.switch('fson','on')

    def auto_off (self):
        #self.multiplexer.switch('soft_shutter','close')
        self.multiplexer.switch('fson','off')

