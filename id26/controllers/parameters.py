from id26.scripts.beamline_parameters import ID26Parameters, switch_instance
from id26.scripts.beamline_settings import ID26Settings

#_no_setup and call_backs not implemented for those classes
#note that callbacks will have to be converted from string (yml config) to a callable before it is passed to parameters or setting classes.

class ID26ParamWR (ID26Parameters):

    def __init__ (self, name, config_dict):

        self.config_dict = config_dict 
        _defaults = {}
        for item in config_dict['defaults']:
            _defaults.update (item.items())

        super().__init__(name, _defaults)

    
class ID26Setting (ID26Settings):

    def __init__ (self, name, config_dict):

        self.config_dict = config_dict 
        _defaults = {}
        for item in config_dict['defaults']:
            _defaults.update (item.items())

        super().__init__(name, _defaults)

class ID26Global(dict):

    def __init__ (self, name, config_dict):

        self.config_dict=config_dict
        values = {}
        for item in config_dict['values']:
            values.update (item.items())
        super().__init__ (values)

                
        
#backward compatibility    
class ID26Vars (ID26ParamWR):
    pass


