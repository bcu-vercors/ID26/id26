from bliss.common.logtools import log_warning

from id26.scripts.beamline_settings import ID26Settings


class ID26OffsetShutterChopper:

    def __init__(self, chopper):
        self.chopp = chopper
    
    def open (self):
        print ('openning chopper ',self.chopp.name)
        self.chopp.move(1)
        for i in range(3):
            if self.state() is not 'OPEN':
                print ('openning chopper ',self.chopp.name)
                self.chopp.move(1)
                if i==2:
                    log_warning (
                        self,
                        'after 4 open request chopper position is {0} (should be 1, or at least >= 0.2)'.format(self.chopp.position)
                        )

    def close (self):

        print ('closing chopper ',self.chopp.name)
        self.chopp.move(0)
        for i in range(3):
            if self.state() is not 'CLOSED':
                print ('closing chopper ',self.chopp.name)
                self.chopp.move(0)
                if i==2:
                    log_warning (
                        self,
                        'after 4 close request chopper position is {0} (should be 0, or at least < 0.2)'.format(self.chopp.position)
                        )

    def state (self):
        return 'CLOSED' if self.chopp.position < 0.2 else 'OPEN'

    
class ID26OffsetShutterFShAtt:

    def __init__(self, shutter_info):
        
        for f in shutter_info:
            self.f=f
            keys = f.keys()
            if 'fshutter' in keys:
                self.shutter = f['fshutter']
            if 'attenuator' in keys:
                self.atten = f['attenuator']


    def open (self):
        if self.shutter.state != ID26FastShutterState.AUTO: 
            print ('opening', self.shutter.name)
            self.shutter.open()
        print ('opening', self.atten.name)
        self.atten.factor(63)

    def close (self):
        if self.shutter.state != ID26FastShutterState.AUTO: 
            print ('closing', self.shutter.name)
            self.shutter.close()
        print ('closing', self.atten.name)
        self.atten.factor(0)

    def state (self):
        status1 = self.shutter.state.value.upper()

        status2 = 'CLOSED' if self.atten.factor() == 0 else 'OPEN'
        status2 = 'OPEN' if self.atten.factor() == 63 else 'CLOSED'

        print (self.shutter.name, 'status:', status1)
        print (self.atten.name, 'status:', status2)
        
        if status1 in ['AUTO', 'CLOSED'] and status2 == 'CLOSED':
            return 'CLOSED'
        if status1 in ['AUTO', 'OPEN'] and status2 == 'OPEN':
            return 'OPEN'
        return 'FAULT'
        
       
       
class ID26OffsetShutter3:

    def __init__(self, *args, **kwargs):
        pass

    def open (self):
        print ('open - not implemented')

    def close (self):
        print ('close - not implemented')

    def state (self):
        print ('state - not implemented')



TYPES = {
    'chopper':     ID26OffsetShutterChopper,
    'fshutter':    ID26OffsetShutterFShAtt,
    'fake':        ID26OffsetShutter3,
}



class ID26OffsetShutter (ID26Settings):
    '''
    openclose controller object for DarkCounter class
    '''
    def __init__ (self, name, config_dict):

        self._types = list(TYPES.keys())
        self._classes = list(TYPES.values())

        super().__init__ (name,
                          defaults = {'type': self._types[0]},
                          callbacks = {'type': self._cb_check},
        )
        
        config_dict['fake']=None
        
        self._config_obj = list(map(lambda x: config_dict[x],self._types))
        self._controller = None

        self._cb_check (self.get('type'))


    def _cb_check (self, choice):


        if choice in self._types:
            self._controller = TYPES[choice].__call__(self._config_obj[self._types.index(choice)])
            return choice           
        else:
            print (f'{choice} not accepted: shutter type must be one of : {self._types}')
            if self._controller is None:
                self._controller = self._classes[0].__call__(self._config_obj[0])
                return TYPES[0]
            return self.get('type')
        
    def open (self):
        self._controller.open()
    
    def close (self):
        self._controller.close()

    @property    
    def state (self):
        return self._controller.state()

    def is_open (self):
        return True if self._controller.state() == 'OPEN' else False

    def is_closed (self):
        return True if self._controller.state() == 'CLOSED' else False

        
