from bliss.flint.client.plots import BasePlot
  
   

class PositionManagerPlotInBliss(BasePlot):
    """This class provides the API which is exposed by BLISS.

    It is basically a proxy to the real plot in Flint.
    """

    WIDGET = "id26.controllers.flint_radiation_damage.PositionManagerPlotInFlint"
    """Qualified name of the plot living in Flint"""

    def clear_data(self):
        """You can define methods
        """
        # The submit is rused to call the real method of the plot in FLint
        self.submit("clear")
        
    def add_pattern(self, x, y, count, pname, pcolor, width, height, display_indices):
        # TODO: doc
        self.submit("addPattern", x, y, count, pname, pcolor, width, height, display_indices)
        

    def set_xlabel(self, xlabel):
        self.submit("setXlabel", xlabel)
        
    def set_ylabel(self, ylabel):
        self.submit("setYlabel", ylabel)  
        
    def update_limits(self, xmin, xmax, ymin, ymax):
        self.submit("updateLimits", xmin, xmax, ymin, ymax)
        
    def finalize_plot(self):
        return self.submit("finalizePlot")
        

    def get_last_update(self):
        #TODO: what should go here?
        """
        Methods can also return values.

        Which will also be pickled (standard Python serialization library).
        """
        return self.submit("getLastUpdate")


