from bliss.scanning.scan_meta import get_user_scan_meta
from bliss.config.channels import Cache

from id26.helpers.helper_classes import ImmutableFunctions
'''
class: ID26attenuator
package: id26.controllers.attenuator
name: att
wago: $wcid26c
wago_logical_name : xiapf4
attenuation_unit: 0.02 
filters:
- Al 0.02
- Al 0.04
- Al 0.08
- Al 0.16
- Al 0.32
- Al 0.64

'''




class ID26Attenuator (ImmutableFunctions):

    def __init__ (self, name, config_dict):
        self.name = name
        self.config = config_dict

        self.wago = config_dict['wago']
        self.wkey = config_dict['wago_logical_name']
        self.unit = float(config_dict ['attenuation_unit'])
        self.filters = config_dict['filters']

        self.__factor = Cache(self, "factor", default_value=0)
        self.__thickness = Cache(self, "thickness", default_value=0)

        self.factor()
        self.thickness()
        
        self._init_meta_data_publishing()

        
    def _init_meta_data_publishing(self):
        if not self.name:
            user_warning(
                "to publish metadata the attenuator needs a name in config"
            )
            return
        scan_meta_obj = get_user_scan_meta()
        scan_meta_obj.instrument.set(self, lambda _: {self.name: self.metadata()})

    def metadata(self):
        """ 
        this is about metadata publishing to the h5 file AND ICAT
        #thickness = self.thickness()
        #factor = self.factor()
        """
        
        meta_dict = dict()

        meta_dict.update  (
            {
                'Al thickness in microns': 1000*self.__thickness.value,
                'factor': self.__factor.value,
            }
        )

        meta_dict["@NX_class"] = "NXattenuator"

        return meta_dict
        
    def __info__(self):
        msg = ''
        thickness = self.thickness()
        factor = self.factor()
        for ii, filt in enumerate(self.filters):
            msg += f"{filt} ({ii}) is {'IN' if factor&pow(2,ii) else 'out'}\n"
        msg += f"\ntotal factor: {factor} ;  total thickness: {thickness*1000} um of Al.\n"

        return (msg)
     
   
    def factor (self, value = None):
        if value != None:
            register = list(map(int,' '.join(bin(value)[2:].rjust(len(self.filters),'0')[::-1]).split()))       
            self.wago.set (self.wkey, register)
        register = self.wago.get(self.wkey)
        factor = int(''.join(list(map(str,register)))[::-1], base=2)
        self.__factor.value = factor        
        return factor
    
    def thickness (self, value = None):
        if value != None:
            newvalue = int(value/self.unit)
            self.factor(newvalue)
        value = self.unit*self.factor()
        self.__thickness.value = value        
        return value

    def insert (self, *channel):
        register = self.wago.get(self.wkey)
        for val in channel:
            register[val]=1
        self.wago.set(self.wkey, register)    
    
    def extract (self, *channel):
        register = self.wago.get(self.wkey)
        for val in channel:
            register[val]=0
        self.wago.set(self.wkey, register)    

        
    
