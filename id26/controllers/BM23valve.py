import click
import tabulate

from bliss import setup_globals
from bliss.config import settings
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW

class BM23valveManager:
    def __init__(self, name, config):
    
        self._name = name
        self._config = config

        self._valves = []
        valves_config = self._config.get("valves")
        index = 0
        for valve_config in valves_config:
            valve_name = valve_config.get("valve_name")
            wago_controller = valve_config.get("wago_controller")
            wago_channel = valve_config.get("wago_channel")
            self._valves.append(
                BM23valveObject(
                    index,
                    valve_name,
                    wago_controller,
                    wago_channel,
                    self
                )
            )
            index += 1
                
    def _set_valve_attr(self, valve):
        setattr(self, valve._fullname, valve)  
        
    def _del_valve_attr(self, valve):
        delattr(self, valve._fullname)  
        
    def __info__(self):
        lines = []
        for valve in self._valves:
            if valve._enabled.get():
                lines.append([valve._fullname, valve._state()])
        mystr = tabulate.tabulate(lines, tablefmt="plain")
        mystr += "\n"
        return mystr
    
    def _get_meta_data(self):
        meta_data = {}
        for valve in self._valves:
            if valve._enabled.get():
                meta_data[valve._fullname] = valve._state()
        return meta_data
            
    def _print_selection(self, motivation):
        print(f"\nSelect Valve to {motivation}:\n")
        line = [BOLD("#"), BOLD("Name"), BOLD("Current State")]
        if motivation.startswith("enable"):
            line.append(BOLD("Enabled"))
        if motivation.startswith("change state"):
            line.append(BOLD("States (0/1)"))
        lines = [line]
        for valve in self._valves:
            line = [BOLD(str(valve._index+1)), valve._name.get(), valve._state()]
            if motivation.startswith("enable"):
                line.append(valve._enabled.get())
            if motivation.startswith("change state"):
                state0 = valve._state_name[0].get()
                state1 = valve._state_name[1].get()
                line.append(f"({state0}/{state1})")
            lines.append(line)
        lines.append([""])
        qu = BOLD("q")
        lines.append([BOLD("  q"), "Quit"]) 
        mystr = tabulate.tabulate(lines, tablefmt="plain")
        print(mystr)
        rep = click.prompt(f"\nYour choice (type a Valve number or {qu} to quit)", default="q")
        return rep
            
    def enable(self):
        end = False
        while not end:
            rep = self._print_selection("enable/disable")
            if rep == "q":
                end = True
            else:
                rep = int(rep)
                if rep >= 1 and rep <= len(self._valves):
                    valve = self._valves[rep-1]
                    if valve._enabled.get():
                        valve._enabled.set(False)
                        self._del_valve_attr(valve)
                    else:
                        valve._enabled.set(True)
                        self._set_valve_attr(valve)  
    
    def _rename_valve(self, node, old_name, new_name):
        delattr(self, old_name)
        setattr(self, new_name, node)
            
    def rename_valve(self):
        end = False
        while not end:
            rep = self._print_selection("change valve name")
            if rep == "q":
                end = True
            else:
                rep = int(rep)
                if rep >= 1 and rep <= len(self._valves):
                    valve = self._valves[rep-1]
                    name = valve._name.get()
                    name = click.prompt(f"  New Name", default=name)
                    valve._rename_valve(name)
                else:
                    print(BOLD(f"Index {rep} dows not exist\n\n"))
            
    def rename_state(self):
        end = False
        while not end:
            rep = self._print_selection("change state name")
            if rep == "q":
                end = True
            else:
                rep = int(rep)
                if rep >= 1 and rep <= len(self._valves):
                    valve = self._valves[rep-1]
                    name0 = valve._state_name[0].get()
                    name0 = click.prompt(f"  State Name for value 0", default=name0)
                    name1 = valve._state_name[1].get()
                    name1 = click.prompt(f"  State Name for value 1", default=name1)
                    valve._rename_states(name0, name1)
                else:
                    print(BOLD(f"Index {rep} dows not exist\n\n"))
            
    def close_all(self):
        for valve in self._valves:
            if valve._enabled.get():
                valve.close()
        
class BM23valveObject:
    def __init__(self, index, name, wago_controller, wago_channel, controller):
        
        self._index = index
        self._wago_controller = wago_controller
        self._wago_channel = wago_channel
        self._controller = controller
        self._name = settings.SimpleSetting(
                            f"ValveManager_{self._wago_channel}_name", 
                            default_value=name
                        )
        self._enabled = settings.SimpleSetting(
                            f"ValveManager_{self._wago_channel}_enabled", 
                            default_value=True
                        )
        self._state_name = []
        self._state_name.append(settings.SimpleSetting(
                f"ValveManager_{self._wago_channel}_state_0", 
                default_value="close"
            )
        )
        self._state_name.append(settings.SimpleSetting(
                f"ValveManager_{self._wago_channel}_state_1", 
                default_value="open"
            )
        )
        self._set_state_attr()
        
        if self._enabled.get():
            self._controller._set_valve_attr(self)
    
    @property
    def _fullname(self):
        name = self._name.get()
        index = self._index
        return f"v{index+10:02d}_{name}"
    
    def __info__(self):
        rep = f"Name     : {self._name.get()}\n"
        rep += f"Fullname : {self._fullname}\n"
        rep += f"Wago     : {self._wago_controller.name}\n"
        rep += f"Channel  : {self._wago_channel}\n"
        rep += f"State    : {self._state()}"
        return rep
                
    def _set_state_attr(self):
        name = self._state_name[0].get()
        setattr(self, f"set_{name}", self._set_state_0)  
        name = self._state_name[1].get()
        setattr(self, f"set_{name}", self._set_state_1)  
        
    def _del_state_attr(self):
        name = self._state_name[0].get()
        delattr(self, f"set_{name}")  
        name = self._state_name[1].get()
        delattr(self, f"set_{name}")  
        
    def _rename_states(self, state_0, state_1):
        self._del_state_attr()
        self._state_name[0].set(state_0)
        self._state_name[1].set(state_1)
        self._set_state_attr()
        
    def _state(self):
        ch_val = self._wago_controller.get(self._wago_channel)
        return self._state_name[int(ch_val)].get()
        
    def _set_state_1(self):
        self._wago_controller.set(self._wago_channel, 1)
        
    def _set_state_0(self):
        self._wago_controller.set(self._wago_channel, 0)
    
    def _rename_valve(self, new_name):
        old_name = self._fullname
        self._name.set(new_name)
        self._controller._rename_valve(self, old_name, self._fullname)
