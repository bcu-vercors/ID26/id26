import logging
import time
import numpy as np

import matplotlib
matplotlib.use('Qt5Agg')

from PyQt5 import QtCore, QtGui, QtWidgets

from silx.gui import qt
from silx.gui.utils.matplotlib import FigureCanvasQTAgg
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

from matplotlib.figure import Figure
import matplotlib.pyplot as pl

from matplotlib import cm
from matplotlib.patches import Rectangle
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.lines import Line2D


_logger = logging.getLogger(__name__)

cmap = cm.Greys

class MplCanvas(FigureCanvasQTAgg):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        super(MplCanvas, self).__init__(fig)



class PositionManagerPlotInFlint(qt.QMainWindow):
    """
    This class describe the plot inside Flint.
    It's the only one which have to handle Qt and matplotlib.
    It must not be imported by BLISS or any of your scripts.
    """

    def __init__(self, parent=None):
        qt.QMainWindow.__init__(self, parent=parent)
        self.__plot = MplCanvas(self, width=5, height=4, dpi=100)
        self.__plot.setParent(self)
        self._ax = self.__plot.axes
        toolbar = NavigationToolbar(self.__plot, self)

        layout = qt.QVBoxLayout(self)
        layout.addWidget(toolbar)
        layout.addWidget(self.__plot)

        # Create a placeholder widget to hold our toolbar and canvas.
        widget = qt.QWidget()
        widget.setLayout(layout)
        self.setCentralWidget(widget)
        # Your own attributes
        self._x = None
        self._y = None
        self._lastUpdate = None
        self._custom_lines = []
        self._custom_legend = []

    #
    # Your own API
    #

    def clear(self):
        self._lastUpdate = None
        self._ax.clear()
        # clear also the custom legend
        self._custom_lines = []
        self._custom_legend = []

    def addPattern(self, x, y, count, pname, pcolor, width, height, display_indices):
        #print(width)
        self._lastUpdate = time.time()

        # Try-except to make sure no exception will be returned in BLISS
        # But instead logged inside Flint
        try:
            self._updatePlot(x, y, count, pname, pcolor, width, height, display_indices)
        except Exception:
            _logger.critical("Error while updating the plot", exc_info=True)

    def getLastUpdate(self):
        return self._lastUpdate
        
    def setXlabel(self, xlabel):
        self._ax.set_xlabel(xlabel)
    
    def setYlabel(self,ylabel):
        self._ax.set_ylabel(ylabel)
        
    def _updatePlot(self, x, y, count,pname, pcolor, width, height,display_indices):
        self._custom_legend.append(pname)
        self._custom_lines.append(Line2D([0],[0], color = pcolor))
        # main plotting of rectangles
        # three arrays (or lists): x, y, count
        
        for cix, (onex, oney, onecount) in enumerate(zip(x,y,count)):
        # TODO: depending on whether centered
            mycolor = cmap(count[cix]/np.max(count))
            self._ax.add_patch(Rectangle(xy = (onex,oney), width = width, height = height, fc = mycolor, ec=pcolor))
            if display_indices:
                self._ax.text(onex, oney, str(cix), color = pcolor)        
    def updateLimits(self, xmin, xmax, ymin, ymax):
        self._ax.set_xlim((xmin, xmax))
        self._ax.set_ylim((ymin, ymax))
        
    def finalizePlot(self):
        self._ax.grid(True)
        self._ax.legend(self._custom_lines, self._custom_legend)
        self._ax.set_aspect('equal')        
        return self.__plot.draw() 
 


