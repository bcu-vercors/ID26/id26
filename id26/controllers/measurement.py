import functools
import yaml
import numpy
import inspect

from math import sqrt

from bliss import current_session
from bliss.common.axis import Axis
from bliss.common.logtools import log_warning, log_error, log_debug
from bliss.common.motor_group import Group
from bliss.common.counter import SoftCounter
from bliss.common.measurementgroup import get_active as get_active_mg
from bliss.common.scans import loopscan
from bliss.config.static import get_config
from bliss.config.settings import HashObjSetting
from bliss.controllers.calccnt_background import BackgroundCalcCounterController
from bliss.scanning.scan_meta import get_user_scan_meta
from bliss.common.standard import cleanup

from id26.controllers.radiation_damage import RadiationDamagePreset, PositionManager

def load_gui_param (func):
    @functools.wraps(func)
    def wrapper (self, *args, **kwargs):

        if not self.use_daiquiri:
            return func(self, *args, **kwargs)

        scan_saving = current_session.scan_saving
        scan_path = scan_saving.base_path + '/'
        scan_path += scan_saving.proposal_dirname + '/'
        scan_path += scan_saving.beamline + '/'
        filename = scan_path + 'parameteriser/' + self._parameter_type +'/' 
        filename += self._instance_type
        filename += '_'+self.name +'.yml'

        try:
          with open(filename) as file:
            try:
                exp_values = {}
                values = yaml.load(file.read(), Loader=yaml.FullLoader)
                #print(values)
                if values.get("parameter_type") != self._parameter_type:
                    raise KeyError
                if values.get("instance_type") != self._instance_type:
                    raise KeyError
                if values.get("experiment") != None:
                    exp_filename = scan_path + 'parameteriser/experiment/experiment_' 
                    exp_filename += values['experiment'] + '.yml' if not values['experiment'].endswith('.yml') else ""
                    try:
                      with open(exp_filename) as efile:
                        try:
                            exp_values = yaml.load(efile.read(), Loader=yaml.FullLoader)
                            if exp_values.get("parameter_type") != 'experiment':
                                raise KeyError
                        except KeyError:
                            print ("file '{0}' is not an experiment parameterizer file. (type: {1})" .format(
                                exp_filename,
                                exp_values.get("parameter_type"),
                            ))
                            raise RuntimeError
                    except FileNotFoundError as exc:
                            print("WARNING: {1}: no '{0}' experiment parameters file found.".format(
                                values['experiment'],
                                filename,
                            ))

                    if 'shg' in values and values['shg'] == None:
                        values.remove('shg')
                    if 'svg' in values and values['svg'] == None:
                        values.remove('svg')
                        
                self.clear()
                #print(exp_values)
                #print(values)
                self.update(exp_values)
                self.update(values)
            except KeyError:
                print ("file '{0}' not a {1} {4} parameterizer file. (type: {2}/{3})" .format(
                    filename,
                    self._instance_type,
                    values.get("parameter_type"),
                    values.get("instance_type"),
                    self._parameter_type,
                ))
                raise RuntimeError
        except FileNotFoundError as exc:
            print(f"no {filename} daiquiri file found.") # - please consider to use Daiquiri to enter your measurement parameters.")
            raise 
        return func(self, *args, **kwargs)        
    return wrapper


def check_current_sample (func):
    @functools.wraps(func)
    def wrapper (self, *args, **kwargs):
        if self.move_scan_mode == None or self._current_sample:
               return func(self, *args, **kwargs)        
        else:
            raise RuntimeError ('radiation damage protection: current_sample not defined. Nothing done ...')
    return wrapper



class ID26MeasurementHardware:
    
    def __init__(self, name, config):

        #bliss controller
        
        self._meas_hw_name = name
        self.config = config
        self.undulators = config.get('undulators',[]) 
        self.tapers = config.get('tapers',[])
        
        self.dcm = config.get('dcm')
        self.svg = config.get('svg')
        self.shg = config.get('shg')
        #self.stheta = config.get('stheta')
        self.xes_en = config.get('xes_en')
        self.hutch = config.get('hutch', None)
        self.dslit = config.get('dslit')
        self.counter_background = config.get('counter_background', None)
        #obsolete dcm self.eneund = self.dcm.energy_undulator_motor
        self.eneund = self.dcm.motors.energy_tracker
        
        #checks
        
        def _check (obj, klass):
            if not isinstance (obj, klass):
                raise ValueError (f"{obj.name} must be a {klass} instance.")
            
        _check(self.svg, Axis)
        _check(self.shg, Axis)
        _check(self.xes_en, Axis)
        #_check(self.stheta, Axis)
        for axis in self.undulators:
            _check(axis, Axis)
        for axis in self.tapers:
            _check(axis, Axis)

        if self.counter_background:
            _check(self.counter_background, BackgroundCalcCounterController)
        
        _check (self.eneund, Axis)
        
class ID26MeasurementBase (dict):
    def __init__(self, name, *args):
        self.use_daiquiri = False
        self.name = name
        self._position_manager = None
        self._instance_type = None
        self._init_meta_data_publishing ()


    def __info__(self):
        string = 'keys:\n'
        for k in sorted(self.keys()):
                v = self[k]
                string += f'- {k} : {v} \n'
        return string
    
    def _init_meta_data_publishing(self):
        if not self.name:
            return
        scan_meta_obj = get_user_scan_meta()
        scan_meta_obj.instrument.set(self, lambda _: {self.name: self.metadata()})


    @property
    def scan_info(self):
        return {'instrument':{
            'measurement':{
                'name':self.name,
                'type':self._instance_type,
            }}}
        
#    @property
#    def position_manager(self):
#        return self._position_manager

        
    @load_gui_param
    def _init_position_manager(self):
        try:
            self._position_manager = PositionManager(f'{self["experiment"]}_pm')
                                
        except Exception as e:
            print(e)
            print('Cannot initialise radiation protection - missing "experiment" key in your object')
            if self.use_daiquiri:
                print(f'Hint: {self.name}_{self._instance_type}["experiment"] = <experiment name>')
                print(f'      {self.name}_{self._instance_type}._init_position_manager()')
            log_debug(
                self,
                e,
            )
            raise e

    @load_gui_param
    def show_sample_patterns (self):
 
        shg = self.get('shg')
        svg = self.get('svg')
        stheta = self.get('stheta')
        shg = 0.1 if shg is None else shg
        svg = 0.1 if svg is None else svg
        stheta = 0.1 if stheta is None else stheta
        width = shg/numpy.sin(numpy.deg2rad(stheta))
        height = svg

        labels = {
            'x': self.get('sample_mot1'),
            'y': self.get('sample_mot2'),
        }
        
        self._position_manager.show(width=width, height=height, labels=labels)


        

class ID26Measurement (ID26MeasurementHardware, ID26MeasurementBase):

    def __init__ (self, name, measurement):

        if not isinstance (measurement , ID26MeasurementHardware):
            raise RuntimeError ("Please specify a true ID26MeasurementHardware object")
        
        ID26MeasurementHardware.__init__(self, measurement._meas_hw_name, measurement.config)
        ID26MeasurementBase.__init__(self, name)

        self._parameter_type = 'measurement'
        self._current_sample = None
        self._first_correction_index = [None, None]
        self._correction_points = None
        
        #TODO USE Settings here !!!
        # because possible problem: current values not retained if bliss restarted!


    def metadata(self):

        log_debug(
            self,
            "metadata retrieving started ..."
        )

        meta_dict = dict()

        meta_dict["@NX_class"] = "NXID26measurement"
        
        meta_dict.update  ({
            'current_sample' : self._current_sample,
#            'cycle_counts' : self._position_manager.get_cycle_counts(self.current_sample),
#            'positions' : self._position_manager.get_positions(self.current_sample),
            'move_scan_mode' : self.move_scan_mode,
            'first_correction_index' : None if None in self._first_correction_index else self._first_correction_index,  
            'first_index' : self.first_correction_index,
            'sample_mot1': self.get('sample_mot1'),
            'sample_mot2': self.get('sample_mot2'),
        })

        log_debug(
            self,
            "metadata done."
        )

        return meta_dict




    def olddcm_move_undulators(self, energy_target, wait=True):
        undulators = Group(*self.undulators)
        motion = ()
        for undu in self.undulators:
            if undu.state == 'READY':
                if (undu.name[0] == 'u' and undu.track() == True):
                    #print(f"{undu.name}")
                    pos = undu.position
                    calc_position = self.eneund.controller.undu.energy2undulator(undu,energy_target)
                    if abs(calc_position-pos) > self.eneund.controller.approx:
                        motion += (undu, calc_position)             
        undulators.move(*motion, wait=wait)
        
    #TODO why this move_undulators and not using energy trackers ??? again I ask, I forgot the answer....
    #if the energy tracking is correctly setup, the following line is enough - just there is a control on the id state - is this useful ???
    #self.eneund.move(energy_target)
    def move_undulators(self, energy_target, wait=True):
        undulators = Group(*self.undulators)
        motion = []
        for undu in self.undulators:
            if undu.state == 'READY':
                if (undu.name[0] == 'u' and undu.tracking.state == True):
                    #print(f"{undu.name}")
                    pos = undu.position
                    calc_position = undu.tracking.energy2tracker(energy_target)
                    if abs(calc_position-pos) > self.eneund.controller.approx:
                        motion += (undu, calc_position)             
        undulators.move(*motion, wait=wait)

        return motion

    def move_together (self, motion_dict, wait=True):
        mlist = []
        motion = []
        for axis, pos in motion_dict.items():
            if pos is not None:
                mlist.append(axis)
                motion.append(axis)
                motion.append(pos)

        if len(mlist)>0:
            motors = Group(*mlist)
            motors.move(*motion,wait=wait)

        return motion
        
    def auto_offset (self, itime = None):
        if self.counter_background is not None:
            if itime is None:
                itime = self.counter_background.background_setting.get('background_time', 4)
            self.counter_background.take_background(itime)

        
    '''
    radiation_damage controls
    used from user scripts directly, or for printing in ID26Xas and ID26StepXas, and Xes and Rixs classes
    '''

                    
    @property
    def current_sample(self):
        return self._current_sample
        
    @current_sample.setter
    def current_sample(self, name):
        if self._position_manager is None:
            self._init_position_manager()
        if self._position_manager.has_pattern(name):
            self._current_sample = name
            c = get_config()
            try:
                xmot = c.get(self['sample_mot1'])
                ymot = c.get(self['sample_mot2'])
            except KeyError:
                raise RuntimeError('sample_motors not defined. (sample_mot1 and/or sample_mot2 keys missing)')
            self._radiation_damage_preset = RadiationDamagePreset(xmot, ymot, self._position_manager, name)
            self._radiation_damage_motors = Group(self._radiation_damage_preset.xmot, self._radiation_damage_preset.ymot)
        else:
            print (f'{name} not a known sample for {self._position_manager._name} sample protection position manager. Nothing done ...')
            print ('know samples list:')
            print (self._position_manager.get_pattern_list())
            raise ValueError(f'{name} not a known sample for {self._position_manager._name} sample protection position manager.')

    @property
    def first_correction_index(self):
        return self._first_correction_index[0]
        
    @first_correction_index.setter
    @check_current_sample
    def first_correction_index(self, value):
        self._position_manager.set_index(self._current_sample, value)
        self._first_correction_index[0] = value

    @property
    def correction_points(self):
        return self._correction_points
        
    @correction_points.setter
    @check_current_sample
    def correction_points(self, value):
        self._correction_points = value

    @check_current_sample
    def goto_index(self, index):
        self._position_manager.set_index(self._current_sample, index)

    @check_current_sample
    def get_current_index(self):      
        return self._position_manager.get_index(self._current_sample)
        
    @property
    def move_scan_mode(self):
        value = self.get('move_scan_mode',[None])[0]
        if value == 'None':
            return None
        return value
        
    @move_scan_mode.setter
    def move_scan_mode(self, value):
        if value not in [None, 'None', 'point', 'full_scan', 'scan_segment']:
            raise ValueError("scan_mode should be in [None ,'point', 'full_scan', 'scan_segment']")
        self['move_scan_mode'] = [value]

    @property
    def position_manager(self):
        return self._position_manager
        
    @position_manager.setter
    def position_manager(self, obj):
        if isinstance(obj, PositionManager):
            self._position_manager = obj
        else:
            print ('obj must be a PositionManager instance. nothing done.')
            
    @check_current_sample
    def radiation_damage_protect_init (self):
        if self.move_scan_mode:
            i = self._position_manager.get_index(self._current_sample) 
            self._first_correction_index = [
                i,
                sum(self._position_manager._counts[self.current_sample])
            ]

    @check_current_sample
    def radiation_damage_protect_nopoints (self):
        if self.move_scan_mode:

            i0, c0 = self._first_correction_index
            c1 = sum(self._position_manager._counts[self.current_sample])
            
            self.correction_points = c1-c0
            

    @check_current_sample
    def radiation_damage_protect_scan (self, wait=True, protect_type='scan'):
        if self.move_scan_mode and protect_type in self.move_scan_mode:
            x, y = self._position_manager.get_next_position(self._current_sample)
            xmot, ymot = self._radiation_damage_preset.xmot, self._radiation_damage_preset.ymot
            log_debug (
                self,
                f"move ({xmot.name}, {ymot.name}) to ({x}, {y}) (counts={self._position_manager.get_cycle_counts(self.current_sample)})",
            )
            self._radiation_damage_motors.move(xmot, x, ymot, y, wait=wait)

    @check_current_sample
    def radiation_damage_protect_point (self, scanid):
        if self.move_scan_mode == 'point':
            scanid.acq_chain.add_preset(self._radiation_damage_preset)

    @check_current_sample
    def radiation_damage_protect_in_active_mg (self, modes = ['point', 'full_scan', 'scan_segment']):
        
        if self.move_scan_mode in modes:
            axes = [self._radiation_damage_preset.xmot, self._radiation_damage_preset.ymot]
            mot_counters = list(map (lambda x: SoftCounter(x, 'position', name=f"{x.name}_cnt", mode='SINGLE'), axes))
            idx_counters = [SoftCounter(self, "get_current_index", name="index_cnt", mode='SINGLE')]
            active_mg = get_active_mg()
            self._active_mg_management = {
                'mg': active_mg,
                'remove': [],
                'disable':[],
            }
            try:
                for cnt in mot_counters+idx_counters:
                    if cnt not in active_mg._available_counters:
                        active_mg.add(cnt)
                        self._active_mg_management ['remove'].append(cnt)
                    elif cnt.fullname not in active_mg.enabled:
                        active_mg.enable(cnt.fullname)
                        self._active_mg_management ['disable'].append(cnt)

            except Exception as e:
                log_error (
                    self,
                    'Cannot add radiation protection motors positions into active measurement group',
                )
                raise e

    def radiation_damage_protect_in_active_mg_reset (self):
        try:
            active_mg = self._active_mg_management.get('mg')
        except AttributeError: 
            return
        if active_mg:
            active_mg.remove(*self._active_mg_management['remove'])
            for cnt in self._active_mg_management['disable']:
                active_mg.disable(cnt)

        
            

    @check_current_sample
    def cccheck(self,acq_time = None, inc_energy = None, xes_main_max = None, first_index = None, last_index = None, nbpts = None, verbose=True):

        if acq_time is None:
            acq_time = self.get('acq_time')
        if isinstance(acq_time, list):                      #XES 
            acq_time = 1
            print (f'WARNING: acq_time was forced to 1 second. run <your object>.ccscan(acq_time=<your value>) to change it')
        if acq_time is None:
            acq_time = self.get('scan_time') #RIXS / XANES / EXAFS
        if acq_time is None:
            acq_time = 1   

        if inc_energy is None:
            inc_energy =  self.get('energy_excite_above')
            if inc_energy is None:
                print('inc_energy/energy_excite_above is not defined, the Correction Scan will not work.')                
                raise ValueError('incident energy is not defined, aborting')

        if xes_main_max is None:
            xes_main_max = self.get('xes_main_max')
        if xes_main_max is None:
            raise ValueError('xes_main_max not defined.')

        if first_index is None:
            first_index = self.first_correction_index
            
        if first_index is None:
            raise ValueError('First index not defined')

        if (last_index is None and nbpts is None): # all automatic from pm position
            nbpts = self.correction_points
        elif nbpts is None: # probably last index given: #TODO beware if more points than pattern spots!
            nbpts = last_index - first_index + 1

        if nbpts is None:
            raise ValueError('Number of points in the correction scan is not defined.')

        if verbose:
            print(f'number of points is ', nbpts)
            print('first index in the main scan', first_index)
       
        return acq_time, inc_energy, xes_main_max, first_index, nbpts

            
    def ccscan(self, acq_time = 1, inc_energy = None, xes_main_max = None, first_index = None, last_index = None, nbpts = None):
        acq_time, inc_energy, xes_main_max, first_index, nbpts = self.cccheck( acq_time, inc_energy, xes_main_max, first_index, last_index, nbpts, False)
    
         #move incident and emission energy
        self.move_together (
            {
                self.xes_en: xes_main_max,
                self.eneund: inc_energy,
            }
        )
    
        xmot = self._radiation_damage_preset.xmot
        ymot = self._radiation_damage_preset.ymot
        
        self.radiation_damage_protect_in_active_mg ()
        
        # this makes the cc RD independent of the main scan RD
        self._cc_radiation_damage = RadiationDamagePreset(xmot, ymot, self._position_manager, self._current_sample)
        
        # back to the starting index
        if first_index == -1: # if sample not run yet
            self.goto_index(len(self._radiation_damage_preset.pm._positions[self._current_sample])-1) # go to the last point of the pattern so that the next position is the first one
        else:
            self.goto_index(first_index)
            
        print(f'1st index in the correction scan {self.get_current_index()}')

        scan_info = dict()
        scan_info.update(self.scan_info)
        scan_info.update({
            'type':'cc',
            })
        correction_scan = loopscan(nbpts, acq_time, scan_info=scan_info, run = False)
        correction_scan.acq_chain.add_preset(self._cc_radiation_damage) # must be run on the cc RD not to lose the main scan indexing!
        
        with cleanup (self.radiation_damage_protect_in_active_mg_reset):
            correction_scan.run() # run the correction scan

        return correction_scan

       
    
class RadiationProtectionPatterns (ID26MeasurementBase):
    
#    def __init__(self, name, measurement):
    def __init__(self,*args):

        self._parameter_type = 'sample'

#        super().__init__(name, measurement)
        super().__init__(*args)

    def metadata(self):
        return

        log_debug(
            self,
            "metadata retrieving started ..."
        )

        meta_dict = dict()

        meta_dict["@NX_class"] = "NXID26measurementSample"

        try:
            meta_dict.update  ({
                'cycle_counts' : self._position_manager.get_cycle_counts(self.name),
                'positions' : self._position_manager.get_positions(self.name),
            })
        except:
            pass

        log_debug(
            self,
            "metadata done."
        )

        return meta_dict


    def create_pattern (self, verbose = False):
        if not self._position_manager:
            self._init_position_manager()
            self._patterns_lock = HashObjSetting(f"RadiationProtectionPatterns_lock:{self._position_manager._name}") # dict of sample motors tuple/list
#            self._patterns_lock = HashObjSetting(f"RadiationProtectionPatterns_lock:{self.name}") # dict of sample motors tuple/list
        (xmot,ymot) = (self.get('sample_mot1'), self.get('sample_mot2'))
        if self._patterns_lock.get(self.name) is None or self.get('overwrite_pattern'):
            self._patterns_lock [self.name] = (xmot,ymot)
        elif (xmot,ymot) != self._patterns_lock [self.name]:
            raise ValueError(f"The pattern {self.name} was already defined with different sample motors {self._patterns_lock[self.name]}\nRemove existing pattern first, or allow overwritting, or use a new name.")
            
        steps = self._calculate_steps()

        self._create_pattern (steps, verbose = verbose)
        if verbose:
            print(f" - sample motors: {self.get('sample_mot1')}, {self.get('sample_mot2')}")

    def _create_pattern (self, verbose = False):
        raise NotImplementedError
        
    def _calculate_steps(self):

        if 'step1' in self:
            step1 = self['step1']
        elif 'shg' in self:
            step1 = self['shg']/numpy.sin(numpy.deg2rad(self['stheta']))
        else:
            raise ValueError(f'Cannot create pattern {self.name}, missing slits gap definition.')
                
        if 'step2' in self:
            step2 = self['step2']
        elif 'svg' in self:
            step2 = self['svg']
        else:
            raise ValueError(f'Cannot create pattern {self.name}, missing slits gap definition.')

        return (step1,step2)

    def remove_pattern (self, name):
        self._position_manager.remove_pattern(name)
        self._patterns_lock.remove(name)
    
    def remove_all_patterns (self):
        self._position_manager.remove_all_patterns()
        self._patterns_lock.clear()
    
class Circle (RadiationProtectionPatterns):

#    def __init__(self, name, measurement):
    def __init__(self,*args):
        self._instance_type = 'circle'
#        super().__init__(name, measurement)
        super().__init__(*args)
        
    @load_gui_param
    def _create_pattern (self, steps, verbose = False):

        self._position_manager.add_circular_pattern (
            self.name,
            self['center_horizontal'],
            self['center_vertical'],
            steps[0],
            steps[1],
            self['diameter'],
            snake = self.get('snake'),
            centered = self.get('centered'),
            overwrite = self.get('overwrite_pattern')
        )
        if verbose:
            print(f'{self.name} sample')
            print(f" - circular pattern, center at ({self.get('center_horizontal')}, {self.get('center_vertical')}), diameter {self.get('diameter')} mm, stepsize {steps[0]} x {steps[1]} mm.") 
            print(f" - {len(self._position_manager.get_positions(self.name))} spots/positions.")  


class Rectangle (RadiationProtectionPatterns):

#    def __init__(self, name, measurement):
    def __init__(self,*args):
        self._instance_type = 'rectangle'
#        super().__init__(name, measurement)
        super().__init__(*args)
        
    @load_gui_param
    def _create_pattern (self, steps, verbose = False):

        if (self.get('center_horizontal') is not None) and (self.get('center_vertical') is not None):

            self._position_manager.add_rect_pattern(
                self.name,
                self.get('center_horizontal')-self.get('size_horizontal')/2,
                self.get('center_vertical')-self.get('size_vertical')/2,
                self.get('center_horizontal')+self.get('size_horizontal')/2,
                self.get('center_vertical')+self.get('size_vertical')/2,
                steps[0],
                steps[1],
                centered = self.get('centered'),
                snake = self.get('snake'),
                overwrite = self.get('overwrite_pattern'),
            )
            if verbose:
                print(f'{self.name} sample')
                print(f" - rectangular pattern. Center at ({self.get('center_horizontal')}, {self.get('center_vertical')}), size {self.get('size_horizontal')} x {self.get('size_vertical')} mm, stepsize {steps[0]} x {steps[1]} mm.")  
                print(f" - {len(self._position_manager.get_positions(self.name))} spots/positions.")

        elif (self.get('start_horizontal') is not None) and (self.get('start_vertical') is not None):

            self._position_manager.add_rect_pattern(
                self.name,
                self.get('start_horizontal'),
                self.get('start_vertical'),
                self.get('start_horizontal')+self.get('size_horizontal'),
                self.get('start_vertical')+self.get('size_vertical'),
                steps[0],
                steps[1],
                centered = self.get('centered'),
                snake = self.get('snake'),
                overwrite = self.get('overwrite_pattern'),

            )
            if verbose:
                print(f'{self.name} sample')
                print(f" - rectangular pattern. Start at ({self.get('start_horizontal')}, {self.get('start_vertical')}), size {self.get('size_horizontal')} x {self.get('size_vertical')} mm, stepsize {steps[0]} x {steps[1]} mm.")  
                print(f" - {len(self._position_manager.get_positions(self.name))} spots/positions.")

