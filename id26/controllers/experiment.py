"""

- controller:
  name: tst_experiment
  monochromator: $tst_dcm
  monox: $tst_monox
  mirror_layer_z: $tst_hex1z
  param_offset: $id26align_calc_param
  param_distance: $id26align_distance
  axes: 
  - 'undulators': [$tst_u35a, $tst_u35b, $tst_u35c, $tst_t35a, $tst_t35b] # undulators is compulsory - can be empty list []
  - 'optics slits gaps': [$tst_pvg, $tst_phg, $tst_ss1vg, $tst_ss1hg, $tst_hipsvg, $tst_hipshg, $tst_ss2vg, $tst_ss2hg]
  - 'optics slits offsets':  [$tst_ss1ho, $tst_hipsvo, $tst_hipsho, $tst_ss2vo, $tst_ss2ho]
  - 'optics TY t00 ppy': [$tst_TY, $tst_t00y, $tst_ppy, $tst_t00z]    # compulsory group name and axis name TY t00y and ppy
  - 'optics mono':  [$tst_TH2, $tst_CHI2, $tst_mono, $tst_monox]
  - 'optics mirror angles':  [ $tst_rot1z, $tst_rot2z, $tst_rot3y, $tst_rot4z, $tst_hex4tz]
  - 'optics stripes': [$tst_hex1z, $tst_hex2z, $tst_hex3ty, $tst_rot4y]
  - 'mirror centers': [$tst_hex2y, $tst_hex3z, $tst_hex4y] 
  - 'mirror_benders': [$tst_hfm2u, $tst_hfm2d, $tst_vfm3u, $tst_vfm3d]
  - 'optics attenuators': [$tst_patt1, $tst_patt3]
  - 'foils': [$tst_foil00, $tst_foil01, $tst_e0foil]
  - 'KB mirrors': [$tst_hfm2u, $tst_hfm2d, $tst_vfm3u, $tst_vfm3d]
  - 'TEXS motors': [$tst_FY, $tst_FZ, $tst_tsvo, $tst_tsho]
  - 'EH2_motors': [$tst_framety, $tst_e2boxz, $tst_e2vo, $tst_e2ho, $tst_foil02, $tst_e2boxy, $tst_sz]

"""

import functools
import math
import numpy 
import yaml

from silx import io
from tabulate import tabulate

#HashSetting with some method tools
#_setitem_, update, setup, save, and load

from bliss.common.axis import Axis
from bliss.common.motor_group import Group
from bliss.common.logtools import log_warning

from id26.bcolor import bcolor
from id26.scripts.beamline_settings import ID26Settings


#_EXTRA_KEYS = [
#    'mirror_layers', #used bu calcs - can be a property
#    'mono_crystal',  #used by calcs - can be a property too
#    'u35a_track','u35b_track', 'u35c_track', #not used
#    'u35a_harmonic','u35b_harmonic','u35c_harmonic', #u35b used by 1 calcs
#]


# this is already in id26_utils, how to load it? MCL : nothing to load from beamline scripts in there


def yesno (msg = 'Answer yes|no: ', defval = None):
    '''
    return boolean depending on yes/no answer
    '''
    _yes = ['Y', 'YES']
    _no = ['N', 'NO']

    if defval.upper() in _yes+_no :
        msg += f'[{defval}] '
    
    while True:
        ans = input(msg) or defval
        if ans and ans.upper() in _yes:
            return True
        elif ans and ans.upper() in _no:
            return False
        else:
            print('Please, answer yes or no: ')



def load_gui_param (func):
    @functools.wraps(func)
    def wrapper (self):
        '''
        directory = SCAN_SAVING.base_path + '/'
        directory += SCAN_SAVING.proposal_dirname + '/'
        directory += SCAN_SAVING.beamline + '/'
        directory += 'parameterizer/experiment/'
        file_name = SCAN_SAVING.collection_name +'.yml'
        '''
        directory = '/users/blissadm/local/id26.git/id26/data/parameterizer'
        file_name = 'exp2.yml'
        sep = '' if directory [-1] == '/' else '/'
        file_name = directory + sep + file_name

        with open(file_name) as file:
            try:
                values = yaml.load(file.read(), Loader=yaml.FullLoader)
                if values.get("parameter_type") != "experiment":
                    raise KeyError
                self.update(values)
            except KeyError:
                print ("file '{0}' not a valid experiment parameterizer file." .format(file_name))
                raise
        return func(self)        
    return wrapper


#class ID26Experiment(ID26Settings):
class ID26Experiment(dict):

    def __init__(self, name, config):

        self.dcm = config.get('monochromator')
        self.monox = config.get('monox')
        self._axes_dict = config.get('axes')
        self._axes_names_dict = dict()
        self._axes = list()
        discard = list()
        
        for g_name, axes in self._axes_dict.items():
            try:
                if False in map(lambda x:isinstance (x,Axis),axes):
                    raise Exception ("group contains object that is not an axis")

                self._axes_names_dict [g_name] = list(map(lambda x:x.name, axes))
                self._axes.extend (axes)
            except Exception as e:
                log_warning (
                    self,
                    "{0} : {1} - {0} motor group will be discarded.".format(g_name, e)
                    )
                discard.append(g_name)

        for g_name in discard:
            print ("discarding",g_name)
            self._axes_dict.pop(g_name)
                
        #super().__init__(name, defaults=self._build_default_parameters())
        super().__init__(self._build_default_parameters())

        self._offset_param = config.get('param_offset')
        self._distance   = config.get('param_distance')

        #_who_uses self._optics_slits = [PVG, PVO, PHG, PHO, HIPSVO, HIPSVG, HIPSHO, HIPSHG, SS1HO, SS1HG, SS2VG, SS2VO, SS2HG, SS2HO]
        #_who_uses self._experiment_slits = [SVG, SHG]

        #self.update_slits()
        
    #def __dir__(self):
    #    return 'setup', 'save', 'load', 'remove', 'clear', 'update_from_current', 'update_from_h5', 'set_slits', 'move_slits'

    def _build_default_parameters(self):
        default_params = {}
        for one_name in self._axes:
            default_params[one_name.name] = None
        return default_params

    @property
    def mirror_layer (self):
        
        hex1z_pos = self.mirror_layer_z.position
    
        if hex1z_pos < -18 or hex1z_pos >= 18:
            raise ValueError('{0} position {1} is wrong.'.format (self.mirror_layer_z.name, hex1z_pos))
        elif hex1z_pos < -6:
            mirror_layer = 'Pd'
        elif hex1z_pos < 6:
            mirror_layer = 'Si'
        elif hex1z_pos < 18:
            mirror_layer = 'Pt'
            
        return mirror_layer

    
    @property
    def mono_crystal (self):
        return self.dcm._xtals.xtal_sel
    
    def update_from_current(self):
        '''
        Function that sets starting parameter to the current motor positions
        position kept eventually to move them afterwards, or save them - see below *move* methods - also calc_move would update them
        '''

        #PositionInstance
        #posInst = PositionInstance('experiment', self._axes)
        #posInst.update()
        
        for ax in self._axes:
            self[ax.name] = ax.position

        #TODO/#QUESTION also keeping other parameters, not only axis positions: question: what for apart from calc ???
        #self['mirror_layers'] = self.mirror_layer           # used by calcs
        #self['mono_crystal'] = self.mono_crystal            # used by calcs

        #for id in self._axes_dict['undulators']:
            #self[f'{id.name}_track'] = id.track()      #QUESTION - only for saving ???? did not found any trace of saving this. if so could setup metadata ????
            #self[f'{id.name}_harmonic'] = id.harmonic()# used by calcs
    
    def update_from_h5(self, h5_fullname, scanno=None, verbose = False):
        try: # if it is a specfile
            sf = io.open(h5_fullname)
            print(sf)
            
            #sf_label = sf.keys()[-1] if scanno is None else '/'+str(scanno)+'.1'
            sf_dataset = sf[sf.keys()[-1] if scanno is None else '/'+str(scanno)+'.1']
            
            for axis in self._axes:
                name = axis.name
                if name in sf_dataset['instrument']['positioners'].keys():
                    positioner = sf_dataset['instrument']['positioners'][name][()]
                    if numpy.size(positioner)>1:
                        self[name] = positioner[0]
                    else:
                        self[name] = positioner
                    if verbose:
                        print (f'{name} = {self[name]}')
                        
        #TODO: special methods for _EXTRA_KEYS to be writen once metadata is defined
        except OSError: # if it is not a h5 file or wrong path
            print(h5_fullname + ' is not a valid HDF5 filename. Update not done.')
            
            
    def move(self, axis):
        #PositionInstance: posInst.move (axis)
        axis.move(self[axis.name])

        
    def move_with_confirm(self):
        
        for ax in self._axes:
            if (ax.position - self[ax.name]) != 0:
                msg = f'Do you want to move {ax.name} motor by {(self[ax.name] - ax.position):.4f} from {ax.position:.4f} to {self[ax.name]:.4f}?'
                if yesno(msg, defval = 'no'):
                    ax.move(self[ax.name])
            else:
                print(f'Motor position for {ax.name} has not changed. It is already in position at {ax.position}.') 
                
    def move_show(self):

        for ax in self._axes:
            if (ax.position - self[ax.name]) != 0:
                print(f'{bcolor.RED}Motor {ax.name} will move by {(self[ax.name] - ax.position):.4f} from {ax.position:.4f} to {self[ax.name]:.4f}{bcolor.END}')
            else:
                print(f'Motor {ax.name} already in position. will not move') 
                
        
    def smart_move(self, confirm = True):

        for ax_group_name, axes in self._axes_dict.items():
            if confirm:
                print("\n__________________________________________________\n")
                msg = f'Next group \'{ax_group_name}\' contains motors {self._axes_names_dict[ax_group_name]}. Do you want to move any of those?'
                next_group = yesno(msg, defval = 'no')
            else:
                next_group = True
                
            if next_group: 
                #PositionInstance: posInst.move(*axes) - skip motion construction
                motion = ()
                mot_group = Group(*axes)
                for ax in axes:
                    motion += (ax, self[ax.name])
                msg = 'Ready to move?'
                if confirm:
                    print("\n__________________________________________________\n")
                    do_move = yesno(msg, defval = 'no')
                else:
                    do_move = True
                    
                if do_move:
                    if ax_group_name == 'optics TY t00 ppy':
                        print('ATTENTION: risk of pulling on bellows in the t00y section. Moving first to straighten all.')

                        t00y = self._axes_dict [ax_group_name][self._axes_names_dict [ax_group_name].index('t00y')]
                        TY = self._axes_dict [ax_group_name][self._axes_names_dict [ax_group_name].index('TY')]
                        ppy = self._axes_dict [ax_group_name][self._axes_names_dict [ax_group_name].index('ppy')]
                                                
                        t00y_current_val = t00y.position

                        t00y_new_val = self['t00y'] if self['t00y'] else t00y.position
                        TY_new_val = self['TY'] if self['TY'] else TY.position
                        ppy_new_val = self['ppy'] if self['ppy'] else ppy.position

                        motion_1 = (TY, t00y_current_val, ppy, t00y_current_val) # bring both TY and ppy to the current value of t00y
                        motion_2 = (TY, t00y_new_val, ppy, t00y_new_val, t00y, t00y_new_val) # bring all motors to the desired t00y
                        motion_3 = (TY, TY_new_val)
                        motion_4 = (ppy, ppy_new_val)

                        mot_group.move(*motion_1)        
                        mot_group.move(*motion_2)        
                        mot_group.move(*motion_3)        
                        mot_group.move(*motion_4)        
                        
                    elif len(motion) == 0:
                        print("No motors to move")
                    else:
                        print("Moving motors", list(map(lambda x: x.name if isinstance(x, Axis) else x, motion)))
                        mot_group.move(*motion)
                        #PositionInstance: posInst.move(*axes)
                        

    def save_move_file(self,fname):
        #sync(*_AXES_NAMES) #MCL: that makes nothing so of course it is very fast....
        
        with open(fname, 'w+') as fo:
            for ax in self._axes:
                print(ax.name)
                fo.write(f'umv({ax.name}, {self[ax.name]:g})  # from current {ax.position:g} position\n')


                
    def _deflection (self, angle, d1, d2):
        return math.tan(2 * self[angle] / 1000) * (self._distance[d1] - self._distance[d2])

    @load_gui_param
    def calc_move(self, verbose = False):
        ''' 
     Updates the ID26Experiment object with motor positions to move to according to extra keys
     if an offset must be applied, it is loaded from the
     id26align_offset_param object
     optimum taper values
     Returns the updated object (is that useful?)
     id26align_offset_param are mostly offsets, see beacon miscellaneous/align_vars.yml
     '''

        #TODO/#QUESTION this is redundant - would be enough to move eneund and control id tracking on/off
        #self['eneund'] = self['energy']
        #or maybe they want it like that because tracking is only for measurement.
        for id in self._axes_dict['undulators']:
            self[id.name] = id.tracking.energy2tracker (self['energy'])

        #QUESTION : self['*_angle'] : where are those initialised ???? not in the key list

        self['phg'] = min([math.sin(self["hdm1_angle"]/1000)*620*0.9, 1.5])  
        self['rot1z'] = self._offset_param['rot1z'] + self['hdm1_angle']
        self['ss1ho'] = - self._offset_param['ss1ho'] - self._deflection('rot1z', 'ss1', 'hex1')

            # TODO: takes only 1 harmonic, what if the user asks for different ones on each undu?
        
        thickness, self['patt1'], self['patt3'] = self._calc_patts()
        
        print('diamond thickness to use: {}'.format(thickness))

        self['monox'] =  self.monox.calc_ene(self["energy"], verbose = verbose)

        try:
            #layer = self['mirror_layers'])
            layer = self.mirror_layer
            self['hex1z'] = self._offset_param['hex1z'] + self._offset_param ['hex1z_mlayer'][layer]
            if self["hfm2_angle"] != 'out': 
                self['hex2z'] = self._offset_param['hex2z'] + self._offset_param ['hex2z_mlayer'][layer]
        except:
            print('undefined mirror layer')
            self['hex1z'] = self['hex2z'] = None

        #mono_type = self["mono_crystal"]
        mono_type = self.mono_crystal
        
        if mono_type == "Si111" or mono_type == "Si333":
            
            self['TY'] = self._offset_param['TY'] + self._deflection ('rot1z', 'mono', 'hex1') - 21
            self['TH2']= self._offset_param['TH2_111'] 
            self['CHI2']= self._offset_param['CHI2_111']
            
        elif mono_type == 'Si311':
            
            self['TY'] = self._offset_param['TY'] + self._deflection ('rot1z', 'mono', 'hex1') + 21
            self['TH2']= self._offset_param['TH2_311'] 
            self['CHI2']= self._offset_param['CHI2_311']
            
        else:
            
            print('Mono type unknown')
            self['TY'] = self['TH2'] = self['CHI2'] = None

            
        self['t00y']  = self._offset_param['t00y'] + self._deflection ('rot1z', "t00", "hex1")
        self['ppy']   = self._offset_param['ppy'] + self._deflection ('hdm1_angle', "pp", "hex1")
        self['hex2y'] = self._offset_param['hex2y'] + self._deflection ("rot1z", "hex1", "hex2")
        
        self['rot2z'] = self._offset_param['rot2z'] + 2 * self["rot1z"] - self['hfm2_angle']

        
        # deflection towards hall until hex2  , towards ring after

        try:
            self['hex3ty'] = self._offset_param['hex3ty'] + self._offset_param ['hex3ty_mlayer'][layer] + self._deflection ('rot1z', 'hex3', 'hex1')
            if (self['rot2z'] != "out" ):
                self['hex3ty'] -= self._deflection('rot2z', 'hex3', 'hex2') 
        except:
            self['hex3ty'] = None
        
            
        self['rot3y'] = self._offset_param['rot3y'] + self["vfm3_angle"]
        self['rot4z'] = self._offset_param['rot4z'] + 2 * (self['rot1z'] - self["rot2z"])


        self['hex4y'] = - self._offset_param['hex2y'] - self._deflection ("rot1z", 'hex4', "hex1")  
        if (self["rot2z"] != "out" ):
            self['hex4y'] += self._deflection ("rot2z", 'hex4', "hex2") 


        self['FY'] = self._offset_param['FY'] + self._deflection ("rot1z", 'texs', "hex1")  
        if (self["rot2z"] != "out" ):
            self['FY'] -= self._deflection ("rot2z", 'texs', "hex2") 

            
        self['FZ'] = self._offset_param['FZ'] + self._deflection ("rot3y", 'texs', "hex3")

        
        self['framety'] = self._offset_param['framety'] + self._deflection ("rot1z", 'spec', "hex1")
        if (self["rot2z"] != "out" ):
            self['framety'] -= self._deflection ("rot2z", 'spec', "hex2")

      
        self['e2boxy'] = self._offset_param['e2boxy']

        self['e2boxz'] = self._offset_param['e2boxy'] + self._deflection ('rot3y', 'e2box', 'hex3') 

        
        if verbose:
            stdout = list()
            for k, v in self.get_all().items():
                stdout.append([k, v])
            print(tabulate (stdout))
        
        return self






    
    def _calc_patts(self):
        '''
        define optimum diamond thickness for higher harmonics (optimized for u35 undulators)
        the thickness is in um! Patt1 and patt3 will be moved accordingly
        '''

        try:
            id = self._axes_dict ['undulators'][self._axes_names_dict ['undulators'].index('u35b')]
        except ValueError:
            return [None, None, None]
        
        harmonic = id.harmonic()

        #harmonic = self['u35b_harmonic']
        energy_val = self['energy']
        
        if harmonic == 1: # no patt on the fundamental:
            patt_thickness = 0
            patt1_pos = -15
            patt3_pos = -15
      
        else:   #on higher undulator harmonics

            if harmonic > 3:
                patt_thickness = 3500
            else:
                def doit (energy_val):
                    thickness = [3500, 2600, 350, 1750, 1450, 850, 600]
                    energies  = [19.5, 18,   16.5, 15,  13.5, 12]
            
                    for i, e in enumerate(energies):
                        if energy_val > e:
                            return thickness[i]

                    return thickness [-1]
                
                pos = {
                    350:  (-15,-15),
                    600:  (-43,-15),
                    850:  (-15,-43),
                    1450: (-43,-43),
                    1750: (-15,-29),
                    2600: (-29,-43),
                    3500: (-29,-29),
                }
            
                patt_thickness = doit(energy_val)
                patt1_pos = pos [patt_thickness[0]]
                patt3_pos = pos [patt_thickness[1]]

        return [patt_thickness, patt1_pos, patt3_pos]










            


####################### _who_uses

class _WHO_USES:

    '''
    methods that are never called
    '''
    
    def move_slits(self):
        key_error = False
        #ss1vg.move(self['ss1vg'])
        SS1UP.move(1)
        SS1DOWN.move(1)
        for one_axis in self._optics_slits:
            try:
                one_axis.move(self[one_axis.name])
            except KeyError:
                print('ID26Experiment: {:s} key not defined. Nothing moved.'.format(one_axis.name))
                key_error = True
        if key_error is True:
            print('If some key not defined, run ID26Experiment.update_slits() or run frontalign.')

    def update_slits(self, *my_axes):
        if not my_axes:
            my_axes = self._optics_slits
            
        for my_axis in my_axes:
            self[my_axis.name] = my_axis.position

            
