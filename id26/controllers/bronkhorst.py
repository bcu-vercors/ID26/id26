import tabulate

from bliss.controllers.bronkhorst.bronkhorst import Bronkhorst,  BronkhorstNode

class BronkhorstWago(Bronkhorst):
    def __init__(self, name, config):
        Bronkhorst.__init__(self, name, config)

    def __info__(self):
        line1 = [""]
        line2 = ["Setpoint (%)"]
        line3 = ["Measure (%)"]
        line4 = ["Flow (ml/min)"]
        line5 = ["Valve"]
        for channel, node in self._nodes.items():
            if node._enabled.get():
                line1.append(node._fullname)
                line2.append(f"{node.setpoint:.2f}")
                line3.append(f"{node.measure:.2f}")
                line4.append(f"{node.flow:.2f}")
                if node.valve is None:
                    line5.append("None")
                else:
                    line5.append(node.valve._state())
        mystr = tabulate.tabulate([line1, line2, line3, line4, line5], tablefmt="plain")
        mystr += "\n"
        return mystr

    def _get_bronkhorst_node(self, node_config, controller, index):
        node = BronkhorstWagoNode(node_config, controller, index)
        return node

    def _get_meta_data(self):
        meta_data = {}
        for chan, node in self._nodes.items():
            if node._enabled.get():
                if node.valve is None or node.valve._state() == "OPEN":
                    meta_data[node._fullname] = node._get_meta_data()
        return meta_data

    def close_all(self):
        Bronkhorst.close_all(self)
        for channel, node in self._nodes.items():
            if node.valve is not None:
                node.valve.close()

    def valves_open_all(self):
        for channel, node in self._nodes.items():
            if node.valve is not None and node._enabled:
                node.valve.open()

    def valves_close_all(self):
        for channel, node in self._nodes.items():
            if node.valve is not None and node._enabled:
                node.valve.close()


class BronkhorstWagoNode(BronkhorstNode):
    def __init__(self, config, controller, index):

        BronkhorstNode.__init__(self, config, controller=controller, index=index)

        self._wago = config.get("wago", None)
        self._wago_ch = config.get("wago_channel", None)

        self.valve = None
        if self._wago is not None:
            self.valve = BronkhorstWagoValve(self._wago, self._wago_ch)

    def __info__(self):
        mystr = BronkhorstNode.__info__(self)
        if self.valve is not None:
            mystr += f"Valve   : {self.valve.__info__()}\n"
        return mystr


class BronkhorstWagoValve:
    def __init__(self, wago, channel):
        self._wago = wago
        self._channel = channel

    def __info__(self):
        return self._state()

    def _state(self):
        state = self._wago.get(self._channel)
        if state == 1:
            return "OPEN"
        else:
            return "CLOSED"

    def open(self):
        self._wago.set(self._channel, 1)

    def close(self):
        self._wago.set(self._channel, 0)
