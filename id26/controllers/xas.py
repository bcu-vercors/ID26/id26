import math
import functools
import yaml

from bliss.common.cleanup import cleanup, axis as cleanup_axis

from bliss.common.scans.step_by_step import ascan

from id26.scripts.fscan import fscan, FSCAN_PAR
from id26.controllers.measurement import ID26Measurement, load_gui_param

'''
undulators tracked are listed from measurement.yml object (ID26MeasurementHardware)
                              not from dcm.tracking state - dcm.tracking initial state is restored after each measurement.

add xas_scan['undu_tracking']=True to activate exafs mode to your object instantiated from XAS  (or ID26StepXAS for ascan)
'''
class ID26XAS(ID26Measurement):

    def __init__(self, name, measurement):

        super().__init__(name, measurement)
        
        self._instance_type = 'xas'
        self._init_meta_data_publishing () 

    
    @load_gui_param
    def check(self, scan_time = None, number_of_scans = None, xes_en = None, verbose = True):

        def _msg (key = None):
            if self.use_daiquiri:
                msg = "Please use Daiquiri to enter your measurement parameters"
            else:
                msg = "Please, add the ['{0}'] dictionary item.".format(key)
            return msg
                
        #make sure all parameters for scan are defined and acceptable:
        
        if self.get('start_energy') is None:
            raise ValueError('start energy not defined. {0}'.format(_msg('start_energy')))
        if self.get('end_energy') is None:
            raise ValueError('end energy not defined. {0}'.format(_msg('end_energy')))
        if self.get('energy_step') is None:
            raise ValueError('energy step not defined. {0}'.format(_msg('energy_step')))
        
        if self.get('undu_tracking') is None:
            self['undu_tracking']=False

        if not self['undu_tracking'] and not self.get('energy_edge'):
            print('Warning: energy_edge is not defined, no undulators tracking either')
            
        if scan_time is None:
            scan_time = self.get('scan_time')
            
        if scan_time is None:
            raise ValueError('Scan time not defined')

        scan_time = abs(scan_time)
        
        if (scan_time == 0):
            raise RuntimeError(
                "Scan time cannot be 0"
        )    

        if number_of_scans is None:
            number_of_scans = max(self.get('number_scans',1) ,1)
            
        if xes_en is None:
            xes_en = self.get('xes_en')
        
        if self.move_scan_mode is not None:
            self.move_scan_mode = 'full_scan' 
            if self.current_sample == None:
                raise ValueError ('current_sample not defined. Cannot apply radiation damage macros.')

        if verbose:
            print ('Will do {0} loop(s)'.format(number_of_scans))
            print ('Start Energy:',self['start_energy'])
            print ('End Energy:',self['end_energy'])
            print ('Energy Step:',self['energy_step'])
            print ('Scan time:', scan_time)
            print ('Emission Energy:', xes_en)
            print ('shg:',self.get('shg'))
            print ('svg:', self.get('svg'))
            print ('radiation protection:', self.move_scan_mode)
            print ('undulators {0} follow monochromator'.format('do' if self.get('undu_tracking') else 'do not'))
            if not self.get('undu_tracking'):
                print ('Energy Edge:', self.get('energy_edge'))
            
        return scan_time, number_of_scans, xes_en
            
    def scan(self, scan_time = None, number_of_scans = None, xes_en = None):

        scan_time, number_of_scans, xes_en = self.check (scan_time, number_of_scans, xes_en, False)

        self.move_together (
            {
                self.svg: self.get('svg'),
                self.shg: self.get('shg'),
                self.xes_en: xes_en,
            }
        )
        
        scan_data = []

        self.radiation_damage_protect_init ()
        self.radiation_damage_protect_scan ()
        
        self.hook_pre_scanloop()

        with cleanup (
                #self.dcm.energy_motor,
                #*self.undulators,
                #*self.tapers,
                self.hook_post_scanloop,
                self.radiation_damage_protect_nopoints,
                #restore_list = (cleanup_axis.POS,),
                verbose=True,
        ):
            for i in range(number_of_scans):
                scan_data.append(self._scan (self['start_energy'], self['end_energy'], self['energy_step'], scan_time))

        return scan_data
    
    def _scan (self, start_energy, end_energy, energy_step, scan_time):
        raise NotImplementedError ("core scan not implemented") 
        
    def hook_pre_scanloop(self):
        self.radiation_damage_protect_in_active_mg ()
   
    def hook_post_scanloop(self):
        self.radiation_damage_protect_in_active_mg_reset()

        
class ID26ContinuousXAS(ID26XAS):


    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        self._scan_technique =  {
            "technique": {
                "NX_class":"NXnote",
                "abbreviation": ["XAS"],
                "name":["X-ray absorption spectroscopy"],
            }
        }
        self._dataset_technique = ["XAS"]

    def _scan (self, start_energy, end_energy, energy_step, scan_time):
        self.hook_pre_fscan()
        my_fscan = fscan (start_energy, end_energy, scan_time, energy_step, scan_info=self.scan_info, scan_technique=self._scan_technique)
        my_fscan.scan_saving.dataset.add_techniques(*self._dataset_technique)
        self.hook_post_fscan()

        return my_fscan
       
            
    def hook_pre_fscan(self):
        pass
    
    def hook_post_fscan(self):
        pass

    def hook_pre_scanloop(self):
        super().hook_pre_scanloop()
        self._undu_tracking = (FSCAN_PAR['id_linked'], FSCAN_PAR['id_sync'])
        if not self['undu_tracking'] and self.get('energy_edge') is not None:
            self.move_undulators(self['energy_edge'])
        FSCAN_PAR['id_linked'] = self['undu_tracking']
        FSCAN_PAR['id_sync'] = self['undu_tracking']

    def hook_post_scanloop(self):
        super().hook_post_scanloop()
        FSCAN_PAR['id_linked'], FSCAN_PAR['id_sync'] = self._undu_tracking
        


class XAS(ID26ContinuousXAS):
    pass

class Xanes(ID26ContinuousXAS):
    '''
    X-ray Absorption Near-Edge Spectroscopy 
    During scan, the undulator gap remains fixed at the energy chosen in energy_edge
    (a good value is to set it at 2/3 of the scan range). 
    '''

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._instance_type = 'xanes'
        self['undu_tracking']=False
        
        
class Exafs(ID26ContinuousXAS):
    '''
    Extended X-ray Absorption Fine Structure Spectroscopy 
    During scan, the undulator gap changes to maximize the incoming beam flux. 
    '''
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._instance_type = 'exafs'
        self['undu_tracking']=True
        
    def hook_pre_scanloop(self):
        super().hook_pre_scanloop()
        self.auto_offset ()
        
    def hook_pre_fscan(self):
        #TODO ask BD: before each scan or each bunch of scans when they are >1
        #self.auto_offset ()
        #TODO added 220929 this is required for all scans that move the undulators (Xanes too etc...)
        for t35 in self.tapers:
            if self.get(t35.name) is not None:
                t35.move(self[t35.name])    
                       

        
                        
class ID26StepXAS(ID26XAS):

    
    def __init__(self, *args):
        super().__init__(*args)

        self.energy_motor = self.dcm.motors['energy']
        if self.get('undu_tracking') is None:
            self['undu_tracking'] = False

        self._scan_technique =  {
            "technique": {
                "NX_class":"NXnote",
                "abbreviation": ["XAS"],
                "name":["X-ray absorption spectroscopy"],
            }
        }
        self._dataset_technique = ["XAS"]
               
    def _scan (self, start_energy, end_energy, energy_step, scan_time):
            
        no_intervals = math.ceil((end_energy-start_energy)/energy_step)


        scan_info={}
        scan_info.update(self._scan_technique)
        scan_info.update(self.scan_info)
        my_xas_scan = ascan(self.energy_motor, start_energy, end_energy ,int(no_intervals),scan_time, scan_info=scan_info, run = False)
        my_xas_scan.scan_saving.dataset.add_techniques(*self._dataset_technique)
        
        self.radiation_damage_protect_init ()
        self.radiation_damage_protect_point (my_xas_scan)

        my_xas_scan.run() 
        
        self.radiation_damage_protect_nopoints ()
        #if self.move_scan_mode == 'point':
        #    self.correction_points = int(no_intervals)+1
            
        return my_xas_scan
           
    def hook_pre_scanloop (self):
        self._undu_tracking = list(map(lambda x:x.tracking.state, self.undulators))
        self.radiation_damage_protect_in_active_mg (['point'])
                
        if self.get('undu_tracking'):
            for x in self.undulators:
                x.tracking.on()
        else:
            energy_edge = self.get('energy_edge')
            if energy_edge is not None:
                self.eneund.move(energy_edge)
                for x in self.undulators:
                    x.tracking.off()
            else:
                print('Warning: energy_edge is not defined, and undulators tracking is off')

                
    def hook_post_scanloop(self):
        
        for n,x in enumerate(self.undulators):
            if self._undu_tracking[n]:
                x.tracking.on()
            else:
                x.tracking.off()

        self.radiation_damage_protect_in_active_mg_reset()

            

class StepXanes(ID26StepXAS):

    '''
    X-ray Absorption Near-Edge Spectroscopy 
    During scan, the undulator gap remains fixed at the energy chosen in energy_edge
    (a good value is to set it at 2/3 of the scan range). 
    '''
    def __init__(self, *args):
        super().__init__(*args)
        self['undu_tracking'] = False

class StepExafs(ID26StepXAS):
    '''
    EXAFS
    During scan, the undulators follow monochromator according to calibration
    '''

    def __init__(self, *args):
        super().__init__(*args)
        self['undu_tracking'] = True




