from bliss.config import settings

from bliss.common.scans import ascan
from bliss.controllers.counter import CalcCounterController
from bliss.scanning.acquisition.calc import CalcCounterAcquisitionSlave
#from bliss.common.standard import SoftAxis, SoftCounter
from bliss.common.counter import SoftCounter
from bliss.common.soft_axis import SoftAxis

from id26.scripts._det_dt_correction_fit import det_dt_calib_fit  as _det_dt_calib_fit 

'''
- package: id26.controllers.calccnt_dt_correction
  class: DeadTimeCorrectionCalcCounterController
  name: dt_corr

  monitor: $offset.counters.I02_top                #calibration monitor
  monitor_min_counts: 10000

  attenuator: $att
  min_attenuation_factor: 0
  max_attenuation_factor: 63

  detector_max_counts: 50000000     #saturation detection depends on detector: 10^7 for apd in uniform fill, 5x10^5 for apd in 16 bunch and 4 bunch and also ketek

  inputs:
    - counter: $p201_B.counters.apd
      tags: raw

    - counter: $p201_A.counters.sec
      tags: timer

  outputs:
    - name: det_c
'''

class DeadTimeCorrectionCalcCounterController(CalcCounterController):

    def __init__(self, name, config):

        self.mon = config.get ("monitor")
        self.det_max_counts = config.get ("detector_max_counts")
        self.mon_min_counts = config.get ("monitor_min_counts")
        self.min_filter =  config.get ("min_attenuation_factor")
        self.max_filter = config.get ("max_attenuation_factor")
        self.attenuator_object = config.get ("attenuator")

        CalcCounterController.__init__(self, name, config)

        self._integration_time = None
        
        dtcorr_setting_name = f"dtcorr_{self.name}"
        dtcorr_setting_default = {
            'tau':None,
            'c':None,
        }
        self.dtcorr_setting = settings.HashSetting(
            dtcorr_setting_name, default_values=dtcorr_setting_default
        )

    def get_acquisition_object(
        self, acq_params, ctrl_params, parent_acq_params, acq_devices
    ):
        int_time = acq_params.get("count_time", None)
        if int_time is not None:
            self._integration_time = int_time
        return CalcCounterAcquisitionSlave(
            self, acq_devices, acq_params, ctrl_params=ctrl_params
        )

    def get_default_chain_parameters(self, scan_params, acq_params):
        int_time = scan_params.get("count_time", None)
        if int_time is not None:
            self._integration_time = int_time
        return acq_params

    def get_input_counter_from_tag(self, tag):
        for cnt in self.inputs:
            if self.tags[cnt.name] == tag:
                return cnt

        return None

    def __info__(self):
        msg = f"Raw counter       : {self.get_input_counter_from_tag('raw').name}\n"
        msg+= f"Corrected counter : {self.outputs[0].name}\n"
        msg+= f"Tau               : {self.dtcorr_setting['tau']}\n"

        return (msg)
    
    def dt_correction_calib (self, min_filter, max_filter, itime=1):
   
        min_filter = max (min_filter,self.min_filter)
        max_filter = min (max_filter,self.max_filter)
    
        attm = SoftAxis ('attsoftm', self.attenuator_object, position='thickness', move='thickness', export_to_session = False)
        attc = SoftCounter (self.attenuator_object, 'factor')

        thick_begin = self.attenuator_object.unit*min_filter
        thick_end = self.attenuator_object.unit*max_filter

        det_raw = self.get_input_counter_from_tag('raw')
        timer = self.get_input_counter_from_tag('timer')
        
        counters = [attc, det_raw, timer, self.mon]
        
        sca = ascan (attm, thick_begin, thick_end, max_filter-min_filter, itime, *counters, sleep_time=0.1)
                
        self.attenuator_object.factor(min_filter)

        scan_d = sca.streams

        det_dat = scan_d [det_raw.name][:]
        mon_dat = scan_d [self.mon.name][:]
        time_dat = scan_d [timer.name][:]

        if (det_dat > self.det_max_counts*itime).any():
            print ("| DET. DEAD TIME CORRECTION -- WARNING -- SATURATION.    |")
            print ("|   Please try to change the energy of the spectrometer  |")
            return 
    
        fit_tau, fit_c = _det_dt_calib_fit (det_dat, mon_dat, time_dat)

        self.dtcorr_setting['tau'] = fit_tau
        self.dtcorr_setting['c'] = fit_c

        return sca

    
    def calc_function(self, input_dict):

        raw_val = input_dict['raw']
        time_val = input_dict['timer']
        tau = self.dtcorr_setting['tau']
        if tau:
            try:
                _corrected_val = ( (raw_val/time_val) / (1 - ((raw_val/time_val) * tau) ) ) * time_val
            except ZeroDivisionError:
                _corrected_val = 0
        else:
            _corrected_val = raw_val
            
        return {self.tags[self.outputs[0].name]:_corrected_val}

        
