"""
ID26Sxy class, CalcController to 

controller:
- class: ID26Sxy
  package: id26.controllers.sxy
  name: sxy
  axes:
  - name: sxx
    tags: sxx
  - name: syy
    tags: syy
  - name: sth # not compulsory for backward compatibility with spec bhaving
    tags: sth # not compulsory for backward compatibility with spec bhv

  - name: $sx 
    tags: real x
  - name: $sy
    tags: real y
  - name: $stheta
    tags: real theta
"""

import numpy

from bliss.physics.units import ur
from bliss.controllers.motor import CalcController
from id26.scripts.beamline_settings import ID26Settings

class ID26Sxy(CalcController):
    
    def __init__(self, *args, **kwargs):

        CalcController.__init__(self, *args, **kwargs)

        config = self.config.config_dict #ou args[1]
        
    def __info__(self):

        info_str='{0}: {1} - offset {2}\n'.format(
            self._tagged['theta'][0].name,
            self._tagged['theta'][0].position,
            self._tagged['theta'][0].offset,            
        )
        info_str+='{0}: {1} - {2}: {3}\n'.format(
            self._tagged['sxx'][0].name,self._tagged['sxx'][0].position,
            self._tagged['x'][0].name,self._tagged['x'][0].position,
        )
        info_str+='{0}: {1} - {2}: {3}\n\n'.format(
            self._tagged['syy'][0].name,self._tagged['syy'][0].position,
            self._tagged['y'][0].name,self._tagged['y'][0].position,
        )

        return info_str

    def calc_from_real(self, positions_dict):

        #print ('from2',positions_dict)
        
        calc_dict = dict()

        stheta = positions_dict["theta"]

        costh = numpy.cos((stheta)*ur.deg).magnitude
        sinth = numpy.sin((stheta)*ur.deg).magnitude
        
        calc_dict.update(
            {
                    "sxx":  costh * positions_dict["x"] + sinth * positions_dict["y"],
                    "syy": -sinth * positions_dict["x"] + costh * positions_dict["y"],
            }
        )

        if "sth" in self._tagged:
            calc_dict.update(
                {
                    "sth": stheta
                }
            )

        #print ('ret2',calc_dict)
        
        return calc_dict

    def calc_to_real(self, positions_dict):

        #print ('to2',positions_dict)
        #positions_dict can be an array - or - a scalar ...

        real_dict = dict()
        
        if 'sth' in positions_dict:
            stheta = positions_dict["sth"]
        else:
            stheta = self._tagged["theta"][0].position
        
        costh = numpy.cos((stheta)*ur.deg).magnitude
        sinth = numpy.sin((stheta)*ur.deg).magnitude
        
        real_dict.update (
            {
                "theta": stheta,
                "x": costh * positions_dict["sxx"] - sinth * positions_dict["syy"],
                "y": sinth * positions_dict["sxx"] + costh * positions_dict["syy"],
            }
        )
                
        
        #print ('ret2',real_dict)
        
        return real_dict
    

