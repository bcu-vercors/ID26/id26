import numpy
import math
import gevent
import logging

from bliss.physics import trajectory
from bliss.common.axis import CyclicTrajectory
from bliss.common.cleanup import cleanup, axis as cleanup_axis
from bliss.common.logtools import (
    log_error,
    log_warning,
    log_debug,
    log_info,
    log_exception,
    get_logger,
)
from bliss.common.motor_group import TrajectoryGroup, Group
from bliss.common.tango import DevState

from bliss.scanning.chain import ChainPreset
from bliss.scanning.scan_meta import get_user_scan_meta

from id26.controllers.id26undulators import ID26_Undulator

"""
- package: id26.controllers.fscan_controller
  class: ID26FastScan
  name: fastscan
  parameters: $fscan_par
  musst: $musst_cc1
  monochromator: $dcm
  zap_multiplexer: $multiplexer_opiom_cc1
  encoder_pm: $musst_enc_pm
  encoder_hdh: $musst_enc_hdh
  fast_shutter: $fsh
  machinfo: $machinfo
  id_sync_wago: $wcid26h


- package: id26.controllers.parameters
  name: fscan_par
  class: ID26Setting
  defaults:
  - id_linked: 0
  - id_sync: 0
  - id_master: None
  - u35a_min_speed: 0.002 # with new controllers of the undulator mechanics
  - u35b_min_speed: 0.005 # with new controllers of the undulator mechanics
  - u35c_min_speed: 0.002 # with new controllers of the undulator mechanics
  - id_max_speed: 4.9
  - id_setvel_sleep_time: 1
  - id_setvel_max_try: 5
  - id_backlash: 0.2
  - pm600_prf_intervals: 10   # number of intervales with constant motor speed
  - pm600_wp_delay: 0 #0.2 # signal delay between WP and XP
  - mono_move_back: 0   # move mono back to starting position
  - mono_move_back_speed_steps: 180000
  - mono_max_speed_steps: 200000 
"""


class ID26FScanController:
    def __init__(self, name, config_dict):
        self.name = name
        self.config = config_dict
        self.fscan_par = config_dict["parameters"]
        self.dcm = config_dict["monochromator"]
        self.musst = config_dict["musst"]
        self.opiom = config_dict["zap_multiplexer"]
        self.enc_pm = config_dict["encoder_pm"]
        self.enc_hdh = config_dict["encoder_hdh"]
        self.fshutter = config_dict["fast_shutter"]
        self.machinfo = config_dict["machinfo"]

        self.energy = self.dcm.motors["energy"]

        self.eneund = self.dcm.motors["energy_tracker"]

        self.mono = self.energy.controller._tagged["bragg"][0]
        self.pm600 = self.mono.controller

        self.acq_master = None

        try:
            self.idtracker = self.dcm.motors["energy_tracker"].controller
        except Exception as err:
            self.idtracker = None
            log_error(
                self,
                "undulator tracking (id_linked) is unusable - some necessary hardware is not initialised. ({0})".format(
                    str(err)
                ),
            )
            self.fscan_par["id_linked"] = False

        try:
            self.wago = config_dict["id_sync_wago"]
        except Exception as err:
            self.wago = None
            log_error(
                self,
                "fscan with undulator cabled synchro tracking (id_sync) is unusable - some necessary hardware is not initialised. ({0})".format(
                    str(err)
                ),
            )
            self.fscan_par["id_sync"] = 0

        self._initialise_fscan_parameters()
        self._init_meta_data_publishing()

        self.greenlet_move_back = None
        self.greenlet_id_set_vel = dict()
        self.event_trajectory_started = gevent.event.Event()
        self._last_scan = dict()
        self.gap_start_dict = dict()
        self.id_parameters = dict()

        get_logger(self).setLevel(logging.INFO)

        log_info(
            self,
            "{0} controller initialized".format(self.name),
        )

    def __info__(self):
        msg = f"FScan: {self.__repr__()}\n"

        msg += f"Axis: {self.energy.name} "
        for id in list(self._get_id_tracked()):
            msg += f"{id.name} "

        msg += f"\nFShutter is { 'AUTO' if self.opiom.getOutputStat('FSON') == 'ON' else self.fast_shutter.state.value.upper()}\n"
        msg += f"\nMonochromator: {self.dcm.name}, {self.mono.name} {self.mono.controller}\n"
        msg += f"   ID tracker: {self.idtracker.pseudos[0].name if self.idtracker is not None else None}, ID synchro: {self.wago.name if self.wago else None}\n"

        msg += "\n"
        msg += self.fscan_par.__info__()

        for id in self.greenlet_id_set_vel.keys():
            if self.greenlet_id_set[id].ready() is False:
                msg += f"\nWARNING: id_prepare_velocity greenlet is active on {id.name}. (hint: {self.name}.reset())"
        if (
            self.greenlet_move_back is not None
            and self.greenlet_move_back.ready() is False
        ):
            msg += f"\nWARNING: move back greenlet is active.. (hint: {self.name}.wait() or {self.name}.reset())"

        if (
            self.wago.controller.get("ID_sync")
            or self.wago.controller.get("ID_trig")
            or not self.wago.controller.get("u35b_ready")
            or not self.wago.controller.get("u35c_ready")
        ):
            msg += f"\nWARNING: id_sync needs resetting... (hint: {self.name}._reset_id_sync())"

        return msg

    def _get_id_tracked(self):
        for id in self.idtracker.reals:
            if id.name != self.energy.name:
                if isinstance(id.controller, ID26_Undulator) and id.tracking.state:
                    yield id

    def _initialise_fscan_parameters(self):
        def _cb_init_undu_tracking(value):

            if value:
                if self.idtracker == None:
                    log_error(
                        self,
                        "fscan with undulator tracking (id_linked) is unusable - some necessary hardware is not initialised",
                    )
                    value = False

            return value

        def _cb_init_undu_sync(value):
            if value == 1:
                if self.wago == None:
                    log_error(
                        self,
                        "fscan with undulator cabled synchro (id_sync) is unusable - some necessary hardware is not initialised.",
                    )
                    value = 0

            return value

        self.fscan_par._call_backs = {
            "id_linked": _cb_init_undu_tracking,
            "id_sync": _cb_init_undu_sync,
        }

        self.fscan_par._no_setup = [
            "pm600_pre_xp",
            "pm600_post_xp",
            "energy_start",
            "energy_end",
            "energy_step",
            "duration",
            "enc_hdh_0",
            "enc_pm_0",
            "id_acc_time",
            "max_speed_steps",
        ]

    def _init_meta_data_publishing(self):
        if not self.name:
            return
        scan_meta_obj = get_user_scan_meta()
        scan_meta_obj.instrument.set(self, lambda _: {self.name: self.metadata()})

    def metadata(self):

        log_debug(self, "metadata ...")

        meta_dict = dict()

        for key in self.fscan_par.keys():
            meta_dict.update({key: self.fscan_par[key]})

        meta_dict["@NX_class"] = "NXfastscan"

        log_debug(self, "metadata done.")

        return meta_dict

    @property
    def synchro_delay(self):
        delay = self.fscan_par["pm600_wp_delay"]
        return delay


    def _check (self, energy_start, energy_end, duration, energy_step):
        
        if energy_start >= energy_end:
            raise ValueError(
                f"FSCAN: Energy must go incrementing. start ({energy_start}) end ({energy_end})"
            )

        if energy_step == 0:
            raise ValueError("FSCAN: energy_step cannot be 0.")

        if self._check_limits(self.mono, self.dcm.energy2bragg(energy_end)) < 0:
            raise ValueError(
                f"FSCAN: move to {energy_end} keV would exceed low limit on {self.mono.name} ({self.mono.limits[0]} deg.)"
            )
        elif self._check_limits(self.mono, self.dcm.energy2bragg(energy_end)) > 0:
            raise ValueError(
                f"FSCAN: move to {energy_end} keV would exceed high limit on {self.mono.name} ({self.mono.limits[1]} deg.)"
            )

        nbsteps = round((energy_end - energy_start) / (energy_step))

        assert (
            nbsteps > 0
        ), f"energy step too large ({energy_step}) for that energy scan range ({energy_end - energy_start})"

        return nbsteps
    
        
    def prepare(self, energy_start, energy_end, duration, energy_step):

        log_debug(self, "prepare ...")

        # PHASE 0 initialize

        nbsteps = self._check (energy_start, energy_end, duration, energy_step)
        nbpoints = int(nbsteps) + 1

        self.wait()

        self.fscan_par["energy_start"] = energy_start
        self.fscan_par["energy_end"] = energy_end
        self.fscan_par["energy_step"] = energy_step
        self.fscan_par["duration"] = duration
        self._last_scan["init_duration"] = duration

        self.event_trajectory_started.clear()

        log_debug(
            self,
            "EVENT Trajectory Started CLEARED",
        )


        if self.energy.position == numpy.nan:
            log_error("Energy position is nan - cannot move")
            print("Hints:")
            print(f" - Is your monochromator {self.dcm.name} correctly initialized ?")
            if self.dcm._xtals.xtal_sel is None:
                print(
                    f" - Does it have a crystal selected ? ({self.dcm.name}.xtal_change ()) "
                )
            print(f" - try {self.energy.name}.sync_hard()")
            raise RuntimeError

        # PHASE 1 mono move to start position

        # if self.energy.position == numpy.nan or if abs(self.energy.position-energy_start)>self.energy.tolerance:
        # TODO this was due to a bug in bliss - should work now using motor_step_precision or sth like that
        if abs(self.energy.position - energy_start) > self.energy.tolerance:
            self.mono.velocity = (
                self.fscan_par["mono_move_back_speed_steps"] / self.mono.steps_per_unit
            )
            self.energy.move(energy_start, wait=False)

        # PHASE 1 calculate ID velocities

        if self.fscan_par["id_linked"]:
            try:
                master_id = self.dcm.motors["energy_tracker"].controller.axes[
                    self.fscan_par.get("id_master")
                ]
                print(f"MASTER ID is {master_id.name}")
            except:
                master_id = None
                print(f"NO MASTER ID")

            undulist = list(self._get_id_tracked())
            movement = []
            backlash_movement = []

            if not self._same_as_last_scan(
                "energy_start",
                "energy_stop",
                "energy_step",
                "id_linked",
                "id_master",
                "init_duration",
                "duration",
            ):
                self.gap_start_dict = {}
                movement, backlash_movement = self._id_prepare(
                    master_id, undulist, duration, energy_start, energy_end
                )

            else:
                duration = self._last_scan["duration"]
                for id, gap_start_pos in self.gap_start_dict.items():
                    movement.append(id)
                    movement.append(gap_start_pos)

            mot_group = Group(*undulist)
            mot_group.move(*movement, wait=False)
            # mot_group.move(*backlash_movement)

        if self.fscan_par["id_linked"]:
            if master_id is None:
                duration = numpy.array(
                    [self.id_parameters[id][3] for id in undulist]
                ).mean()
            else:
                duration = self.id_parameters[master_id][3]

        exp_time = duration / nbsteps

        log_debug(
            self,
            "nbpoints: {0}, exp_time: {1}, duration: {2}".format(
                nbpoints,
                exp_time,
                duration,
            ),
        )

        if exp_time < self.fscan_par.get("exp_time_min", 0):
            raise RuntimeError("Exposure time")

        self.fscan_par["duration"] = duration

        log_debug(self, "prepare done.")

        if self.fscan_par["id_linked"]:
            id_param = self.id_parameters
        else:
            id_param = {}

        return nbpoints, exp_time, duration, id_param

    def _id_prepare(self, master_id, undulist, duration, energy_start, energy_end):

        log_debug(self, "_id_prepare ...")

        self.id_parameters = {}

        if master_id is not None:

            if master_id not in undulist:
                master_id.tracking.on()
                undulist.append(master_id)

            # GREENLETIZE - unused, have to wait for it imediately after
            self.id_parameters[master_id] = self._id_prepare_velocity(
                master_id, duration, energy_start, energy_end
            )
            duration = self.id_parameters[master_id][
                3
            ]  # the requested duration will be set to the duration calculated for the master undulator
            log_debug(
                self,
                "MASTER ID is {0}, {1} seconds as scan duration matches possible master ID velocity".format(
                    master_id.name, duration
                ),
            )

        movement = []
        backlash_movement = []

        for id in undulist:
            if master_id is None:
                self.greenlet_id_set_vel[id] = gevent.spawn(
                    self._id_prepare_velocity, id, duration, energy_start, energy_end
                )

            elif master_id is not None and id != master_id:
                self.greenlet_id_set_vel[id] = gevent.spawn(
                    self._id_prepare_velocity,
                    id,
                    duration,
                    energy_start,
                    energy_end,
                    master_speed=self.id_parameters[master_id][2],
                )

        if self.greenlet_id_set_vel:
            gevent.joinall(
                list(self.greenlet_id_set_vel.values())
            )  # or add raise_error = True, but dont know what will happen with other greenlets ?
            for id in undulist:
                if id != master_id:
                    # self.id_parameters [id] = self.greenlet_id_set_vel.pop(id).value
                    # may that way raise the exception that migth have happened within the greenlit
                    self.id_parameters[id] = self.greenlet_id_set_vel.pop(id).get(
                        block=True
                    )

        if master_id is None:
            id_acctime = []
            for id in undulist:
                id_acctime.append(self.id_parameters[id][4])
            maxtime = max(id_acctime)

        for id in undulist:
            gap_start = self.id_parameters[id][0]
            velocity = self.id_parameters[id][2]
            acctime = self.id_parameters[id][4]
            accdist = self.id_parameters[id][5]

            # delay between undu motion trigger received by PM600 and start of trajectory. (OPIOM cable)
            gap_offset = velocity * self.synchro_delay  # speed*time = distance: ok
            # gap_offset = 0 ???? # the delay does not translate into a gap offset

            if master_id is not None:
                accdist = self.id_parameters[master_id][5]
                self.fscan_par["id_acc_time"] = self.id_parameters[master_id][4]
            else:
                accdist += (maxtime - acctime) * velocity
                self.fscan_par["id_acc_time"] = maxtime

            gap_starting_position = gap_start - accdist - gap_offset

            log_debug(
                self,
                "{0} gap_first_pos {2} acctime {1} gap_start {3} duration {4} velocity {5}".format(
                    id.name,
                    acctime,
                    gap_starting_position,
                    gap_start,
                    self.id_parameters[id][3],
                    velocity,
                ),
            )

            # if 'id_backlash' in self.fscan_par:
            #   backlash = self.fscan_par['id_backlash']
            #   delta = gap_starting_position - getattr(id.controller.device, f'{id.name}_gap_position')
            #   if math.copysign (delta, backlash) != delta:
            #       backlash_movement.append(id)
            #       backlash_movement.append(gap_starting_position)
            #       gap_starting_position -= 2*backlash

            movement.append(id)
            movement.append(gap_starting_position)
            self.gap_start_dict[id] = gap_starting_position

        log_debug(self, "_id_prepare done.")

        return movement, backlash_movement

    def _id_prepare_velocity(
        self, id, duration, energy_start, energy_end, master_speed=None
    ):

        log_debug(self, "_id_prepare_velocity ...")

        gap_start = id.tracking.energy2tracker(energy_start)
        gap_end = id.tracking.energy2tracker(energy_end)

        minspeed = self.fscan_par.get(f"{id.name}_min_speed")
        if not minspeed:
            raise ValueError(f"FSCAN: {id.name} min speed not defined")
        maxspeed = self.fscan_par["id_max_speed"]

        if master_speed is None:
            delta_gap = gap_end - gap_start
            speed = delta_gap / duration
            speed = minspeed * int(speed / minspeed)
        else:
            speed = master_speed

        if speed < minspeed:
            speed = minspeed

        elif speed > maxspeed:
            speed = maxspeed

        with cleanup(id, restore_list=(cleanup_axis.VEL,)):
            if id.state == "READY":
                id.controller.set_velocity(id, speed)
                gevent.sleep(self.fscan_par["id_setvel_sleep_time"])
            else:
                raise RuntimeError(
                    f"FSCAN: State of axis {id} is {id.state} ! cannot change velocity."
                )

            set_velocity = getattr(
                id.controller.device, f"{id.name}_GAP_velocity"
            )  # inquire what speed the undulator acctually accepted and will move with

            for i in range(self.fscan_par.get("id_setvel_max_try", 1)):
                ok = False
                if set_velocity / speed > 1.1 or set_velocity / speed < 0.9:
                    log_warning(
                        self,
                        "{3}:{0}: velocity not set yet (set_velocity {1}, calculated speed {2})".format(
                            id.name,
                            set_velocity,
                            speed,
                            i,
                        ),
                    )
                    if i > self.fscan_par.get("id_setvel_max_try", 1) / 2:
                        id.controller.set_velocity(id, speed)
                    gevent.sleep(self.fscan_par["id_setvel_sleep_time"])
                    set_velocity = getattr(
                        id.controller.device, f"{id.name}_GAP_velocity"
                    )
                else:
                    ok = True
                    break
            if not ok:
                raise RuntimeError(
                    f"FSCAN: could not set velocity {speed} (current: {set_velocity})"
                )

            log_debug(
                self,
                "{0}: applying velocity {1} and read back velocity {2}.".format(
                    id.name,
                    speed,
                    set_velocity,
                ),
            )

            calc_duration = (gap_end - gap_start) / set_velocity

        id.velocity = (
            maxspeed  # to make sure it moves with maxspeed to starting position
        )

        # consider undulator acceleration
        acceleration = getattr(
            id.controller.device, id.controller.axis_info[id].get("attr_acc_name")
        )
        firstvelocity = getattr(
            id.controller.device, id.controller.axis_info[id].get("attr_fvel_name")
        )

        if set_velocity > firstvelocity and acceleration != 0:
            acctime = (set_velocity - firstvelocity) / acceleration
            accdist = (set_velocity - firstvelocity) * acctime
        else:
            acctime = 0
            accdist = 0

        log_debug(self, "_id_prepare_velocity done.")

        return (gap_start, gap_end, set_velocity, calc_duration, acctime, accdist)

    def _check_limits(self, axis, user_target_pos):

        dial_target_pos = axis.user2dial(user_target_pos)
        target_pos = dial_target_pos * axis.steps_per_unit
        user_low_limit, user_high_limit = axis.limits
        low_limit = axis.user2dial(user_low_limit) * axis.steps_per_unit
        high_limit = axis.user2dial(user_high_limit) * axis.steps_per_unit

        if high_limit < low_limit:
            high_limit, low_limit = low_limit, high_limit
            user_high_limit, user_low_limit = user_low_limit, user_high_limit

        if target_pos < low_limit:
            return -1
        if target_pos > high_limit:
            return 1
        return 0

    def _backlash(self, axis, delta):
        backlash = axis.backlash / axis.sign * axis.steps_per_unit
        if backlash:
            if abs(delta) > 0 and math.copysign(delta, backlash) != delta:
                target_pos -= backlash
                delta -= backlash
            else:
                backlash = 0

    def program_trajectories(
        self,
        energy_start,
        energy_end,
        duration,
        energy_step,
    ):

        log_debug(self, "program_trajectories ...")

        prf_intervals = self.fscan_par["pm600_prf_intervals"]
        id_linked = self.fscan_par["id_linked"]

        traj = list()
        group = None

        # PM600
        traj.append(
            self._mono_program_trajectory(
                energy_start,
                energy_end,
                duration,
            )
        )

        # ID
        # if not self._same_as_last_scan('energy_start', 'energy_stop', 'energy_step', 'id_linked', 'id_master','duration', 'pm600_prf_intervals', 'synchro_delay')

        # (PhasePlate ?)
        # TrajectoryGroup: This object groups the movements for all axis along trajectories
        # group = TrajectoryGroup(pm600_trajectory,y_trajectory, calc_axis=None)

        group = TrajectoryGroup(*traj)

        try:
            log_debug(
                self, "waiting mono is ready. mono is %s",self.mono.state
            )
            self.mono.wait_move()

            self.fscan_par["enc_hdh_0"] = self.enc_hdh.read()
            #self.fscan_par["enc_pm_0"] = self.enc_pm.read()

            if "READY" == self.mono.state:
                log_debug(self, "loading trajectories")
                # GREENLETIZE - already a greenlet but joined. must be waited anyway last timeconsuming action in the sequence.
                group.prepare()  # the program is loaded in PM600 here

            else:
                raise RuntimeError(
                    f"FSCAN: {self.mono.name} is {self.mono.state}. cannot load trajectory."
                )

        except Exception as err:
            log_exception(self, "could not load trajectories into controllers")
            raise (err)

        log_debug(self, "program_trajectories done.")

        return group

    def _mono_program_trajectory(
        self,
        energy_start,
        energy_end,
        duration,
    ):

        log_debug(self, "_mono_program_trajectory ...")

        nintervals = self.fscan_par["pm600_prf_intervals"]

        # At t0 == 0 and t9 == duration seconds in (nintervals+1) points.
        times = numpy.linspace(0, duration, nintervals + 1)
        angles = self.dcm.energy2bragg(
            numpy.linspace(energy_start, energy_end, nintervals + 1)
        )  # degrees

        # this is user angles - convert into dial in case there is an offset on the mono:
        # (position - self.offset) / self.sign
        # backlash = self.backlash / self.sign * self.steps_per_unit
        # velocity_in_steps = velocity * self.sign * self.steps_per_unit

        angles -= self.dcm.motors["bragg"].offset
        angles /= self.dcm.motors["bragg"].sign

        # build PVT array and create trajectory object for mono axis:
        traj = trajectory.PointTrajectory()
        angles_c = angles - angles[0]
        traj.build(times, {"PM600": angles_c})
        pvt = traj.pvt()
        pm600_trajectory = ID26FscanCyclicTrajectory(
            self.mono,
            pvt["PM600"],
            backlash=self.mono.backlash,
            nb_cycles=1,
            origin=angles[0],
        )

        # as pre_xp and post_xp are not passed to *trajectories, set them elsewhere in pm600 controller
        # group.prepare() -->             gevent.spawn(controller._prepare_trajectory, *trajectories)
        #                                             wont pass any other parameters....
        # add pre_xp and post_xp pieces of sequences
        # 1WP22222220 write port , i.e. send signal to opiom to start the zap

        pre_xp = []

        if self.fscan_par["id_linked"]:
            pre_xp.append("WA0")  # wait for last bit to be 0,= ID_moving
            pre_xp.append(
                f"DE{int(self.fscan_par['id_acc_time']*1000)}"
            )  # setup delay for ID acceleration time
            pre_xp.append("WE")  # wait delay ends

        pre_xp.append("WP22222220")  # write port, send signal to opiom to start the zap
        pre_xp.append(
            f"DE{self.synchro_delay*1000}"
        )  # setup delay before starting the profile (FSCAN_PAR['pm600_wp_delay'] and shutter delay in some cases)
        pre_xp.append("WE")  # wait delay ends

        post_xp = ["WE", "WP22222221"]
        # post_xp = ['WP22222222'] #just to read the OK after 1XS2
        # do not move WP command to fscan_cleanup - that would make XS command not returning OK

        # 1DExx set delay to wait acq finishes -> post_xp.add('DE{0}'.format(delay2))
        # 1MAxx go back to steps0 if requested -> done by cleanup
        # 1SVxx set back steady velocity       -> done by cleanup

        self.mono.trajectory_pre_xp = pre_xp
        self.mono.trajectory_post_xp = post_xp

        self.fscan_par["pm600_pre_xp"] = self.mono.trajectory_pre_xp
        self.fscan_par["pm600_post_xp"] = self.mono.trajectory_post_xp

        pos = pm600_trajectory.pvt["position"]
        vel = (
            pm600_trajectory.pvt["position"][:-1] - pm600_trajectory.pvt["position"][1:]
        ).max()
        vel /= duration / nintervals
        mxv = self.fscan_par["mono_max_speed_steps"] / self.mono.steps_per_unit

        log_debug(
            self,
            "{0} profile intervals on PM600: {1}, max speed estimate: {2}".format(
                nintervals,
                pos,
                vel,
            ),
        )

        if vel > mxv:
            raise RuntimeError(
                f"FSCAN: above Max speed limit on PM600: {vel} (max: {mxv})"
            )

        log_debug(self, "_mono_program_trajectory done.")

        return pm600_trajectory

    def _move_back(self):

        log_debug(self, "_move_back ...")

        energy_start = self.fscan_par["energy_start"]
        print (f'\nMOVE BACK: fscan motors are moving back to energy start, {energy_start} keV')

        movement = [self.energy, energy_start]
        motlist = [self.energy]

        if self.fscan_par["id_linked"]:
            for axis in self._get_id_tracked():
                motlist.append(axis)
                movement.append(axis)
                movement.append(axis.tracking.energy2tracker(energy_start))

        self.mono.velocity = (
            self.fscan_par["mono_move_back_speed_steps"] / self.mono.steps_per_unit
        )
        group = Group(*motlist)
        group.move(*movement)
        self.mono.velocity = self.mono.config_velocity

        # print("- PM600 position:", self.mono.position)
        # print("- PM600 velocity:", self.mono.velocity)
        # print("- musst_enc_hdh.energy() : ", self.enc_hdh.energy())

        print ('\nMOVE BACK: fscan motors have arrived at starting position - READY for next fscan')

        log_debug(self, "_move_back done.")

    def _cleanup(self):

        log_debug(self, "_cleanup ...")

        # print("FSCAN ENDING")
        # readsacknoledge of XS command that is delayed always
        self.pm600.flush()
        #try:
        #    self.pm600.sock.readline()
        #except Exception as e:
        #    log_debug(
        #        self,
        #        "_cleanup : nothing (no OK) on serial line (%s)",e
        #    )

        self._cleanup_sync()

        if self.fscan_par["mono_move_back"] == 1:
            self.greenlet_move_back = gevent.spawn(self._move_back)
        else:
            self.mono.velocity = self.mono.config_velocity

        log_debug(self, "_cleanup done")

    def _cleanup_sync(self):
        self.energy.sync_hard()

        self.event_trajectory_started.clear()

        log_debug(
            self,
            "EVENT Trajectory Started CLEARED",
        )

    def _save_last_scan(self):
        for key in (
            "energy_start",
            "energy_stop",
            "energy_step",
            "duration",
            "pm600_prf_intervals",
            "id_linked",
            "id_master",
        ):
            self._last_scan[key] = self.fscan_par.get(key)
        self._last_scan["synchro_delay"] = self.synchro_delay

    def _same_as_last_scan(self, *keys):

        keys = list(keys)

        if len(keys) == 0:
            return False

        for key in keys:
            if key not in self._last_scan.keys():
                log_debug(
                    self,
                    f"{key} not in last scan parameters",
                )
                return False

        if "synchro_delay" in keys:
            if self._last_scan["synchro_delay"] != self.synchro_delay:
                log_debug(
                    self,
                    "synchro delays changed",
                )
                return False
            keys.pop(keys.index("synchro_delay"))

        if "init_duration" in keys:
            if self._last_scan["init_duration"] != self.fscan_par.get("duration"):
                log_debug(
                    self,
                    "last scan duration changed",
                )

                return False
            keys.pop(keys.index("init_duration"))

        for key in keys:
            if self._last_scan[key] != self.fscan_par.get(key):
                log_debug(
                    self,
                    f"{key} changed since last scan",
                )
                return False

        return True

    def _error_cleanup(self):

        log_debug(self, "_error_cleanup ...")

        if self.event_trajectory_started.is_set():
            print(
                "FSCAN ERROR CLEANUP - aborting trajectories and resetting controller ports"
            )
            self.pm600.stop_trajectory()
            self.pm600.flush()
            self.pm600.io_command("WP22222221", self.mono.channel)

            # no need - already in acquisition_master
            # if self.idtraj is not None:
            #    self.idtraj._stop_trajectory()

            self._cleanup_sync()

        self.mono.velocity = self.mono.config_velocity

        if self.fscan_par["id_sync"]:
            self._reset_id_sync()

        # self._last_scan = {} this is only meant to skip preparation phase, so can be kept

        log_debug(self, "_error_cleanup done.")

    def _reset_id_sync(self):
        """
        Reset ID synchronisation with cable
        """
        log_debug(self, "_reset_id_sync ...")
        if not self.wago.get("u35b_ready"):
            self.wago.set("ID_sync", 0)
            self.wago.set("ID_trig", 1)
            gevent.sleep(0.05)
            log_debug(self, "wago u35b sync released")

        if not self.wago.get("u35c_ready"):
            self.wago.set("ID_sync", 1)
            self.wago.set("ID_trig", 1)
            log_debug(self, "wago u35c sync released")

        self.wago.set("ID_sync", 0)
        self.wago.set("ID_trig", 0)

        for axis in self._get_id_tracked():
            axis.controller.device.Abort()

        # print("wago ID_sync released")
        log_debug(self, "_reset_id_sync done.")

    def reset(self):

        log_debug(self, "reset ...")

        self.energy.settings.set("state", self.energy.hw_state)
        self.eneund.settings.set("state", self.eneund.hw_state)

        self.pm600.stop_trajectory()
        self.pm600.flush()
        self.pm600.io_command("WP22222221", self.mono.channel)

        self.energy.sync_hard()
        self.mono.velocity = self.mono.config_velocity

        self.event_trajectory_started.clear()

        self._last_scan = {}
        self.mono.trajectory_prog.clear()
        if self.fscan_par["id_linked"]:
            self._reset_id_sync()

        if len(self.greenlet_id_set_vel) > 0:
            gevent.joinall(list(self.greenlet_id_set_vel.values()))
            undulist = self.greenlet_id_set_vel.keys()
            for id in undulist:
                self.greenlet_id_set_vel.pop(id).kill()

        if (
            self.greenlet_move_back is not None
            and self.greenlet_move_back.ready() is False
        ):

            log_debug(self, "move back greenlet is active - will be killed")

            print(
                f'MOVE BACK GREENLET ALIVE: motors are still moving back to starting energy, {self.fscan_par["energy_start"]} keV'
            )

            self.greenlet_move_back.kill()
            self.mono.stop(wait=True)
            self.idtracker.stop(wait=True)

            print("MOVE BACK GREENLET KILLED: fscan motors have stopped")
            self.mono.velocity = self.mono.config_velocity

            log_debug(self, "move back greenlet killed")

        self.opiom.switch("ACQ_TRIG", "COUNT")

        print("- PM600 state :", self.mono.state)
        print("- energy state:", self.energy.state)
        print("- PM600 velocity:", self.mono.velocity)
        print("- PM600 position        :", self.mono.position)
        print("- musst_enc_hdh.angle() : ", self.enc_hdh.angle())
        print("- musst_enc_hdh.energy() : ", self.enc_hdh.energy())
        print("- READY for next fscan")

        log_debug(self, "reset done.")

    def wait(self):
        log_debug(self, "wait ...")

        if self.greenlet_move_back is not None:
            if self.greenlet_move_back.dead is False:
                print(
                    f'motors are moving back to starting energy, {self.fscan_par["energy_start"]} keV'
                )
                log_debug(self, "move back greenlet is active - joining")

            self.greenlet_move_back.join()
        log_debug(self, "wait done.")


class ID26FscanCyclicTrajectory(CyclicTrajectory):

    """
    I need to remove last point in PVT when serveral scan cycles, because we want to finish zap measurement on energy_end
    energy incrementing
    angles decrementing
    delta
    delta = target_pos - current_pos
    backlash = self.backlash / self.sign * self.steps_per_unit
    if backlash:
      if abs(delta) > 0 and math.copysign(delta, backlash) != delta:
        # move and backlash are not in the same direction; apply backlash correction, the move will happen in 2 steps
        target_pos -= backlash
        delta -= backlash
      else:
        backlash = 0
    motion.backlash = backlash
    """

    def __init__(self, *args, backlash=None, **kwargs):
        self.backlash = backlash
        super().__init__(*args, **kwargs)

    @property
    def pvt(self):
        pvt = super().pvt
        if self.is_closed:  # OR self.nb_cycles>1
            pvt = numpy.delete(pvt, len(pvt) - 1)
            if self.backlash:
                pos = pvt["position"]
                delta = pos[1:] - pos[: len(pos) - 1]
                check1 = delta > 0 if self.backlash < 0 else delta < 0
                if check1[-1] is True:
                    raise RuntimeError(
                        "FSCAN: backlash cannot be completed at the end of such a scan"
                    )
                pos[1:] -= check1 * self.backlash
                pvt["position"] = pos
        return pvt


class ZapMultiplexerChainPreset(ChainPreset):
    def __init__(self, multiplexer):
        ChainPreset.__init__(self)
        self.multiplexer = multiplexer
        log_debug(
            self.multiplexer,
            "zap multiplexer initialized",
        )

    def prepare(self, acq_chain):
        pass

    def start(self, acq_chain):
        self.multiplexer.switch("ACQ_TRIG", "ZAP")
        log_debug(
            self.multiplexer,
            "acquisition triggering set to ZAP mode",
        )

    def stop(self, acq_chain):
        self.multiplexer.switch("ACQ_TRIG", "COUNT")
        log_debug(
            self.multiplexer,
            "acquisition triggering set to COUNT mode",
        )


class IDlinkedConstantVelocity:
    def __init__(self, id_tracker_parameters):
        self.id_speed_dict = id_tracker_parameters["id_speed_dict"]
        self.id_sync = id_tracker_parameters["id_sync"]
        self.id_wago = id_tracker_parameters["wago"]
        self.id_max_speed = id_tracker_parameters["id_max_speed"]

        get_logger(self).setLevel(logging.DEBUG)

    def prepare(self):
        # move to start already done in fscan.prepare
        # check it is finished or wait
        # then program gap_end motions ...
        if len(self.id_speed_dict) > 0:
            log_debug(
                self,
                "prepare",
            )
            self.idmove = []
            axislist = []
            for id in self.id_speed_dict:
                if id.state == "MOVING":
                    log_debug(
                        self,
                        "{0} still moving to start position - waiting ...".format(
                            id.name
                        ),
                    )
                id.wait_move()
                id.velocity = self.id_speed_dict[id][2]
                pos = self.id_speed_dict[id][1]
                axislist.append(id)
                self.idmove.append(id)
                self.idmove.append(pos)
            self.idgroup = Group(*axislist)
            if self.id_sync:
                self.id_wago.set("ID_sync", 1)
                log_debug(
                    self,
                    "wago ID_sync activated",
                )

    def start(self):

        if len(self.id_speed_dict) > 0:
            log_debug(
                self,
                "start",
            )
            self.idgroup.move(*self.idmove, wait=False)

            if self.id_sync:
                cnt = 0
                ready_list = list(
                    map(
                        lambda x: self.id_wago.get(f"{x.name}_ready"),
                        self.id_speed_dict.keys(),
                    )
                )
                ready = max(ready_list)

                while ready > 0:
                    log_warning(
                        self,
                        "wago u*_ready: {0} axis not ready - wait 0.1 sec. #{1}".format(
                            ready_list,
                            cnt + 1,
                        ),
                    )
                    gevent.sleep(0.1)
                    cnt += 1
                    if cnt > 150:
                        raise RuntimeError(
                            "FSCAN: ID not ready for synchro mode triggering"
                        )
                    ready_list = list(
                        map(
                            lambda x: self.id_wago.get(f"{x.name}_ready"),
                            self.id_speed_dict.keys(),
                        )
                    )
                    ready = max(ready_list)
                log_debug(
                    self,
                    "id motion ready.",
                )
                self.id_wago.set("ID_trig", 1)
                log_debug(
                    self,
                    "id motion triggered.",
                )

    def stop(self):
        if len(self.id_speed_dict) > 0:
            log_debug(
                self,
                "stop",
            )
            if self.id_sync:
                for id in self.id_speed_dict.keys():
                    id.controller.device.Abort()  # abort cabled sync motions
                    log_debug(
                        self,
                        f"{id.name} motion aborted.",
                    )
                ok = 0
                while id.controller.device.State() == DevState.MOVING:
                    # if one id is moving all controller device state is moving
                    if ok == 0:
                        print("at least one ID is MOVING - waiting it stops.\r")
                        ok = 1
                log_debug(
                    self,
                    "all ID have stopped.",
                )

                self.id_wago.set("ID_sync", 0)
                self.id_wago.set("ID_trig", 0)
                log_debug(
                    self,
                    "wago ID_sync released",
                )

            else:
                self.idgroup.stop()
                log_debug(
                    self,
                    "ID grouped motion stopped.",
                )

            for id in self.id_speed_dict.keys():
                id.velocity = self.id_max_speed

            log_debug(
                self,
                "ID max velocities restored.",
            )
