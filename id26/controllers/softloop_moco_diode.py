# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import time
import gevent
import numpy as np
from bliss.common.regulation import ExternalInput
from bliss.common.regulation import SoftLoop
from bliss.common.logtools import log_debug


class MocoDiodeInput(ExternalInput):
    """  """

    def __init__(self, name, config):
        super().__init__(config)
        
        self.offset_finm = config['offset_finm']
        self.offset_foutm = config['offset_foutm']
        self.signal_beam_limit = config['signal_beam_limit']
        self.signal_min = config.get('signal_min',-np.infty)
        self.signal_max = config.get('signal_max',np.infty)
        self._last_value = 0

    def _get_moco_counts(self):
        count_moco = self.device.comm('?fbeam')
        count_finm = float(count_moco.split()[0])
        count_foutm = float(count_moco.split()[1])
        return count_finm, count_foutm
        
    def allow_regulation(self):
        count_finm, count_foutm = self._get_moco_counts()
        
        d1 = count_finm - self.offset_finm
        d2 = count_foutm - self.offset_foutm
        
        if d1 <= 0 or d2 <= 0:
            return False
            
        signal_beam = d1 + d2 
        if (signal_beam >= self.signal_beam_limit) and (self.signal_min < signal_beam) and (signal_beam < self.signal_max):
            return True
        else:
            return False

    def read(self):
        """ """
        count_finm, count_foutm = self._get_moco_counts()
        
        d1 = count_finm - self.offset_finm
        d2 = count_foutm - self.offset_foutm
        
        if d1 > 0 and d2 > 0:
#            self._last_value = np.log(d1/d2)
            self._last_value = (d1 - d2)/(d1+d2)
        
        return self._last_value
        
        
    def state(self):
        """ returns the input device state """

        log_debug(self, "MocoDiodeInput:state")
        return "READY"




