import gevent

from bliss.common.scans import DEFAULT_CHAIN
from bliss.common.user_status_info import status_message
from bliss.common.logtools import log_debug

from bliss.scanning.chain import ChainIterationPreset

from bliss.controllers.machinfo import MachInfo, WaitForRefillPreset


class ID26MachInfo(MachInfo):

    def __init__(self, name, config):
        super().__init__(name, config)
        self.fshutter = config['fast_shutter']

    @property
    def check(self):
        return self._MachInfo__check

    @check.setter
    def check(self, flag):
        if flag:
            preset = ID26WaitForRefillPreset(self)
            DEFAULT_CHAIN.add_preset(preset, name=self.KEY_NAME)
            self._MachInfo__check = True
            print("Activating Wait For Refill on scans")
        else:
            DEFAULT_CHAIN.remove_preset(name=self.KEY_NAME)
            self._MachInfo__check = False
            print("Removing Wait For Refill on scans")

    def iter_wait_for_refill(self, checktime, waittime=0.0, polling_time=1.0):
        ok = self.check_for_refill(checktime)
        while not ok:
            self.fshutter.close_fast()
            yield "WAIT_INJECTION"
            gevent.sleep(polling_time)
            ok = self.check_for_refill(checktime)
            if ok:
                yield "WAITING_AFTER_BEAM_IS_BACK"
                gevent.sleep(waittime)
                ok = self.check_for_refill(checktime)
            if ok:
                self.fshutter.open_fast()


class ID26WaitForRefillPreset(WaitForRefillPreset):

    class PresetIter(ChainIterationPreset):
        def __init__(self, machinfo, checktime, waittime, polling_time):
            self.machinfo = machinfo
            self.checktime = checktime
            self.waittime = waittime
            self.polling_time = polling_time

        def start(self):
            ok = self.machinfo.check_for_refill(self.checktime)
            with status_message() as p:
                while not ok:
                    p("Close fast shutter. Waiting for refill...")
                    self.machinfo.fshutter.close_fast()
                    gevent.sleep(self.polling_time)
                    ok = self.machinfo.check_for_refill(self.checktime)
                    if ok and self.waittime:
                        p("Waiting {self.waittime} after Beam is back               ")
                        gevent.sleep(self.waittime)
                        ok = self.check_for_refill(self.checktime)
                    if ok:
                        p("Opening back fast shutter                       ")
                        self.machinfo.fshutter.open_fast()
                        gevent.sleep(self.machinfo.fshutter.delay)
                        

                        

#
# Simulation helpers for testing
#


class ID26MachInfoSimu(MachInfo):

    def __init__(self, *a, **ka):
        super().__init__(*a, **ka)
        self._simu_bool_list = [0, 0, 0, 0]
    
    def check_for_refill(self, checktime):
        if len(self._simu_bool_list)>0:
            return self._simu_bool_list.pop(0)
        return True

        
