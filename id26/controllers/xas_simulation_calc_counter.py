# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from bliss.controllers.counter import CalcCounterController
import numpy as np

class XanesCalcCounterController(CalcCounterController):
    # try as a sum of gaussian and linear
    def calc_function(self, input_dict):
        csum = 0
        for cnt in self.inputs:
            csum += input_dict[self.tags[cnt.name]]

        csum = csum / float(len(self.inputs))

        return {self.tags[self.outputs[0].name]: csum}
        
    # gaussian on top of a logistic function
    # need only a single input + x value(s) - linear input
            
        #inputdata = input_dict[self.tags[self.inputs[0].name]] #gaussian
        #some_x = np.linspace(0,10, len(inputdata))
        #logisticdata = np.max(inputdata) / (1 + np.exp(-(some_x-np.median(some_x))))
        #xanesdata = logisticdata + inputdata
        #return {self.tags[self.outputs[0].name]: xanesdata}
