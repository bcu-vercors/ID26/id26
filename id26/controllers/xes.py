import numpy
import time

from bliss.config.static import get_config
from bliss.common.axis import Axis
from bliss.common.cleanup import cleanup, axis as cleanup_axis
from bliss.common.logtools import log_warning, log_error, log_debug
from bliss.common.scans import loopscan
from bliss.common.scans.step_by_step import ascan
from bliss.scanning.chain import AcquisitionChannel
from bliss.scanning.group import Sequence
from bliss.scanning.scan_info import ScanInfo

from id26.scripts.fscan import fscan, FSCAN_PAR
from id26.controllers.measurement import ID26Measurement, load_gui_param
from id26.controllers.radiation_damage import RadiationDamagePreset, PositionManager

class Xes(ID26Measurement):

    def __init__(self, name, measurement):

        super().__init__(name, measurement)

        self._instance_type = 'xes'


    @load_gui_param
    def check(self, acq_time = None, number_of_scans = None, inc_energy = None, verbose = True):
    
        def _msg (key = None):
            if self.use_daiquiri:
                msg = "Please use Daiquiri to enter your measurement parameters"
            else:
                msg = "Please, add/fix the ['{0}'] dictionary item.".format(key)
            return msg
                
        def _get_xes_regions (param):
            for i in self.get('regions'):
                yield i[param]

        def _int2float (key):
            if isinstance (self.get(key), int):
                self[key]=float(self.get(key))
            
            
                
        #make sure all parameters for scan are defined and acceptable:
        
        if self.use_daiquiri:
            try:
                self['energy_step'] = list(_get_xes_regions ('reg_xes_en_step'))
                self['end_energy'] = list(_get_xes_regions ('reg_xes_en_end'))
                reg_acq_time = list(_get_xes_regions ('reg_acq_time'))
            except:
                raise ValueError ('corrupted xes parameteriser file - please create a new one')


        _int2float('start_energy')    
        _int2float('end_energy')    
        _int2float('energy_step')    
            
        if not isinstance (self.get('start_energy'), float):
            raise ValueError('start emission energy not defined, or not correct. {0} - must be a float number.'.format(_msg('start_energy')))

        if isinstance (self.get('end_energy'), float):
            self['end_energy'] = [self.get('end_energy')]
            
        if isinstance (self.get('energy_step'), float):
            self['energy_step'] = [self.get('energy_step')]

        if not isinstance (self.get('end_energy'), list) or not isinstance (self.get('energy_step'), list):
            raise ValueError('end energy and/or energy step not properly defined. \n{0}\n{1}\nmust be float numbers lists.'.format(
                _msg('end_energy'),
                _msg('energy_step'),
            ))
            
        if acq_time is None:
            if self.use_daiquiri:
                acq_time = reg_acq_time
            else:
                if self.get('acq_time') is None:
                    raise ValueError('Acquitision times not defined')
                _int2float('acq_time')    
                acq_time = self.get('acq_time')
                if isinstance (acq_time, float):
                    acq_time = [acq_time]
            
        acq_time = list(map(abs, acq_time))
        
        if (min(acq_time) == 0):
            raise RuntimeError(
                "Acquisition time cannot be 0"
        )    

        lengths = list(map (len,[acq_time, self['end_energy'], self['energy_step']]))
        try:
            assert sum(lengths)/len(lengths) == lengths[0]
        except AssertionError:
            raise ValueError('acquisition times / end energy and/or energy step not properly defined. \n{0}\n{1}\n{2}\nmust be float numbers lists - same size.'.format(
                _msg('acq_time'),
                _msg('end_energy'),
                _msg('energy_step'),
            ))

        if self.hutch == None or self.hutch.lower() not in ['eh1','eh2']:
            log_warning (
                self,
                "hutch unknown. [{0}]".format(self.hutch)
                )
        else:
            energies = [self['start_energy']] + self['end_energy']
            reverse = True if self.hutch.lower() == 'eh1' else False
            #EH1 - TEXS - energy must go down
            #EH2 - energy must go up
            energies.sort(reverse=reverse)

            try:
                assert energies == [self['start_energy']] + self['end_energy']
            except AssertionError:
                raise ValueError('start and end energy not properly defined. \n{0}\n{1}\nmust be ordered float numbers. Note: For TEXS start_energy is higher than end_energy'.format(
                    _msg('start_energy'),
                    _msg('end_energy'),
                ))
                
        
        if number_of_scans is None:
            number_of_scans = max(self.get('number_scans',1) ,1)
            
        if inc_energy is None:
            inc_energy = self.get('energy_excite_above')

        if verbose:
            print("Will do {0} loop(s)".format(number_of_scans))
            print ('Start Energy:',self['start_energy'])
            print ('End Energy:',self['end_energy'])
            print ('Energy Step:',self['energy_step'])
            print ('Acq. time(s):', acq_time)
            print ('Energy Excite Above:', inc_energy)
            print ('shg:',self.get('shg'))
            print ('svg:', self.get('svg'))
            print ('radiation protection:', self.move_scan_mode)
            print ('hutch:', self.hutch)
            nints=list()
            for idx, ene_end in enumerate(self['end_energy']):
                ene_start = self['start_energy'] if idx == 0 else self['end_energy'][idx-1] 
                nints.append(abs(round((ene_end-ene_start)/self['energy_step'][idx])))
            print('nints', nints)
            print ('* total number of points per scan', sum(nints)+1)
            print ('* total acquisition time per scan', nints[0]*acq_time[0]+sum(map(lambda x,y:x*y,acq_time,nints)))
        return acq_time, number_of_scans, inc_energy


    def scan(self, acq_time = None, number_of_scans = None, inc_energy = None, multiregion = False, run = True):
        '''
        single xes_en scan or multi-region xes_en scan
        if self['start_xes_en'], self['step_xes_en'] and self['acq_time'] are lists or numpy arrays
        '''
        # first check all necessary params/arguments defined
                
        acq_time, number_of_scans, inc_energy = self.check(acq_time, number_of_scans, inc_energy, verbose=False)

        self.move_together (
            {self.eneund: inc_energy,
             self.svg: self.get('svg'),
             self.shg: self.get('shg'),
         })

        self.radiation_damage_protect_init ()
        self.radiation_damage_protect_in_active_mg ()

        with cleanup (
                self.radiation_damage_protect_nopoints ,
                self.radiation_damage_protect_in_active_mg_reset ,
        ):

            
        #    for i in range(number_of_scans):
        #        seq = self._sequence_xes_regions([self['start_energy']] + self['end_energy'], self['energy_step'] , acq_time)
        # return self._seq.scan

            start = time.perf_counter()
            scans = []
            for i in range(number_of_scans):
                if multiregion:
                    scans.append(self._scan_xes_mr([self['start_energy']] + self['end_energy'], self['energy_step'] , acq_time, run=run))
                else:
                    scans.append(self._scan_xes_regions([self['start_energy']] + self['end_energy'], self['energy_step'] , acq_time))

            end = time.perf_counter()
            print(f"  Scan(s) took {(end-start):.2f}s\n")

        return scans

    def _sequence_xes_regions (self, xes_energies, ene_steps , acq_times, *counter_args):

        scan_info = ScanInfo()
        scan_info = ScanInfo.normalize(scan_info)

        scan_info.set_sequence_info(scan_count=len(ene_steps))
        scan_info.add_curve_plot(x=f"axis:{self.xes_en.name}")

        title = f'seq_xes_regions {self.xes_en.name} {xes_energies[0]}'
        for idx, ene in enumerate(xes_energies[1:]):
            title += f' {ene} {ene_steps[idx]} {acq_times[idx]}'

        def _nints ():
            n=0
            for idx, ene_start in enumerate(xes_energies[:-1]):
                ene_end = xes_energies[idx+1]
                n+= abs(round((ene_end-ene_start)/ene_steps[idx]))
            return n
        
        scan_info.update({
            'title':title,
            'start':xes_energies[0],
            'stop':xes_energies[-1],
            'npoints': _nints()+1,
            'type': 'seq_xes_regions',
            #'instrument': instrument,
        })

        # Create data group for special sequence channels
        scan_info.set_channel_meta(
            "scan_numbers",
            group="sequence",
        )
        scan_info.set_channel_meta(
            "scans",
            group="sequence",
        )

        scan_info.set_channel_meta(
            f"axis:{self.xes_en.name}", start=scan_info['start'], stop=scan_info['stop'], points=scan_info['npoints']
        )

        scan_info.update(self.scan_info)
        
        self._seq = Sequence(scan_info=scan_info, title=title)
        self._acq_channels = dict()#TODO only useful for debug

        scans = self._scan_xes_regions (xes_energies, ene_steps , acq_times, *counter_args, save=False, run=False)

        self._xes_scans = scans #useful for debug, and also for cc
        #print('scans written')

        for channel in scans[0].scan_info['channels']:
            self._acq_channels[channel] = []
            self._seq.add_custom_channel(AcquisitionChannel(channel,float,()))

        with self._seq.sequence_context() as seqc:
            
            #print('starting sequence execution')

            self._scan_seq = seqc   #TODO only useful for debug
            
            for n,sc in enumerate(scans):
                if len(scans)>1:
                   print (f'executing scan {int(seqc.sequence.scan.scan_number)} on region {n+1}/{len(scans)}:')
                if n==0:
                    self.radiation_damage_protect_scan ()
                elif n>0 and self.move_scan_mode == 'scan_segment':
                    self.radiation_damage_protect_scan()
                seqc.add(sc)
                try:
                    sc.run()
                    for ch in self._acq_channels.keys():
                        try: 
                            self._acq_channels[ch]+=list(sc.get_data()[ch])
                        except Exception as e:                            
                            log_warning(self, f"No data for channel: {ch}")
                            #breakpoint()
                            #images cannot be saved today
                        
                except Exception as e:
                    log_error(
                        self,
                        "xes scan {2} region {1} crashed with error ({0})".format(
                            e,
                            n,
                            int(seqc.sequence.scan.scan_number)
                        )
                    )
                    raise


            for channel_name, data in self._acq_channels.items():
                try:
                    self._seq.custom_channels[channel_name].emit(numpy.array(data))
                except Exception as e:
                    log_warning(self, f"Cannot emit for channel_name: {channel_name}")
                    #breakpoint()
                    
            return self._seq

    def _scan_xes_mr(self, xes_energies, ene_steps , acq_times, *counter_args, save=True, run=True):
        from id26.scripts.multiregion import multiregion
        from bliss import setup_globals
        exp_time = 0
        regions = []
        for idx, ene_start in enumerate(xes_energies[:-1]):
            if idx>0:
                ene_start+=ene_steps[idx-1]
            ene_end = xes_energies[idx+1]
            intervals =  abs(round((ene_end-ene_start)/ene_steps[idx]))
            print(f"region {idx+1} ({ene_start} to {ene_end}) has {int(intervals)} intervals, and lasts {intervals*acq_times[idx]} seconds")
            exp_time+=intervals*acq_times[idx]
            regions.append((ene_start, ene_end, int(intervals), acq_times[idx]))

        print("MR Counter args", counter_args)
        scan = multiregion(self.xes_en, regions, *counter_args, p201_master = setup_globals.p201_A, run=False)
        self.radiation_damage_protect_point(scan)

        if run:
            self.radiation_damage_protect_scan()
            scan.run()

            # TODO: how to deal with
            # self.move_scan_mode == 'scan_segment':
            #     self.radiation_damage_protect_scan()

        return scan

    def _scan_xes_regions(self, xes_energies, ene_steps , acq_times, *counter_args, save=True, run=True):
        
        scans = []
        exp_time = 0
        for idx, ene_start in enumerate(xes_energies[:-1]):
            if idx>0:
                ene_start+=ene_steps[idx-1]
            ene_end = xes_energies[idx+1]
            #points separating segments are measured twice !!!
            #change this by considering only ene_start and ene_steps and the last segment ene_end, but coordinate with Marius before
            intervals =  abs(round((ene_end-ene_start)/ene_steps[idx]))
            print(f"region {idx+1} ({ene_start} to {ene_end}) has {int(intervals)} intervals, and lasts {intervals*acq_times[idx]} seconds")
            exp_time+=intervals*acq_times[idx]
            scanid = ascan(self.xes_en, ene_start, ene_end, int(intervals), acq_times[idx], *counter_args, run=False, save=save)
            scans.append(scanid)
            self.radiation_damage_protect_point (scanid) 
  
        print("The total acquisition time (excluding bliss and metadata overhead) is {:.2f} seconds". format(exp_time))

        if run:
            for n, one_scan in enumerate(scans):
                if n==0:
                    self.radiation_damage_protect_scan ()
                elif n>0 and self.move_scan_mode == 'scan_segment':
                    self.radiation_damage_protect_scan()
                one_scan.scan_info.update(self.scan_info)
                one_scan.run()
            
        return scans


