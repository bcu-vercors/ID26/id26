# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.
from bliss.controllers.lima.roi import Roi
from bliss.controllers.counter import CalcCounterController
import numpy as np

class RoiShiftCounterController(CalcCounterController):
    # integrate roi shifted
    #def __init__(self, name, config):
        #self.camera = config.get("camera")
        #self.rois = self.camera.roi_counters.get_rois()
        # for roi in self.rois:
        # print(roi.name)
    
    def calc_function(self, input_dict):
        image = input_dict[self.tags[self.inputs[0].name]][0] 
        camera = self.inputs[0]._counter_controller
        #print(camera.name)
        #print(np.shape(image))        
        output_dict = dict()
        
        for cnt in self.outputs:
            tag = self.tags[cnt.name]
            if tag in camera.roi_counters.keys():
                roi_name = tag
                roi = camera.roi_counters[tag]
                image_roi = image[roi.x:roi.x+roi.width, roi.y: roi.y+roi.height] 
                roi_intg = np.sum(image_roi)
                #print(roi_intg)
                # TODO: replace find maxidx by a 2d gaussian fit!
                # TODO: get also FWHM from the fit
                # find maxidx  in the roi:
                myidx = np.unravel_index(np.argmax(image_roi), np.shape(image_roi))
                #print(myidx)
                new_start_x = int(myidx[0]+roi.x-(roi.width/2))
                new_start_y = int(myidx[1]+roi.y-(roi.height/2))
                #print(new_start_x, new_start_y)
                new_image_roi = image[new_start_x:new_start_x+roi.width, new_start_y: new_start_y+roi.height]
                #print(np.sum(new_image_roi))
                #print(new_start_x, new_start_y, roi.width, roi.height)
                # reapply the new roi indices!
                self.inputs[0]._counter_controller.roi_counters[roi_name] = Roi(x = new_start_x, y = new_start_y, width = roi.width, height = roi.height)
                # handle several rois!
                # find the roi name
                output_dict[tag] = np.sum(new_image_roi)
            else: # roi probably not defined
                output_dict[tag] = 0 # None does not work!
        #return {self.tags[self.outputs[0].name]: }
        return output_dict
        
