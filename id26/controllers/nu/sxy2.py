import numpy

from bliss.physics.units import ur
from bliss.controllers.motor import CalcController
from id26.scripts.beamline_settings import ID26Settings

class ID26Sxy2 (CalcController):

    def __init__(self, *args, **kwargs):

        CalcController.__init__(self, *args, **kwargs)

        config = self.config.config_dict #ou args[1]
        
        #self.angle_offset = config ['angle_offset']
        self.parameters = ID26Settings('sxy',
                                        defaults = {
                                            'angle_offset': 0,
                                        },
                                    )
      
    def __info__(self):

        info_str='{0}: {1}\n'.format(self._tagged['theta'][0].name,self._tagged['theta'][0].position)
        info_str+='{0}: {1} - {2}: {3}\n'.format(
            self._tagged['sxx'][0].name,self._tagged['sxx'][0].position,
            self._tagged['x'][0].name,self._tagged['x'][0].position,
        )
        info_str+='{0}: {1} - {2}: {3}\n\n'.format(
            self._tagged['syy'][0].name,self._tagged['syy'][0].position,
            self._tagged['y'][0].name,self._tagged['y'][0].position,
        )
        info_str += self.parameters.__info__()
        return info_str

    def calc_from_real(self, positions_dict):

        #print ('from',positions_dict)
        
        calc_dict = dict()

        offset = self.parameters['angle_offset']
        stheta = positions_dict["theta"]

        costh = numpy.cos((stheta+offset)*ur.deg).magnitude
        sinth = numpy.sin((stheta+offset)*ur.deg).magnitude
        
        calc_dict.update(
            {
                    "sxx":  costh * positions_dict["x"] + sinth * positions_dict["y"],
                    "syy": -sinth * positions_dict["x"] + costh * positions_dict["y"],
            }
        )

        if "sth" in self._tagged:
            calc_dict.update(
                {
                    "sth": stheta
                }
            )

        #print ('ret',calc_dict)
        
        return calc_dict

    def calc_to_real(self, positions_dict):

        #print ('to',positions_dict)
        #positions_dict can be an array - or - a scalar ...

        real_dict = dict()
        
        offset = self.parameters['angle_offset']

        if 'sth' in positions_dict:
            stheta = positions_dict["sth"]
        else:
            stheta = self._tagged["theta"][0].position
        
        costh = numpy.cos((stheta+offset)*ur.deg).magnitude
        sinth = numpy.sin((stheta+offset)*ur.deg).magnitude
        
        real_dict.update (
            {
                "theta": stheta,
                "x": costh * positions_dict["sxx"] - sinth * positions_dict["syy"],
                "y": sinth * positions_dict["sxx"] + costh * positions_dict["syy"],
            }
        )
                
        
        #print ('ret',real_dict)
        
        return real_dict
    

    
'''
backlash/limits well set should not make backlash motion hit limits.
limits of real motors are always checked prior to their move. no need of calculating this ... 
and that wont work in bliss like for monox !!!!

    def _sxy_limits (offset):
        pass
        crasps = cos(rad(A[stheta]+sthetaoffset))
        srasps = sin(rad(A[stheta]+sthetaoffset))
        trasps = tan(rad(A[stheta]+sthetaoffset))
        if (fabs(srasps) < 1e-10) { 
                syy_lim[1] = user_lim(sy, crasps)
                syy_lim[2] = user_lim(sy,-crasps)
                sxx_lim[1] = user_lim(sx, crasps)
                sxx_lim[2] = user_lim(sx,-crasps)
        } else if (fabs(crasps) < 1e-10) {    # ath = n*Pi + Pi/2
            syy_lim[1] = user_lim(sx, srasps)
            syy_lim[2] = user_lim(sx,-srasps)
            sxx_lim[1] = user_lim(sx, srasps)
            sxx_lim[2] = user_lim(sx,-srasps)
        } else {
            syy_lim[0] = -A[sxx]*trasps +user_lim(sy, 1)/crasps
            syy_lim[1] = -A[sxx]*trasps +user_lim(sy,-1)/crasps
            syy_lim[2] =  A[sxx]/trasps +user_lim(sx, 1)/srasps
            syy_lim[3] =  A[sxx]/trasps +user_lim(sx,-1)/srasps
            sxy_sort_array(syy_lim)
            sxx_lim[0] =  A[syy]*trasps +user_lim(sx,-1)/crasps
            sxx_lim[1] =  A[syy]*trasps +user_lim(sx, 1)/crasps
            sxx_lim[2] = -A[syy]/trasps +user_lim(sy,-1)/srasps
            sxx_lim[3] = -A[syy]/trasps +user_lim(sy, 1)/srasps
            sxy_sort_array(sxx_lim)
        }
        if(set_lim(motor_num(sxx), sxx_lim[1], sxx_lim[2]) != 0 ) {
                        print " set_lim on motor sxx failed"
                        return (-1)
        }

        if(set_lim(motor_num(syy), syy_lim[1], syy_lim[2]) != 0 ) {
                            print " set_lim on motor syy failed"
                            return (-1)
        }
            
'''
