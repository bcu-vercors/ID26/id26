import gevent
import math

from bliss.physics.units import ur

from bliss.controllers.moco import Moco
from bliss import setup_globals

class id26moco(Moco):

    def __init__(self, name, config_tree):
    
        Moco.__init__(self, name, config_tree)
        self.moco_max = 2  # used to calculate the slope
        self.percentage = 5

        dcm = config_tree.get('dcm')
        self.xtals = dcm._xtals
        self.energy_axis = dcm.motors['energy']
        self.energy2bragg = dcm.energy2bragg
            
    def set_voltage(self):
        
        self.outbeam("VOLT",  "NORM", "UNIP", 10, "NOAUTO", silent=True)
        self.slope = -1.3
        self.phase = 100 # PG 25/08/2021
        self.tau = 0.1
        self.frequency = 40

        print("MoCo box outbeam set to voltage.")
        print(f"Slope set to {self.slope} and phase {self.phase}. You may check this using moco_scan(phase).")
        
        self.moco_max = 2

    def set_current(self):
        
        self.outbeam("CURR",  "NORM", "UNIP", 5e-6, "NOAUTO")
        self.slope = 1e-6
        self.phase = 120
        self.tau = 0.1
        
        print("MoCo box outbeam set to current.")
        print(f"Slope set to {self.slope} and phase {self.phase}. You may check this using moco_scan(phase).")
        
        self.moco_max = 1e-6

    def start_regul(self, percent=None):

        if percent is None:
            percent = self.percentage
            
        self.stop()
                
        moco_amp = self.set_amp(percent)
        
        _aux =  (moco_amp/(percent/100)) # this is the FWHM of the RC in units of qgths

        if _aux == 0:
            print("Error in moco_set, _aux ==0  moco_slope not properly calculated")
            return
        else:
#            moco_slope = (self.moco_max/ _aux) /10
            moco_slope = -(self.moco_max/ _aux) / 5 # PG 25/08/2021

        self.slope = moco_slope
        print(f"Moco slope set to {moco_slope}.\n")

        self.go()

        print("MoCo feedback started.")
        print("Waiting for feedback to reach maximum...")
        
        gevent.sleep(5)
        
    def set_amp(self, percent=None):
        
        if percent is None:
            percent = self.percentage
            
        moco_amp = self.calc_amp(percent)
        self.amplitude = moco_amp
        print(f"MoCo box amplitude set. (moco_amp={moco_amp}) ")
    
        return moco_amp
        
    def calc_amp(self, percent=None):
        
        if percent is None:
            percent = self.percentage
        
        #xtals = energy.controller.mono.xtals # changed 29/8/2023 BD
        g_mo_d = (self.xtals.xtal[self.xtals.xtal_sel].d*ur.m).to("angstrom").magnitude

        if g_mo_d > 3.0:
            # energy resolution
            resolution = 7000.0
        elif (1.5 < g_mo_d) and (g_mo_d < 3):
            # energy resolution
            resolution=35000.0
        elif g_mo_d < 1.5:
            # energy resolution
            resolution = 70000.0

        ene_pos = self.energy_axis.position
        #bragg_pos = energy.controller.mono.energy2bragg(ene_pos) # changed 29/8/2023 BD
        bragg_pos = self.energy2bragg(ene_pos)
        winkel = math.radians(bragg_pos)

        try:
            delta_bragg = ((1.0 / resolution) * math.sin(winkel)) / math.cos(winkel)
            print(f"delta_bragg = {delta_bragg}")
        except:
            raise RuntimeError(f"MOCO ({self.name}: in calc_amp: Division by 0")

        # conversion factor for qgth2 to microrad
        delta_qgth2 = delta_bragg * 1000000.0 / 68.2875
        rc_fwhm = delta_bragg * 1000000.0 * math.sqrt(2.0)
        moco_amp = delta_qgth2 * math.sqrt(2.0) * percent / 100.0
        
        print(f"I assume a resolving power of {resolution}")
        print(f"At {ene_pos} KeV ({bragg_pos} degrees) the rc FWHM is {rc_fwhm} microrad and {delta_qgth2*math.sqrt(2)} in units of qgth2.")
        print(f"I determine a MoCo amplitude at {percent} percent of the FWHM, i.e. around {moco_amp}")

        return moco_amp

