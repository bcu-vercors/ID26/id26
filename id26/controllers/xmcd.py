import gevent

from bliss.controllers.motors.wago import WagoMotor

from id26.bcolor import bcolor

class XMCD (WagoMotor):

    def initialize(self):
        super().initialize()
        self._polkey = self.config.get("polarity_key")
        self._itlckey = self.config.get("interlock_key")
        self._curkey = self.config.get("current_key")
        self._scale = self.config.get("wago_scale", 1)
        ramp = self.config.get("wago_ramp")
        self.ramp_step = ramp.get('step', 0.05)
        self.ramp_period = ramp.get('period', 0.25)
        if self._scale == 0:
            self._scale = 1
        
    def read_position(self, axis):
        pos = super().read_position(axis)
        return pos*self.polarity*self._scale
        
    def start_one(self, motion):

        self.check_interlock()
        
        polarity = self.polarity
        
        if (motion.target_pos < 0 and polarity > 0) or (motion.target_pos > 0 and polarity < 0):
            print ("need to switch polarity, first go to 0")
            self._ramp_wago_voltage (0)
            gevent.sleep(0.5)
            self.polarity = -polarity

        self._ramp_wago_voltage(abs(motion.target_pos/self._scale))


    def _ramp_wago_voltage (self, target):

        curval = self.wago.get (self._curkey)
        delta = target-curval

        if (abs(delta) > self.ramp_step) :
            nsteps = abs(int(delta/self.ramp_step))
            print (f"too big change in current, will ramp {nsteps} steps")
            sign = 1 if delta > 0 else -1

            for ii, value in enumerate(map(lambda x:curval+x*self.ramp_step*sign, range(nsteps))):
                gevent.sleep(self.ramp_period)
                print (f"- step {ii+1}: {value*self._scale:.3f} A")
                self.wago.set(self._curkey, value)
                
        print (f"- target: {target*self._scale:.3f} A")
        self.wago.set(self._curkey, target)
        
    #TODO    
    #def state(self, axis):
    #    return AxisState("READY")

    #TODO do we need it ?
    def stop(self, axis):
        pass



    #XMCD
    @property
    def polarity (self):
        #0 --> inverted ie -1
        #1 --> normal   ie +1
        pol = self.wago.get(self._polkey)
        if pol == 0:
            return -1
        elif pol == 1:
            return 1
        else:
            raise Exception (f'{pol} not significant wago {self._polkey} value as a polarity')

    @polarity.setter
    def polarity (self, value):
        #0 --> inverted ie -1
        #1 --> normal   ie +1
        
        cur = self.wago.get(self._curkey)

        if abs(cur)>0.0001:
            raise Exception ("impossible to set magnet polarity, current is on {cur} : first move current to 0")

        self.wago.set(self._polkey, 1 if value > 0 else 0)
        

    def check_interlock (self):
        itlck = self.wago.get(self._itlckey)
        if itlck == 1:
            print('MAGNET interlock is ok (SAFE)')
        else:
            print(f'{bcolor.RED}MAGNET interlock FAULT{bcolor.END}')
            print('moving current to zero')
            self.wago.set(self._curkey, 0)
            raise Exception ('MAGNET interlock Fault')

        
            
