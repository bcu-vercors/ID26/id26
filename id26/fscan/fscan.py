import gevent

from bliss.common.user_status_info import status_message
from bliss.controllers.ct2.device import AcqMode as CT2AcqMode
from bliss.scanning.chain import AcquisitionMaster, ChainPreset
from bliss.scanning.acquisition.calc import CalcChannelAcquisitionSlave, CalcHook


from fscan.mcatools import get_fscan_mca_params
from fscan.fscanrunner import FScanRunner
from fscan.ftimescan import MusstProgFTimeScan
from fscan.mussttools import MusstChanUserCalc, MusstBaseCalc

from id26.fscan.ftimescan import ID26FScanConfig, ID26FTimeScanMaster

from bliss.common.logtools import (
#    log_error,
    log_warning,
#    log_debug,
#    log_info,
#    get_logger,
)

'''      
FSCAN

- name: fscanconfig_pm600
  class: ID26FTimeScanConfigPM600
  package: id26.fscan.ftimescan
  devices:
    musst: $musst_cc1
    opiom_multiplexer: $multiplexer_opiom_cc1

ftimescan_pm600 = fscanconfig_pm600.ftimescan_pm600
'''


class ID26FTimeScanConfigPM600(ID26FScanConfig):

    def __init__(self, name, config):
        super().__init__(name, config)
        self.add_scan('ftimescan_pm600', ID26FTimeScanMasterPM600)


class ID26FTimeScanMasterPM600(ID26FTimeScanMaster):
    
    def __init__(self, name, config):
        super().__init__(name, config)

        self.machinfo = config['devices'].get('machinfo')
        
        self._calchook = MusstEncoderCalcHook(
            encoder = config['devices'].get('mono_encoder'),
        )
        
        self._parameters = {
            'mono_trajectory': None,
            'id_trajectory': None,
            'event_trajectory': None,
            'id_linked': None,
            'duration': None,
        }
        self.pars.scan_mode = "EXTSTART"

       
   
    def setup_acq_chain (self, chain):
        pars = self.inpars

        # --- musst prog
        musstprog = MusstProgFTimeScan(self.musst)
        musstprog.check_max_timer(pars.total_time)
        musstprog.set_max_data_rate(self.musst_data_rate_factor * (2./pars.period))
        musstprog.set_extstart_params(pars.npoints, pars.acq_time, pars.period)

        if pars.lima_acc_used:
            musstprog.set_acc_params(pars.lima_acc_nb, pars.lima_acc_period)
        musstprog.setup(chain)

        musstprog.add_epoch_trig_calc(chain)
        musstprog.add_trig_calc(chain, "timer", *[])
        if musstprog.DataPerPoint == 2:
            musstprog.add_period_calc(chain, "timer", *[])
        musstprog.add_center_calc(chain)

        musstprog.add_user_calc(chain, [self._calchook], None)

        musstprog.set_timer_external_channel()
        acq_master = musstprog.musst_master

        # --- counters
        ct2pars = {
            "npoints": pars.npoints,
            "acq_expo_time": pars.acq_time,
            "acq_mode": CT2AcqMode.ExtGate,
        }
        self.mgchain.setup("ct2", chain, acq_master, ct2pars)

        # --- lima devs
        trig_mode = "EXTERNAL_TRIGGER_MULTI"

        limapars = {
            "acq_mode": pars.lima_acq_mode,
            "acq_trigger_mode": trig_mode,
            "acq_expo_time": pars.acq_time,
            "acq_nb_frames": pars.npoints,
            "wait_frame_id": [pars.npoints-1,],
            "prepare_once": True,
            "start_once": True,
        }
        self.mgchain.setup("lima", chain, acq_master, limapars)
        self.lima_used = self.mgchain.get_controllers_done("lima")

        # --- mca devices
        for mca_ctrl in self.mgchain.get_controllers_not_done("mca"):
            mca_params = get_fscan_mca_params(mca_ctrl, self.inpars.npoints, self.inpars.period)
            self.mgchain.setup("mca", chain, acq_master, mca_params, mca_ctrl.name)
            if "trigger_mode" in mca_params:
                # --- in spectrum reading mode
                self.mca_used.append(mca_ctrl)

        # --- mosca devices
        moscapars = {
            "trigger_mode": "GATE",
            "npoints": pars.npoints,
            "preset_time": pars.acq_time,
            "wait_frame_id": [pars.npoints,],
            "start_once": True,
        }
        self.mgchain.setup("mosca", chain, acq_master, moscapars)

        # --- calc
        self.mgchain.setup_calc_counters(chain, acq_master)

        # --- sampling
        self.mgchain.setup_sampling(chain, pars.sampling_time)

       
        top_masters = [PM600AcquisitionMaster(
            self._parameters['mono_trajectory'],
            self._parameters['event_trajectory'],
        )]
        if self._parameters['id_linked']:
            top_masters.append(IDAcquisitionMaster(
                self._parameters['id_trajectory'],
            ))
        for i, master in enumerate(top_masters):
            if master:
                slave = master
                master = chain.top_masters[0] if i==0 else top_masters[i-1]
                chain.add(master, slave)


        try:
            if self.machinfo.check:
                chain.add_preset(WaitForRefillChainPreset(
                    self.machinfo,
                    checktime=self._parameters["duration"]
                ))
        except Exception:
            log_warning(self, 'check for refill could not be activated - check your machinfo settings.', exc_info=True)

        #TODO IDsyncAcqmaster ???
        print(chain.tree)

    def get_runner_class(self):
        return ID26FTimeScanCustomRunnerPM600

    
class ID26FTimeScanCustomRunnerPM600(FScanRunner):
    def __call__(self, acq_time, npoints, period=0, **kwargs):
        pars = dict(acq_time = acq_time,
                    npoints = npoints,
                    period = period,
               )
        pars.update(kwargs)
        #self.run_with_pars(pars)
        scan_info = pars.pop("scan_info", None)
        x = pars.pop("x", None)
       
        lpars = self.get_local_pars()
        lpars.update(pars)
        self.pars.set(**lpars)
        #self.run(scan_info)
        self.prepare(scan_info)
        self._scan.scan_info ['plots'][0]['items'][0]['x'] = x
        self.start()


class PM600AcquisitionMaster(AcquisitionMaster):

    def __init__(self, trajectory, event):
        super().__init__(name='pm600_master')
        self.trajectory = [trajectory]
        self.pm600 = trajectory.axis.controller
        self.event = event

    def prepare(self):
        pass
    
    def start(self):
        self.event.set()
        self.pm600.start_trajectory(*self.trajectory)
        
    def stop(self):
        pass
    

class IDAcquisitionMaster(AcquisitionMaster):
    def __init__(self, trajectory):
        super().__init__(name = 'id_master')
        self.trajectory = trajectory

    def prepare(self):
        self.trajectory.prepare()

    def start(self):
        self.trajectory.start()
        
    def stop(self):
        self.trajectory.stop()

        
class MusstEncoderCalcHook(MusstChanUserCalc):
    def __init__(self,  encoder):
        self._encoder = encoder
        self._input_channel = "encoder_hdh_center"
        self._output_channels = ["hdh_enc","hdh_angle","hdh_energy",]
        
    def select_input_channels(self, channels_list, scan_pars):
        return [self._input_channel]

    def get_output_channels(self):
        return self._output_channels

    def steps2angle (self, steps):
        return (steps * self._encoder.direction + self._encoder.offset["user"] ) / self._encoder.steps_per_unit
    
    def angle2energy (self, angle):
        return self._encoder.dcm.bragg2energy(angle)
    
    def calculate (self, input_data_dict):
        steps = input_data_dict[self._input_channel]
        angle = self.steps2angle (steps)
        result = {
            "hdh_enc": steps,
            "hdh_angle": angle ,
            "hdh_energy":  self.angle2energy (angle),
        }
 
        return result


class WaitForRefillChainPreset(ChainPreset):
    def __init__(self, machinfo, checktime=None, waittime=None, polling_time=1.):
        ChainPreset.__init__(self)
        self.machinfo = machinfo
        self.checktime = checktime
        self.waittime = waittime
        self.polling_time = polling_time

        print(
            f"{machinfo.name} will automatically wait for refill before fscan launches. (duration: {checktime})"
        )

        
    def start(self, acq_chain):
        ok = self.machinfo.check_for_refill(self.checktime)
        with status_message() as p:
            while not ok:
                p("Waiting for refill...")
                gevent.sleep(self.polling_time)
                ok = self.machinfo.check_for_refill(self.checktime)
                if ok and self.waittime:
                    p("Waiting {self.waittime} after Beam is back")
                    gevent.sleep(self.waittime)
                    ok = self.machinfo.check_for_refill(self.checktime)
    


                    
