from fscan.chaintools import ChainTool
from fscan.fscanconfig import FScanConfig
from fscan.ftimescan import FTimeScanMaster

from id26.controllers.fscan_controller import ZapMultiplexerChainPreset
from id26.controllers.fast_shutter import FShutterFTimeScanChainPreset

'''
from fscan.fscanrunner import FScanRunner
from fscan.fscantools import FScanParamStruct
from fscan.ftimescan import FTimeScanPars
'''




'''
- name: fscanconfig
  class: ID26FTimeScanConfig
  package: id26.fscan.ftimescan
  devices:
    musst: $musst_cc1
    opiom_multiplexer: $multiplexer_opiom_cc1
    fast_shutter: $fsh

ftimescan = fscanconfig.ftimescan_id26
'''

class ID26ChainTool(ChainTool):
    
    def setup_calc_counters(self, chain, master, *names):
        all_nodes = self.get_nodes_by_type("calc", *names)
        if not len(all_nodes):
            return

        all_done = self.get_controllers_done()
        for node in all_nodes:
            input_ctrls = self._get_calc_input_controllers(node.controller)
            setup_ctrls = [ ctrl in all_done for ctrl in input_ctrls ]
            count_time = master.vars['ACQTIME']/master.musst.get_timer_factor()
            node.set_parameters(acq_params={"count_time": count_time})
            if all(setup_ctrls):
                chain.add(master, node)

                
class ID26FTimeScanMaster(FTimeScanMaster):
    
    def init_scan(self):
        super().init_scan()
        self.mgchain = ID26ChainTool(self.meas_group, self.chain_config)


class ID26FScanConfig(FScanConfig):

    def __init__(self, name, config):
        super().__init__(name, config)
        self.multiplexer = config['devices'].get('opiom_multiplexer')
        self.add_chain_preset (ZapMultiplexerChainPreset(self.multiplexer))


class ID26FTimeScanConfig(ID26FScanConfig):

    def __init__(self, name, config):
        super().__init__(name, config)
        self.fsh = config['devices'].get('fast_shutter')
        self.add_chain_preset (FShutterFTimeScanChainPreset (self.fsh))
        self.add_scan('ftimescan_id26', ID26FTimeScanMaster)


