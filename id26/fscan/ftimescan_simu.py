from fscan.chaintools import ChainTool
from fscan.fscanconfig import FScanConfig
from fscan.ftimescan import FTimeScanMaster
from id26.controllers.fscan_controller import ZapMultiplexerChainPreset
from id26.controllers.fast_shutter import FShutterFTimeScanChainPreset


from bliss.scanning.chain import AcquisitionMaster
from fscan.fscanrunner import FScanRunner
from fscan.fscantools import FScanParamStruct
from fscan.ftimescan import FTimeScanPars

from bliss.common.logtools import (
    log_error,
    log_warning,
    log_debug,
    log_info,
    get_logger,
    user_warning,
    user_debug,
)



'''
- name: fscanconfig
  class: ID26FTimeScanConfig
  package: id26.fscan.ftimescan
  devices:
    musst: $musst_cc1
    opiom_multiplexer: $multiplexer_opiom_cc1
    fast_shutter: $fsh

ftimescan = fscanconfig.ftimescan_id26
'''

class ID26ChainTool(ChainTool):
    
    def setup_calc_counters(self, chain, master, *names):
        all_nodes = self.get_nodes_by_type("calc", *names)
        if not len(all_nodes):
            return

        all_done = self.get_controllers_done()
        for node in all_nodes:
            input_ctrls = self._get_calc_input_controllers(node.controller)
            setup_ctrls = [ ctrl in all_done for ctrl in input_ctrls ]

            count_time = master.vars['ACQTIME']/master.musst.get_timer_factor()
            
            node.set_parameters(acq_params={"count_time": count_time})
            if all(setup_ctrls):
                chain.add(master, node)

class ID26FTimeScanMaster(FTimeScanMaster):
    def init_scan(self):
        super().init_scan()
        self.mgchain = ID26ChainTool(self.meas_group, self.chain_config)


class ID26FScanConfigBase(FScanConfig):

    def __init__(self, name, config):
        super().__init__(name, config)
        self.multiplexer = config['devices'].get('opiom_multiplexer')
        self.add_chain_preset (ZapMultiplexerChainPreset(self.multiplexer))


class ID26FTimeScanConfig(ID26FScanConfigBase):

    def __init__(self, name, config):
        super().__init__(name, config)
        self.fsh = config['devices'].get('fast_shutter')
        self.add_chain_preset (FShutterFTimeScanChainPreset (self.fsh))

        self.add_scan('ftimescan_id26', ID26FTimeScanMaster)

'''      
NEW FSCAN

- name: fscanconfig_pm600
  class: ID26FTimeScanConfigPM600
  package: id26.fscan.ftimescan
  devices:
    musst: $musst_cc1
    opiom_multiplexer: $multiplexer_opiom_cc1

ftimescan_pm600 = fscanconfig_pm600.ftimescan_pm600
'''


class ID26FTimeScanConfigPM600(ID26FScanConfigBase):

    def __init__(self, name, config):
        super().__init__(name, config)
        #do we need the machinfo stuff ?
        
        self.add_scan('ftimescan_pm600', ID26FTimeScanMasterPM600)


class ID26FTimeScanMasterPM600(ID26FTimeScanMaster):
    def __init__(self, name, config):
        super().__init__(name, config)
       
        self.multiplexer = config['devices'].get('opiom_multiplexer')
        self._parameters = {
            '_mono_trajectory': None,
            '_id_trajectory': None,
            '_event_trajectory': None,
            'id_linked': None,
        }
        
        self.pars.scan_mode = "EXTSTART"
        
    def _setup_top_masters (self):
        pars = self._parameters
        
        monomaster = ID26PM600AcquisitionMaster(
            pars['_mono_trajectory'],
            pars['_event_trajectory'],
        )
        
        idmaster = ID26IDAcquisitionMaster(
            pars['_id_trajectory'],
        ) if pars['id_linked'] else None
        
        return [monomaster, idmaster]
    
    def setup_acq_chain (self, chain):
        super().setup_acq_chain(chain)
        top_masters = self._setup_top_masters ()
        for i, master in enumerate(top_masters):
            if master:
                slave = master
                master = chain.top_masters[0] if i==0 else top_masters[i-1]
                chain.add(master, slave)
            
        #TODO IDsyncAcqmaster ???
        print ('helloooo')
        print(chain.tree)


        # specific
        
class ID26PM600AcquisitionMaster(AcquisitionMaster):

    def __init__(self, mono_trajectory, event):
        super().__init__(name='pm600_master')
        self.monotraj = [mono_trajectory]
        self.pm600 = mono_trajectory.axis.controller
        self.event = event

    def prepare(self):
        pass
    
    def start(self):
        self.event.set()
        self.pm600.start_trajectory(*self.monotraj)
        
    def stop(self):
        pass


class ID26IDAcquisitionMaster(AcquisitionMaster):
    def __init__(self, id_trajectory):
        super().__init__(name = 'id_master')
        self.idtraj = id_trajectory

    def prepare(self):
        self.idtraj.move_to_start()

    def start(self):
        self.idtraj.start_trajectory()
        
    def stop(self):
        self.idtraj.stop_trajectory()

        
'''      
NEW FSCAN with SIMULATED PM600 and ID but using OPIOM and MUSST switches

use OPIOM O8 to simulate pm600 motion
put cable from O8 to I1 instead of pm600 cable
self.multiplexer.switch("SOFT_TRIGGER","OPEN")
self.multiplexer.switch("SOFT_TRIGGER","CLOSE
OPTICS [10]: opiom_cc1.outputs_stat()
O1       O2      O3      O4      O5      O6      O7      O8
0        0       0       0       0       0       0       0
OB1      OB2     OB3     OB4     OB5     OB6     OB7     OB8
0        0       0       0       0       0       0       0
OPTICS [11]: multiplexer_opiom_cc1.switch('SOFT_TRIGGER','OPEN')
OPTICS [12]: opiom_cc1.outputs_stat()
O1       O2      O3      O4      O5      O6      O7      O8
0        0       0       0       0       0       0       1
OB1      OB2     OB3     OB4     OB5     OB6     OB7     OB8
0        0       0       0       0       0       0       0
OPTICS [13]: multiplexer_opiom_cc1.switch('SOFT_TRIGGER','CLOSE')
OPTICS [14]: opiom_cc1.outputs_stat()
O1       O2      O3      O4      O5      O6      O7      O8
0        0       0       0       0       0       0       0
OB1      OB2     OB3     OB4     OB5     OB6     OB7     OB8
0        0       0       0       0       0       0       0


")
- name: fscanconfig_simu1
  class: ID26FTimeScanConfigSIMU1
  package: id26.fscan.ftimescan
  devices: 
    musst: $musst_cc1
    opiom_multiplexer: $multiplexer_opiom_cc1

fscan_simu1 = fscanconfig_simu1.ftimescan_pm600
'''


class ID26FTimeScanConfigSIMU1(ID26FScanConfigBase):

    def __init__(self, name, config):
        super().__init__(name, config)
        #do we need the machinfo stuff ?
        
        self.add_scan('ftimescan_pm600', ID26FTimeScanMasterSIMU1)


class ID26FTimeScanMasterSIMU1(ID26FTimeScanMaster):
    def __init__(self, name, config):
        super().__init__(name, config)
       
        self.multiplexer = config['devices'].get('opiom_multiplexer')
        self._parameters = {
            '_mono_trajectory': 1,
            '_id_trajectory': 1,
            '_event_trajectory': None,
            'id_linked': None,
        }
        
        self.pars.scan_mode = "EXTSTART"
        
    def _setup_top_masters (self):
        pars = self._parameters
        
        monomaster = ID26PM600AcquisitionMasterSIMU1(
            pars['_mono_trajectory'] if not pars['id_linked'] else None,
            pars['_event_trajectory'],
            self.multiplexer,
        )
        
        idmaster = ID26IDAcquisitionMasterSIMU1(
            pars['_id_trajectory'],
            self.multiplexer,
        ) if pars['id_linked'] else None
        
        return [monomaster, idmaster]
    
    def setup_acq_chain (self, chain):
        super().setup_acq_chain(chain)
        top_masters = self._setup_top_masters ()
        for i, master in enumerate(top_masters):
            if master:
                slave = master
                master = chain.top_masters[0] if i==0 else top_masters[i-1]
                chain.add(master, slave)
            
        #TODO IDsyncAcqmaster ???
        print(chain.tree)


        # specific
        
class ID26PM600AcquisitionMasterSIMU1(AcquisitionMaster):

    def __init__(self, mono_trajectory, event, opiom):
        super().__init__(name='pm600_master_simu1')
        self.monotraj = [mono_trajectory]
        self.pm600 = None
        self.event = event
        self.multiplexer = opiom

    def prepare(self):
        pass
    
    def start(self):
        log_debug(
            self,
            'start',
        )
        self.event.set()
        log_debug(
            self,
            'EVENT Trajectory Started SET',
        )
        print("pm600.start_trajectory()")
        if self.monotraj[0]:
            self.multiplexer.switch("SOFT_TRIGGER","OPEN")
            print("simulating mono is moving")
            gevent.sleep(self.monotraj[0])

        
    def stop(self):
        if self.monotraj[0]:
            self.multiplexer.switch("SOFT_TRIGGER","CLOSE")




class ID26IDAcquisitionMasterSIMU1(AcquisitionMaster):
    def __init__(self, id_trajectory, opiom):
        super().__init__(name = 'id_master')
        self.idtraj = id_trajectory
        self.multiplexer = opiom


    def prepare(self):
        log_debug(
            self,
            'prepare',
        )
        print("self.idtraj.move_to_start()")

    def start(self):
        log_debug(
            self,
            'start',
        )
        print("self.idtraj.start_trajectory()")
        self.multiplexer.switch("SOFT_TRIGGER","OPEN")
        print("simulating mono is moving")
        gevent.sleep(self.idtraj)

    def stop(self):
        log_debug(
            self,
            'stop',
        )
        self.multiplexer.switch("SOFT_TRIGGER","CLOSE")
        print("self.idtraj.stop_trajectory()")

        
