============
ID26 project
============

[![build status](https://gitlab.esrf.fr/ID26/id26/badges/master/build.svg)](http://ID26.gitlab-pages.esrf.fr/ID26)
[![coverage report](https://gitlab.esrf.fr/ID26/id26/badges/master/coverage.svg)](http://ID26.gitlab-pages.esrf.fr/id26/htmlcov)

ID26 software & configuration

Latest documentation from master can be found [here](http://ID26.gitlab-pages.esrf.fr/id26)
