#  ID26Setting BLISS Config Object Class
Class provinding functions to store/retrieve/handle values of a set of variables previously declared in a config yml file.

[ID26GlobalSetting Class](https://gitlab.esrf.fr/bcu-vercors/ID26/id26/-/blob/master/docs/beamline_settings.md). provides the same functions. Refer to it for a complete description of the functions.

Those variables are made persistant in time, so that they can be retrieved even in case of quitting bliss session.
They can be shared between sessions.

## Usage:

- **http://bassoon:9030**

  -   add some yml   
<pre>
       - package: id26.controllers.parameters
         class: ID26Setting
         plugin: bliss 
         name: *the unique name of the bliss object*
         defaults:
            - var1: value
            - var2: value
            -  ...
</pre>

- **http://bassoon:9030**

  - add the bliss object name to your session yml file.
  - *name*.setup(); *name*.save(); *name*['varname']=value ...

## Example:

<pre>

bassoon % /users/blissadm/local/beamline_configuration/miscellaneous/general_vars.yml:

- package: id26.controllers.parameters
  name: par26
  class: ID26Setting
  defaults:
  - distance_ps: 26820
  - u35u_mingap: 10.81
  - u35m_mingap: 11.10
  - u35d_mingap: 10.91
  - shutter_control : 0
  - energy_edge : None
  - energy_excite_above: None

bassoon % /users/blissadm/local/beamline_configuration/sessions/mcl.yml:
- par26

MCL [24]: par26
Out [24]: ID26Setting: par26
          * means default value not stored in Redis

          values:
          - distance_ps : 26820 *
          - u35u_mingap : 10.81 *
          - u35m_mingap : 11.1 *
          - u35d_mingap : 10.91 *
          - shutter_control : 0 *
          - energy_edge : None *
          - energy_excite_above : None *

MCL [26]: par26.setup() #or par26['energy_edge']=7, or par26.load('/tmp') 
MCL [27]: par26.save('/tmp')
par26 saved to file:/tmp/id26Variables_par26.yml

MCL [47]: align.align_mirrors( par26['energy_edge'], horz = True, vert = True, range_horz = align_par['range_horz'], range_vert = 3, monitor = 'offset:I02')







