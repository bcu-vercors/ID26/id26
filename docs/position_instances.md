# PositionInstances ID26 BLISS Class
Class provinding tools to store/retrieve positions of a set of axis along other user actions.

The positions are made persistant in time, so that they can be retrieved even in case of quitting bliss session.

They belong to one particular bliss session and cannot be shared between sessions.
## Usage:
- **from id26.scripts.position_instances import PositionInstances**
- **user_instance = PositionInstances ('instance_name' , mot1, mot2, mot3 ...)**
   loads as 'user_instance' session object any previously initialised PositionInstances with the name 'instance_name', or create it if it is the first time it is instantiated.
  - if a list of axis is passed as arguments, slots are prepared for that set of axis in redis database. 
  - if no axis is passed, slots are prepared for all the motors of the session.
  
- **user_instance.update ()**
   stores in 'user_instance' object (attached to 'instance_name' in redis) the current positions of the axis in the set. 
- **user_instance.update  (mot1, mot2)**
   stores in 'user_instance' object the current positions of the listed axis only. 
- **user_instance.position  (mot3, 3.21)**
   stores in 'user_instance' the specified position value for the specified axis. value can be None.
- **user_instance.setup ()**
   launches a dialog window to enter positions to be stored in 'user_instance'. Position can be None 
- **user_instance.move (mot3, mot4)**
  move all axis listed to the position that was stored (if any) for them in 'user_instance'
- **user_instance.move_all ()**
  move all axis of the set to the position that was stored (if any) for them in 'user_instance'. 
- **user_instance.load (directory, suffix='')**
   stores positions read from a previously saved file (see .save() below)
   - directory (path) must be specified
   - a suffix can be given, the filename is : {session}_positions{suffix}.yml 

The following functions act on all the position instances existing in redis database. The behaviour will be the same whatever is the session object calling it.
- **user_instance.show ()**
  shows all positions sets stored among their different existing instances in redis database. It might happen that some instances are no longer useful. **note that this is the user responsibility to clear unused instances** to free redis memory with the next command.
- **user_instance.remove_instance ('unwanted_instance_name')**
remove specified instance from the database.

- **user_instance.add  (mot3, mot4, mot5)**
   add some axis slots. do not forget to assign them some positions after. (.update(), .position() or .setup()). 
- **user_instance.remove (mot1, mot2)**
   remove axis slots from the instances.
  
- **user_instance.save (directory, suffix = '')**
  saves to disk a yml files with all instances content.
  - a directory must be specified.
  - a suffix can be given,
  - the filename is : {session}_positions{suffix}.yml 
  
## Example:
<pre>
 MCL [5]: pos0 = PositionInstances ('oldpos0', gap)
 Initialising oldpos0
 axis names: ['gap']
  
 MCL [6]: pos0.update()
 updating oldpos0 with {'gap': 3.0}
  
 MCL [7]: #gap position is stored
  
 MCL [8]: user_script_aligning_mot1()
 Moving gap from 3 to 10
 Aligning mot1 bla bla
  
 MCL [9]: pos1 = PositionInstances ('oldpos1',mot1)
 Initialising oldpos1
 axis names: ['mot1']
  
 MCL [10]: #a slot for mot1 is created, gap is kept
  
 MCL [11]: pos1.show()
  
 oldpos1 (SELECTED)             oldpos0             oldpos2             default
 gap          None                 3.0                 3.0                None
 mot1         None                None                None                None
 creation_date    # 2020-03-23-14:40  # 2020-03-23-14:38  # 2020-03-19-15:29  # 2020-03-19-15:29
 last_accessed    # 2020-03-23-14:40  # 2020-03-23-14:39  # 2020-03-23-14:38  # 2020-03-19-15:29
  
 MCL [12]: pos1.update()
 updating oldpos1 with {'gap': 10.0, 'mot1': 29.0}
  
 MCL [13]: pos1.show()
  
 oldpos1 (SELECTED)             oldpos0             oldpos2             default
 gap           10.0                3.0                 3.0                None
 mot1          29.0               None                None                None
 creation_date    # 2020-03-23-14:40  # 2020-03-23-14:38  # 2020-03-19-15:29  # 2020-03-19-15:29
 last_accessed    # 2020-03-23-14:42  # 2020-03-23-14:39  # 2020-03-23-14:38  # 2020-03-19-15:29
  
 MCL [14]: pos0.move(gap)
 Moving gap from 10 to 3
  
 MCL [15]: #gap moved back to pos0 stored position
  
 MCL [16]: #As I dont need oldpos2 instance, I will clear it
  
 MCL [17]: pos0.remove_instance('oldpos2')
 oldpos2 removed.
  
 MCL [18]: pos0.show()
  
 oldpos0 (SELECTED)             oldpos1             default
 gap          10.0                10.0                None
 mot1         29.0                29.0                None
 creation_date    # 2020-03-23-14:38  # 2020-03-23-14:40  # 2020-03-19-15:29
 last_accessed    # 2020-03-23-14:53  # 2020-03-23-14:52  # 2020-03-19-15:29
  
 MCL [19]: user_script_aligning_c[ontinue()
</pre>


