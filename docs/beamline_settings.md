# ID26Settings ID26 BLISS Class
Class provinding functions to store/retrieve/handle values of a set of variables.

Those variables are made persistant in time, so that they can be retrieved even in case of quitting bliss session.
They can be shared between sessions.
## Usage:
- **from id26.scripts.beamline_settings import ID26Settings**

- **obj = ID26Settings (name)**
   loads in session object *obj* any previously initialised ID26Settings set with the same name, 
   or create its Redis slot if it is the first time it is initialised.
- **obj [var_name] = value**
  fills *obj* with variables and their values. 
  creates the variables if it is the first time they are addressed
- **obj.setup ()**
   launches a dialog window to assign values to already existing variables in the set.  
- **obj.save (directory, prefix = '',  suffix = '')**
  saves to disk a yml file with *obj* content.
  - a directory must be specified.
  - a prefix and a suffix can be given,
  - the filename is : {prefix}id26Variables_{name}{suffix}.yml 
- **obj.load (directory,  prefix='',  suffix='', donotupdate = False)**
  loads in *obj* variables and values from a previously saved file 
   - directory (path) must be specified
   - a prefix and a suffix can be given, the filename is : {prefix}id26Variables_{name}{suffix}.yml 
  if *donotupdate* is True, file content is just displayed, no variable is updated.   
  Note that :
      -  all the variables in the file will be either updated or created (again if they were removed at some point) .
      -  all the variables in the set but not in the file are not affected by the load.  
- **obj**
  shows obj content
- **obj.remove (\*var_name)**
   removes unwanted variables from the set.
- **obj.clear()**
   entirely clears the set of variables. 
  
## Example:
<pre>

MCL [25]: align_par = ID26Settings ('align_par')
MCL [29]: align_par ['range_verz'] = 20
MCL [30]: align_par ['range_unwanted'] = 20
MCL [31]: align_par ['range_horz'] = 20
MCL [32]: align_par ['nosteps'] = 200
MCL [33]: align_par ['counttime'] = 2
MCL [34]: align_par
Out [34]: ID26Settings: align_par
          * means default value not stored in Redis

          values:
          - range_verz : 20
          - range_unwanted : 20
          - range_horz : 20
          - nosteps : 200
          - counttime : 2

MCL [35]: align_par.save('/tmp',prefix='20200406_',suffix='_Test')
align_par saved to file:/tmp/20200406_id26Variables_align_par_Test.yml
MCL [38]: align_par.remove('range_unwanted')

MCL [45]: align_par.setup()
MCL [46]: align_par
Out [46]: ID26Settings: align_par
          * means default value not stored in Redis

          values:
          - range_verz : 10
          - range_horz : 20
          - nosteps : 200
          - counttime : 2


MCL [47]: align.align_mirrors(par26['energy_edge'], horz = True, vert = True, range_horz = align_par['range_horz'], range_vert = 3, monitor = 'offset:I02')






